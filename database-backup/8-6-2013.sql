-- Adminer 3.5.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `actuality_comment`;
CREATE TABLE `actuality_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3CD18099C4663E4` (`page_id`),
  KEY `IDX_3CD18099F675F31B` (`author_id`),
  CONSTRAINT `FK_3CD18099C4663E4` FOREIGN KEY (`page_id`) REFERENCES `actuality_page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_3CD18099F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `actuality_comment` (`id`, `page_id`, `text`, `title`, `created`, `updated`, `author_id`) VALUES
(1,	2,	'textík',	'nadpis',	'2013-05-04 14:47:37',	NULL,	NULL);

DROP TABLE IF EXISTS `actuality_page`;
CREATE TABLE `actuality_page` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_2BB8DA4BBF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `actuality_page` (`id`, `itemsPerPage`) VALUES
(2,	10);

DROP TABLE IF EXISTS `agenda_data`;
CREATE TABLE `agenda_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `text` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `event_id` smallint(6) NOT NULL,
  `event_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_90D65113C4663E4` (`page_id`),
  KEY `IDX_90D65113F675F31B` (`author_id`),
  CONSTRAINT `FK_90D65113C4663E4` FOREIGN KEY (`page_id`) REFERENCES `agenda_page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_90D65113F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `agenda_page`;
CREATE TABLE `agenda_page` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_292F1450BF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `agenda_page` (`id`, `itemsPerPage`) VALUES
(4,	10);

DROP TABLE IF EXISTS `directory`;
CREATE TABLE `directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invisible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_467844DA727ACA70` (`parent_id`),
  CONSTRAINT `FK_467844DA727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `directory` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `element`;
CREATE TABLE `element` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nameRaw` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` int(11) NOT NULL,
  `langMode` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_41405E398C22AA1A` (`layout_id`),
  KEY `IDX_41405E39C4663E4` (`page_id`),
  KEY `IDX_41405E3934ECB4E6` (`route_id`),
  KEY `IDX_41405E3982F1BAF4` (`language_id`),
  KEY `name_idx` (`name`),
  CONSTRAINT `FK_41405E3934ECB4E6` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_41405E3982F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_41405E398C22AA1A` FOREIGN KEY (`layout_id`) REFERENCES `layout` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_41405E39C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `protected` tinyint(1) NOT NULL,
  `file` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invisible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8C9F3610727ACA70` (`parent_id`),
  CONSTRAINT `FK_8C9F3610727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `directory` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `file` (`id`, `parent_id`, `protected`, `file`, `name`, `path`, `invisible`) VALUES
(2,	NULL,	0,	0,	'Bakalarska-prace.pdf',	'bakalarska-prace.pdf',	0);

DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `short` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D4DB71B55E237E06` (`name`),
  UNIQUE KEY `UNIQ_D4DB71B58F2890A2` (`short`),
  UNIQUE KEY `UNIQ_D4DB71B5E16C6B94` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `language` (`id`, `name`, `short`, `alias`) VALUES
(1,	'Čeština',	'cs',	'cs');

DROP TABLE IF EXISTS `layout`;
CREATE TABLE `layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locked` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `layout` (`id`, `file`, `locked`, `name`) VALUES
(1,	'@cmsModule/layouts/bootstrap/@layout.latte',	0,	'@cmsModule - bootstrap - default'),
(2,	'@appearaniceModule/basic/@layout.latte',	0,	'@appearaniceModule - basic');

DROP TABLE IF EXISTS `layoutconfig`;
CREATE TABLE `layoutconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetKey` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8F3F68C5A76ED395` (`user_id`),
  KEY `IDX_8F3F68C5C4663E4` (`page_id`),
  KEY `created_idx` (`created`),
  CONSTRAINT `FK_8F3F68C5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8F3F68C5C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `log` (`id`, `user_id`, `page_id`, `created`, `target`, `targetKey`, `type`, `action`, `message`) VALUES
(1,	NULL,	1,	'2013-04-22 10:47:59',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(2,	NULL,	1,	'2013-04-22 10:48:13',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(3,	NULL,	1,	'2013-04-22 10:48:15',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	'Page has been published'),
(4,	NULL,	2,	'2013-04-22 10:54:41',	'ActualityModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(5,	NULL,	2,	'2013-04-22 10:54:45',	'ActualityModule\\Entities\\PageEntity',	2,	NULL,	'updated',	'Page has been published'),
(6,	NULL,	3,	'2013-04-22 10:56:44',	'ThesiscollectionModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(7,	NULL,	3,	'2013-04-22 10:56:50',	'ThesiscollectionModule\\Entities\\PageEntity',	3,	NULL,	'updated',	'Page has been published'),
(8,	NULL,	NULL,	'2013-04-22 10:57:41',	'Venne\\Forms\\Form',	NULL,	'/admin/cs/informations',	'other',	'Configuration has been updated'),
(9,	NULL,	NULL,	'2013-04-22 10:59:19',	'Venne\\Forms\\Form',	NULL,	'/admin/cs/application',	'other',	'Configuration has been updated'),
(10,	NULL,	NULL,	'2013-04-22 10:59:25',	'Venne\\Forms\\Form',	NULL,	'/admin/cs/application',	'other',	'Configuration has been updated'),
(11,	NULL,	NULL,	'2013-04-22 10:59:56',	'Venne\\Forms\\Form',	NULL,	'/admin/cs/application',	'other',	'Configuration has been updated'),
(12,	NULL,	4,	'2013-04-22 11:08:06',	'AgendaModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(13,	NULL,	5,	'2013-04-22 11:08:35',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(14,	NULL,	5,	'2013-04-22 11:09:05',	'CmsModule\\Content\\Entities\\StaticPageEntity',	5,	NULL,	'updated',	''),
(15,	NULL,	5,	'2013-04-22 11:09:12',	'CmsModule\\Content\\Entities\\StaticPageEntity',	5,	NULL,	'updated',	'Page has been published'),
(16,	NULL,	4,	'2013-04-22 11:09:22',	'AgendaModule\\Entities\\PageEntity',	4,	NULL,	'updated',	'Page has been published'),
(17,	NULL,	6,	'2013-04-22 11:10:00',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(18,	NULL,	6,	'2013-04-22 11:10:13',	'CmsModule\\Content\\Entities\\StaticPageEntity',	6,	NULL,	'updated',	''),
(19,	NULL,	6,	'2013-04-22 11:10:18',	'CmsModule\\Content\\Entities\\StaticPageEntity',	6,	NULL,	'updated',	'Page has been published'),
(20,	NULL,	NULL,	'2013-04-22 11:10:40',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(21,	NULL,	NULL,	'2013-04-22 11:10:42',	'CmsModule\\Content\\Entities\\StaticPageEntity',	7,	NULL,	'updated',	'Page has been published'),
(22,	NULL,	8,	'2013-04-22 11:11:47',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(23,	NULL,	8,	'2013-04-22 11:11:49',	'CmsModule\\Content\\Entities\\StaticPageEntity',	8,	NULL,	'updated',	'Page has been published'),
(24,	NULL,	8,	'2013-04-22 11:12:08',	'CmsModule\\Content\\Entities\\StaticPageEntity',	8,	NULL,	'updated',	''),
(25,	NULL,	9,	'2013-04-22 11:12:27',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(26,	NULL,	9,	'2013-04-22 11:12:39',	'CmsModule\\Content\\Entities\\StaticPageEntity',	9,	NULL,	'updated',	''),
(27,	NULL,	9,	'2013-04-22 11:12:42',	'CmsModule\\Content\\Entities\\StaticPageEntity',	9,	NULL,	'updated',	'Page has been published'),
(28,	NULL,	10,	'2013-04-22 11:13:13',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(29,	NULL,	10,	'2013-04-22 11:13:15',	'CmsModule\\Content\\Entities\\StaticPageEntity',	10,	NULL,	'updated',	'Page has been published'),
(30,	NULL,	10,	'2013-04-22 11:13:28',	'CmsModule\\Content\\Entities\\StaticPageEntity',	10,	NULL,	'updated',	''),
(31,	NULL,	11,	'2013-04-22 11:13:45',	'CmsModule\\Content\\Entities\\StaticPageEntity',	NULL,	NULL,	'created',	''),
(32,	NULL,	11,	'2013-04-22 11:13:49',	'CmsModule\\Content\\Entities\\StaticPageEntity',	11,	NULL,	'updated',	''),
(33,	NULL,	11,	'2013-04-22 11:13:55',	'CmsModule\\Content\\Entities\\StaticPageEntity',	11,	NULL,	'updated',	'Page has been published'),
(34,	NULL,	5,	'2013-04-22 11:14:33',	'CmsModule\\Content\\Entities\\StaticPageEntity',	5,	NULL,	'updated',	''),
(35,	NULL,	1,	'2013-04-22 16:20:49',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(36,	NULL,	1,	'2013-04-22 16:43:46',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(37,	NULL,	1,	'2013-04-22 16:47:30',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(38,	NULL,	1,	'2013-04-22 18:14:30',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(39,	NULL,	1,	'2013-04-22 18:31:42',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(40,	NULL,	NULL,	'2013-04-22 18:32:19',	'CmsModule\\Content\\Entities\\StaticPageEntity',	7,	NULL,	'updated',	''),
(41,	NULL,	1,	'2013-04-22 18:50:14',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(42,	NULL,	1,	'2013-04-22 23:01:12',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(43,	NULL,	1,	'2013-04-24 13:29:20',	'CmsModule\\Content\\Entities\\StaticPageEntity',	1,	NULL,	'updated',	''),
(44,	1,	NULL,	'2013-04-27 15:53:31',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(45,	1,	NULL,	'2013-04-27 15:56:29',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(46,	1,	NULL,	'2013-05-03 11:38:54',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(47,	1,	NULL,	'2013-05-03 11:31:07',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(48,	1,	NULL,	'2013-05-03 11:36:47',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(49,	2,	NULL,	'2013-05-03 11:50:21',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(50,	1,	NULL,	'2013-05-03 13:59:50',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(51,	1,	NULL,	'2013-05-03 14:07:47',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(52,	1,	NULL,	'2013-05-03 14:09:01',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(53,	2,	NULL,	'2013-05-03 14:19:19',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(54,	2,	NULL,	'2013-05-03 14:19:43',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(55,	1,	NULL,	'2013-05-03 14:36:32',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(56,	1,	NULL,	'2013-05-03 14:49:56',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(57,	NULL,	12,	'2013-05-03 15:06:15',	'CmsModule\\Content\\Entities\\RegistrationPageEntity',	NULL,	NULL,	'created',	''),
(58,	NULL,	12,	'2013-05-03 15:06:31',	'CmsModule\\Content\\Entities\\RegistrationPageEntity',	12,	NULL,	'updated',	''),
(59,	NULL,	12,	'2013-05-03 15:06:38',	'CmsModule\\Content\\Entities\\RegistrationPageEntity',	12,	NULL,	'updated',	'Page has been published'),
(60,	3,	NULL,	'2013-05-03 15:08:09',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(61,	3,	NULL,	'2013-05-03 15:08:42',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(62,	1,	NULL,	'2013-05-03 15:24:33',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(63,	1,	NULL,	'2013-05-03 16:10:23',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(64,	1,	NULL,	'2013-05-04 12:28:18',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(65,	1,	NULL,	'2013-05-04 12:33:24',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(66,	2,	NULL,	'2013-05-04 12:33:40',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(67,	2,	NULL,	'2013-05-04 13:33:08',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(68,	2,	NULL,	'2013-05-04 13:33:17',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(69,	3,	NULL,	'2013-05-04 13:37:05',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(70,	3,	NULL,	'2013-05-04 14:11:28',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(71,	2,	NULL,	'2013-05-04 14:11:54',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(72,	2,	NULL,	'2013-05-04 14:53:09',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(73,	2,	NULL,	'2013-05-04 15:31:57',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(74,	NULL,	NULL,	'2013-05-04 15:35:35',	'ThesischatModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(75,	NULL,	NULL,	'2013-05-04 15:36:19',	'ThesischatModule\\Entities\\PageEntity',	13,	NULL,	'updated',	'Page has been published'),
(76,	2,	NULL,	'2013-05-04 16:19:41',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(77,	2,	NULL,	'2013-05-04 16:37:56',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(78,	3,	NULL,	'2013-05-04 16:44:19',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(79,	3,	NULL,	'2013-05-04 16:46:45',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(80,	2,	NULL,	'2013-05-04 16:47:02',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(81,	2,	NULL,	'2013-05-04 16:47:09',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(82,	NULL,	NULL,	'2013-05-09 15:35:42',	'PollModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(83,	NULL,	NULL,	'2013-05-09 15:36:23',	'PollModule\\Entities\\PageEntity',	14,	NULL,	'updated',	'Page has been published'),
(84,	NULL,	NULL,	'2013-05-09 15:36:44',	'PollModule\\Entities\\PageEntity',	14,	NULL,	'updated',	''),
(85,	NULL,	NULL,	'2013-05-09 23:14:05',	'PollModule\\Entities\\PageEntity',	14,	NULL,	'removed',	''),
(86,	NULL,	NULL,	'2013-05-10 11:44:09',	'PollModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(87,	NULL,	NULL,	'2013-05-10 11:45:52',	'PollModule\\Entities\\PageEntity',	14,	NULL,	'updated',	'Page has been published'),
(88,	2,	NULL,	'2013-05-10 13:55:29',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(89,	2,	NULL,	'2013-05-10 14:30:33',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(90,	2,	NULL,	'2013-05-10 14:39:21',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(91,	2,	NULL,	'2013-05-10 15:52:21',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(92,	2,	NULL,	'2013-05-10 16:34:45',	'Proxies\\__CG__\\CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(93,	3,	NULL,	'2013-05-10 16:35:34',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(94,	3,	NULL,	'2013-05-10 16:36:56',	'Proxies\\__CG__\\CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(95,	5,	NULL,	'2013-05-10 17:05:09',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(96,	5,	NULL,	'2013-05-11 11:18:19',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(97,	2,	NULL,	'2013-05-11 11:18:53',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(98,	2,	NULL,	'2013-05-11 13:33:55',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(99,	2,	NULL,	'2013-05-11 14:20:16',	'Proxies\\__CG__\\CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(100,	4,	NULL,	'2013-05-11 14:20:34',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(101,	4,	NULL,	'2013-05-11 14:21:18',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(102,	2,	NULL,	'2013-05-11 14:31:14',	'Proxies\\__CG__\\CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(103,	2,	NULL,	'2013-05-11 15:30:42',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(104,	4,	NULL,	'2013-05-11 15:31:09',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(105,	2,	NULL,	'2013-05-12 17:52:10',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(106,	2,	NULL,	'2013-05-12 18:05:08',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(107,	5,	NULL,	'2013-05-12 18:05:22',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(108,	5,	NULL,	'2013-05-12 18:07:01',	'Proxies\\__CG__\\ThesischatModule\\Entities\\PageEntity',	13,	NULL,	'removed',	''),
(109,	5,	14,	'2013-05-12 18:08:50',	'ThesischatModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(110,	5,	14,	'2013-05-12 18:08:58',	'ThesischatModule\\Entities\\PageEntity',	14,	NULL,	'updated',	'Page has been published'),
(111,	5,	NULL,	'2013-05-12 18:09:54',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(112,	2,	NULL,	'2013-05-12 18:10:10',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(113,	2,	NULL,	'2013-05-12 19:59:49',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(114,	2,	NULL,	'2013-05-12 22:16:36',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(115,	2,	NULL,	'2013-05-13 22:59:13',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(116,	5,	NULL,	'2013-05-13 22:59:48',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(117,	5,	NULL,	'2013-05-13 23:00:47',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(118,	2,	NULL,	'2013-05-13 23:01:03',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(119,	2,	NULL,	'2013-05-13 23:02:48',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(120,	5,	NULL,	'2013-05-13 23:03:01',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(121,	4,	NULL,	'2013-05-14 16:20:30',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(122,	4,	NULL,	'2013-05-15 18:32:53',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(123,	5,	NULL,	'2013-05-17 10:59:27',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(124,	5,	NULL,	'2013-05-17 14:09:04',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(125,	5,	NULL,	'2013-05-17 14:18:27',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(126,	5,	NULL,	'2013-05-17 14:58:15',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(127,	4,	NULL,	'2013-05-17 14:58:36',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(128,	4,	NULL,	'2013-05-17 16:55:09',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(129,	2,	NULL,	'2013-05-17 16:55:26',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(130,	2,	NULL,	'2013-05-17 16:56:33',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(131,	4,	NULL,	'2013-05-17 16:57:08',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(132,	4,	NULL,	'2013-05-17 16:58:08',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(133,	3,	NULL,	'2013-05-17 16:58:29',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(134,	3,	NULL,	'2013-05-17 16:58:43',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(135,	2,	NULL,	'2013-05-17 16:59:06',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(136,	2,	NULL,	'2013-05-17 17:02:45',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(137,	3,	NULL,	'2013-05-17 17:03:05',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(138,	3,	NULL,	'2013-05-17 17:05:09',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(139,	4,	NULL,	'2013-05-17 17:05:28',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(140,	4,	NULL,	'2013-05-17 17:11:25',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(141,	5,	NULL,	'2013-05-17 17:11:54',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(142,	5,	14,	'2013-05-17 17:17:41',	'ThesischatModule\\Entities\\PageEntity',	14,	NULL,	'updated',	''),
(143,	5,	2,	'2013-05-17 17:18:09',	'ActualityModule\\Entities\\PageEntity',	2,	NULL,	'updated',	''),
(144,	5,	NULL,	'2013-05-18 10:20:23',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(145,	5,	NULL,	'2013-05-18 10:27:18',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(146,	2,	NULL,	'2013-05-18 10:27:42',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(147,	2,	NULL,	'2013-05-18 10:31:00',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(148,	5,	NULL,	'2013-05-18 10:31:15',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(149,	5,	NULL,	'2013-05-18 10:55:47',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(150,	5,	NULL,	'2013-05-18 10:56:34',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(151,	5,	NULL,	'2013-05-18 11:16:40',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(152,	5,	NULL,	'2013-05-18 11:41:27',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(153,	4,	NULL,	'2013-05-18 11:41:50',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(154,	4,	NULL,	'2013-05-18 12:37:05',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(155,	5,	NULL,	'2013-05-19 18:12:32',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(160,	5,	NULL,	'2013-05-21 18:24:27',	'ThesischatModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(161,	5,	NULL,	'2013-05-21 18:47:09',	'ThesischatModule\\Entities\\PageEntity',	17,	NULL,	'updated',	'Page has been published'),
(162,	5,	NULL,	'2013-05-21 19:59:42',	'Proxies\\__CG__\\ThesischatModule\\Entities\\PageEntity',	17,	NULL,	'removed',	''),
(163,	5,	NULL,	'2013-05-21 20:01:44',	'ThesischatModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(164,	5,	NULL,	'2013-05-21 20:01:49',	'ThesischatModule\\Entities\\PageEntity',	18,	NULL,	'updated',	'Page has been published'),
(165,	5,	NULL,	'2013-05-22 13:54:24',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(166,	4,	NULL,	'2013-05-22 13:55:24',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(167,	4,	NULL,	'2013-05-22 14:39:29',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(168,	4,	NULL,	'2013-05-22 15:59:59',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(169,	2,	NULL,	'2013-05-22 16:00:15',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(170,	2,	NULL,	'2013-05-22 16:09:49',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(171,	5,	NULL,	'2013-05-22 16:10:19',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(172,	5,	NULL,	'2013-05-22 16:40:58',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(173,	4,	NULL,	'2013-05-22 16:41:13',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(174,	4,	NULL,	'2013-05-23 23:22:55',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(175,	5,	NULL,	'2013-05-23 23:23:09',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(176,	2,	NULL,	'2013-05-25 10:39:12',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(177,	2,	NULL,	'2013-05-25 10:42:10',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(178,	4,	NULL,	'2013-05-25 10:42:35',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(179,	4,	NULL,	'2013-05-25 10:45:28',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(180,	2,	NULL,	'2013-05-25 10:45:49',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(181,	2,	NULL,	'2013-05-25 11:47:29',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(182,	5,	NULL,	'2013-05-25 11:47:49',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(183,	5,	NULL,	'2013-05-25 11:52:21',	'Proxies\\__CG__\\ThesischatModule\\Entities\\PageEntity',	18,	NULL,	'removed',	''),
(184,	5,	19,	'2013-05-25 12:17:43',	'ThesischatModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(185,	5,	19,	'2013-05-25 12:18:12',	'ThesischatModule\\Entities\\PageEntity',	19,	NULL,	'updated',	''),
(186,	5,	19,	'2013-05-25 12:18:17',	'ThesischatModule\\Entities\\PageEntity',	19,	NULL,	'updated',	'Page has been published'),
(187,	5,	NULL,	'2013-05-25 15:54:41',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(188,	5,	NULL,	'2013-05-28 15:04:14',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(189,	5,	NULL,	'2013-05-28 15:05:02',	'ThesisarchiveModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(190,	5,	NULL,	'2013-05-28 15:40:54',	'ThesisarchiveModule\\Entities\\PageEntity',	20,	NULL,	'updated',	'Page has been published'),
(193,	5,	12,	'2013-05-30 15:19:26',	'CmsModule\\Content\\Entities\\RegistrationPageEntity',	12,	NULL,	'updated',	''),
(194,	5,	19,	'2013-05-30 15:19:26',	'ThesischatModule\\Entities\\PageEntity',	19,	NULL,	'updated',	''),
(195,	5,	NULL,	'2013-05-30 15:19:26',	'CmsModule\\Content\\Entities\\StaticPageEntity',	7,	NULL,	'removed',	''),
(196,	5,	21,	'2013-05-30 15:19:46',	'ThesisarchiveModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(197,	5,	21,	'2013-05-30 15:19:55',	'ThesisarchiveModule\\Entities\\PageEntity',	21,	NULL,	'updated',	'Page has been published'),
(198,	5,	NULL,	'2013-05-30 20:58:30',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(199,	1,	NULL,	'2013-05-30 20:58:52',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(200,	1,	NULL,	'2013-05-30 20:59:02',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(201,	5,	NULL,	'2013-05-30 21:06:23',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(202,	5,	NULL,	'2013-05-31 09:32:11',	'Proxies\\__CG__\\CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(203,	4,	NULL,	'2013-05-31 09:32:33',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(204,	4,	NULL,	'2013-05-31 09:36:18',	'Proxies\\__CG__\\CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(205,	5,	NULL,	'2013-05-31 09:36:32',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(206,	5,	NULL,	'2013-05-31 10:47:13',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(207,	2,	NULL,	'2013-05-31 10:47:46',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(208,	2,	NULL,	'2013-05-31 11:00:09',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(209,	1,	NULL,	'2013-05-31 11:00:51',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(210,	1,	NULL,	'2013-05-31 11:06:45',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(211,	4,	NULL,	'2013-05-31 11:07:01',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(212,	4,	NULL,	'2013-05-31 11:26:14',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(213,	4,	NULL,	'2013-05-31 11:27:56',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(214,	4,	NULL,	'2013-05-31 11:56:42',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(215,	2,	NULL,	'2013-05-31 12:02:45',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(216,	2,	NULL,	'2013-05-31 12:03:41',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(217,	2,	NULL,	'2013-05-31 12:06:46',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(218,	2,	NULL,	'2013-06-01 15:24:42',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(219,	4,	NULL,	'2013-06-01 15:25:03',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(220,	4,	NULL,	'2013-06-01 15:55:50',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(221,	2,	NULL,	'2013-06-01 15:56:14',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(222,	2,	NULL,	'2013-06-01 15:58:43',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(223,	3,	NULL,	'2013-06-01 15:59:08',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(224,	3,	NULL,	'2013-06-01 16:00:57',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(225,	5,	NULL,	'2013-06-01 16:01:11',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(226,	5,	22,	'2013-06-01 16:28:47',	'ThesisarchiveModule\\Entities\\PrearchivePageEntity',	NULL,	NULL,	'created',	''),
(227,	5,	20,	'2013-06-01 16:55:25',	'ThesisarchiveModule\\Entities\\PageEntity',	NULL,	NULL,	'created',	''),
(228,	5,	20,	'2013-06-01 16:55:45',	'ThesisarchiveModule\\Entities\\PageEntity',	20,	NULL,	'updated',	'Page has been published'),
(231,	5,	NULL,	'2013-06-03 18:15:30',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	NULL,	NULL,	'created',	''),
(232,	5,	NULL,	'2013-06-03 18:16:19',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	NULL,	NULL,	'created',	''),
(233,	5,	NULL,	'2013-06-03 18:16:43',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	NULL,	NULL,	'created',	''),
(234,	5,	NULL,	'2013-06-03 18:22:24',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	24,	NULL,	'updated',	''),
(235,	5,	NULL,	'2013-06-03 18:22:24',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	25,	NULL,	'updated',	''),
(236,	5,	NULL,	'2013-06-03 18:22:24',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	23,	NULL,	'removed',	''),
(237,	5,	NULL,	'2013-06-03 18:22:57',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	25,	NULL,	'updated',	''),
(238,	5,	NULL,	'2013-06-03 18:22:57',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	24,	NULL,	'removed',	''),
(239,	5,	NULL,	'2013-06-03 18:23:05',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	25,	NULL,	'removed',	''),
(240,	5,	26,	'2013-06-03 18:23:19',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	NULL,	NULL,	'created',	''),
(241,	5,	26,	'2013-06-03 23:19:28',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	26,	NULL,	'updated',	'Page has been published'),
(245,	5,	23,	'2013-06-04 18:43:45',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	NULL,	NULL,	'created',	''),
(246,	5,	24,	'2013-06-04 18:44:08',	'ThesisarchiveModule\\Entities\\ArchivepageEntity',	NULL,	NULL,	'created',	''),
(247,	5,	23,	'2013-06-04 18:44:55',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	23,	NULL,	'updated',	''),
(248,	5,	23,	'2013-06-04 18:45:00',	'ThesisarchiveModule\\Entities\\PrearchivepageEntity',	23,	NULL,	'updated',	'Page has been published'),
(249,	5,	24,	'2013-06-04 18:45:13',	'ThesisarchiveModule\\Entities\\ArchivepageEntity',	24,	NULL,	'updated',	'Page has been published'),
(250,	5,	NULL,	'2013-06-06 12:48:20',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(251,	2,	NULL,	'2013-06-06 12:48:37',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(252,	2,	NULL,	'2013-06-06 14:41:29',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(253,	4,	NULL,	'2013-06-06 14:41:56',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(254,	4,	NULL,	'2013-06-06 14:49:51',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(255,	2,	NULL,	'2013-06-06 14:50:14',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(256,	2,	NULL,	'2013-06-06 14:54:13',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(257,	4,	NULL,	'2013-06-06 14:54:34',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged in'),
(258,	4,	NULL,	'2013-06-06 16:42:11',	'CmsModule\\Security\\Entities\\UserEntity',	4,	NULL,	'other',	'User has been logged out'),
(259,	1,	NULL,	'2013-06-06 16:42:35',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(260,	1,	NULL,	'2013-06-07 00:31:46',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(261,	5,	NULL,	'2013-06-07 00:32:03',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(262,	5,	NULL,	'2013-06-07 09:49:04',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(263,	3,	NULL,	'2013-06-07 09:49:20',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(264,	3,	NULL,	'2013-06-07 09:50:25',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(265,	5,	NULL,	'2013-06-07 09:50:42',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(266,	5,	NULL,	'2013-06-07 10:18:16',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(267,	2,	NULL,	'2013-06-07 10:18:36',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(268,	2,	NULL,	'2013-06-07 10:19:34',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(269,	5,	NULL,	'2013-06-07 10:19:53',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(270,	5,	NULL,	'2013-06-07 10:40:54',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(271,	2,	NULL,	'2013-06-07 10:41:08',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(272,	2,	NULL,	'2013-06-07 10:42:06',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(273,	5,	NULL,	'2013-06-07 10:42:22',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(274,	5,	NULL,	'2013-06-07 14:03:30',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(275,	2,	NULL,	'2013-06-07 14:11:04',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(276,	2,	NULL,	'2013-06-07 14:31:59',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(277,	5,	NULL,	'2013-06-07 14:32:15',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(278,	5,	NULL,	'2013-06-07 15:11:43',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(279,	2,	NULL,	'2013-06-07 15:12:01',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(280,	2,	NULL,	'2013-06-07 16:01:17',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(281,	5,	NULL,	'2013-06-07 16:01:36',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(282,	5,	NULL,	'2013-06-07 16:06:45',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(283,	2,	NULL,	'2013-06-07 16:07:07',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(284,	2,	NULL,	'2013-06-07 16:09:31',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(285,	5,	NULL,	'2013-06-07 16:09:50',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(286,	5,	NULL,	'2013-06-07 16:16:38',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(287,	2,	NULL,	'2013-06-07 16:16:54',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(288,	2,	NULL,	'2013-06-07 16:28:13',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(289,	5,	NULL,	'2013-06-07 16:28:31',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged in'),
(290,	5,	NULL,	'2013-06-07 16:29:59',	'CmsModule\\Security\\Entities\\UserEntity',	5,	NULL,	'other',	'User has been logged out'),
(291,	3,	NULL,	'2013-06-07 16:30:15',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(292,	3,	NULL,	'2013-06-07 16:32:20',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(293,	2,	NULL,	'2013-06-07 16:32:37',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(294,	2,	NULL,	'2013-06-07 17:16:34',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged out'),
(295,	3,	NULL,	'2013-06-07 17:16:54',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged in'),
(296,	3,	NULL,	'2013-06-07 18:02:43',	'CmsModule\\Security\\Entities\\UserEntity',	3,	NULL,	'other',	'User has been logged out'),
(297,	1,	NULL,	'2013-06-07 18:03:32',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged in'),
(298,	1,	NULL,	'2013-06-07 18:16:11',	'CmsModule\\Security\\Entities\\UserEntity',	1,	NULL,	'other',	'User has been logged out'),
(299,	2,	NULL,	'2013-06-07 18:16:25',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in'),
(300,	2,	NULL,	'2013-06-08 15:58:05',	'CmsModule\\Security\\Entities\\UserEntity',	2,	NULL,	'other',	'User has been logged in');

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sessionId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reload` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AA08CB10A76ED395` (`user_id`),
  KEY `search_idx` (`sessionId`),
  CONSTRAINT `FK_AA08CB10A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `loginpage`;
CREATE TABLE `loginpage` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8701F7DEC4663E4` (`page_id`),
  KEY `IDX_8701F7DE833D8F43` (`registration_id`),
  CONSTRAINT `FK_8701F7DE833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `registrationpage` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_8701F7DEBF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8701F7DEC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `previous_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `navigationTitleRaw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `navigationShow` tinyint(1) NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `translationFor` int(11) DEFAULT NULL,
  `virtualParent_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_140AB620389B783` (`tag`),
  KEY `IDX_140AB62034ECB4E6` (`route_id`),
  KEY `IDX_140AB6202DD3A10D` (`translationFor`),
  KEY `IDX_140AB620727ACA70` (`parent_id`),
  KEY `IDX_140AB6202DE62210` (`previous_id`),
  KEY `IDX_140AB620D5A6A541` (`virtualParent_id`),
  CONSTRAINT `FK_140AB6202DD3A10D` FOREIGN KEY (`translationFor`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_140AB6202DE62210` FOREIGN KEY (`previous_id`) REFERENCES `page` (`id`),
  CONSTRAINT `FK_140AB62034ECB4E6` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_140AB620727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_140AB620D5A6A541` FOREIGN KEY (`virtualParent_id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `page` (`id`, `route_id`, `parent_id`, `previous_id`, `name`, `created`, `updated`, `navigationTitleRaw`, `navigationShow`, `tag`, `published`, `position`, `translationFor`, `virtualParent_id`, `type`) VALUES
(1,	1,	NULL,	NULL,	'vývoj',	'2013-04-22 10:47:59',	'2013-04-22 10:47:59',	NULL,	1,	NULL,	1,	1,	NULL,	NULL,	'staticPage'),
(2,	2,	1,	NULL,	'aktuality',	'2013-04-22 10:54:41',	'2013-04-22 10:54:41',	NULL,	1,	NULL,	1,	1,	NULL,	NULL,	'actualityPage'),
(3,	3,	1,	2,	'zadání prací',	'2013-04-22 10:56:44',	'2013-04-22 10:56:44',	NULL,	1,	NULL,	1,	2,	NULL,	NULL,	'thesiscollectionPage'),
(4,	4,	1,	3,	'agenda',	'2013-04-22 11:08:06',	'2013-04-22 11:08:06',	NULL,	1,	NULL,	1,	3,	NULL,	NULL,	'agendaPage'),
(5,	5,	1,	4,	'submenu',	'2013-04-22 11:08:35',	'2013-04-22 11:08:35',	NULL,	1,	NULL,	1,	4,	NULL,	NULL,	'staticPage'),
(6,	6,	5,	NULL,	'položka A',	'2013-04-22 11:10:00',	'2013-04-22 11:10:00',	NULL,	1,	NULL,	1,	1,	NULL,	NULL,	'staticPage'),
(8,	8,	5,	6,	'položka B',	'2013-04-22 11:11:47',	'2013-04-22 11:11:47',	NULL,	1,	NULL,	1,	2,	NULL,	NULL,	'staticPage'),
(9,	9,	5,	8,	'položka C',	'2013-04-22 11:12:27',	'2013-04-22 11:12:27',	NULL,	1,	NULL,	1,	3,	NULL,	NULL,	'staticPage'),
(10,	10,	8,	NULL,	'položka B1',	'2013-04-22 11:13:13',	'2013-04-22 11:13:13',	NULL,	1,	NULL,	1,	1,	NULL,	NULL,	'staticPage'),
(11,	11,	8,	10,	'položka B2',	'2013-04-22 11:13:45',	'2013-04-22 11:13:45',	NULL,	1,	NULL,	1,	2,	NULL,	NULL,	'staticPage'),
(12,	13,	1,	5,	'registráce',	'2013-05-03 15:06:15',	'2013-05-03 15:06:15',	NULL,	0,	NULL,	1,	5,	NULL,	NULL,	'registrationPage'),
(19,	28,	1,	12,	'průběh práce/prací',	'2013-05-25 12:17:42',	'2013-05-25 12:17:42',	NULL,	1,	NULL,	1,	6,	NULL,	NULL,	'thesischatPage'),
(23,	33,	1,	19,	'k archivaci',	'2013-06-04 18:43:45',	'2013-06-04 18:43:45',	NULL,	1,	NULL,	1,	7,	NULL,	NULL,	'thesisprearchivePage'),
(24,	34,	1,	23,	'archiv prací',	'2013-06-04 18:44:08',	'2013-06-04 18:44:08',	NULL,	1,	NULL,	1,	8,	NULL,	NULL,	'thesisarchivePage');

DROP TABLE IF EXISTS `pagelanguagelink`;
CREATE TABLE `pagelanguagelink` (
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`language_id`),
  KEY `IDX_5B43B76DC4663E4` (`page_id`),
  KEY `IDX_5B43B76D82F1BAF4` (`language_id`),
  CONSTRAINT `FK_5B43B76D82F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_5B43B76DC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pagelanguagelink` (`page_id`, `language_id`) VALUES
(1,	1),
(2,	1),
(3,	1),
(4,	1),
(5,	1),
(6,	1),
(8,	1),
(9,	1),
(10,	1),
(11,	1),
(12,	1),
(14,	1),
(15,	1),
(16,	1),
(19,	1),
(20,	1),
(21,	1),
(22,	1),
(23,	1),
(24,	1),
(26,	1);

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `resource` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `privilege` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E04992AAD60322AC` (`role_id`),
  CONSTRAINT `FK_E04992AAD60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `redirectpage`;
CREATE TABLE `redirectpage` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `redirectUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9A5E78DDC4663E4` (`page_id`),
  CONSTRAINT `FK_9A5E78DDBF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_9A5E78DDC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `registrationpage`;
CREATE TABLE `registrationpage` (
  `id` int(11) NOT NULL,
  `userType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `socialMode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailFrom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_7DD145D2BF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `registrationpage` (`id`, `userType`, `mode`, `socialMode`, `email`, `sender`, `mailFrom`, `subject`) VALUES
(12,	'\\CmsModule\\Security\\Entities\\UserEntity',	'basic',	'load&save',	'<p>Thank your for your registration.</p>\r\n			<p>Your registration informations:</p>\r\n\r\n			<strong>E-mail:</strong> {$email}<br />\r\n			<strong>Password:</strong> {$password}\r\n\r\n			<p>\r\n				Please activate your account here: {$link}\r\n			</p>',	'',	'',	'');

DROP TABLE IF EXISTS `registrationpageentity_roleentity`;
CREATE TABLE `registrationpageentity_roleentity` (
  `registrationpageentity_id` int(11) NOT NULL,
  `roleentity_id` int(11) NOT NULL,
  PRIMARY KEY (`registrationpageentity_id`,`roleentity_id`),
  KEY `IDX_825AF760D3DF8570` (`registrationpageentity_id`),
  KEY `IDX_825AF7608536DE0C` (`roleentity_id`),
  CONSTRAINT `FK_825AF7608536DE0C` FOREIGN KEY (`roleentity_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_825AF760D3DF8570` FOREIGN KEY (`registrationpageentity_id`) REFERENCES `registrationpage` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `registrationpageentity_roleentity` (`registrationpageentity_id`, `roleentity_id`) VALUES
(12,	3);

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_57698A6A5E237E06` (`name`),
  KEY `IDX_57698A6AD60322AC` (`role_id`),
  CONSTRAINT `FK_57698A6AD60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `role` (`id`, `role_id`, `name`) VALUES
(1,	NULL,	'studijni'),
(2,	NULL,	'vedouci'),
(3,	NULL,	'student'),
(4,	NULL,	'garant'),
(5,	NULL,	'admin');

DROP TABLE IF EXISTS `route`;
CREATE TABLE `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `localUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paramCounter` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `robots` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `changefreq` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `copyLayoutFromParent` tinyint(1) NOT NULL,
  `cacheMode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyCacheModeFromParent` tinyint(1) NOT NULL,
  `copyLayoutToChildren` tinyint(1) NOT NULL,
  `childrenLayout_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2C42079C4663E4` (`page_id`),
  KEY `IDX_2C4207934ECB4E6` (`route_id`),
  KEY `IDX_2C420798C22AA1A` (`layout_id`),
  KEY `IDX_2C4207931EA3E7A` (`childrenLayout_id`),
  KEY `type_idx` (`type`),
  KEY `url_idx` (`url`),
  CONSTRAINT `FK_2C4207931EA3E7A` FOREIGN KEY (`childrenLayout_id`) REFERENCES `layout` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_2C4207934ECB4E6` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_2C420798C22AA1A` FOREIGN KEY (`layout_id`) REFERENCES `layout` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_2C42079C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `route` (`id`, `page_id`, `route_id`, `layout_id`, `type`, `url`, `localUrl`, `params`, `paramCounter`, `published`, `title`, `keywords`, `description`, `author`, `robots`, `changefreq`, `priority`, `copyLayoutFromParent`, `cacheMode`, `copyCacheModeFromParent`, `copyLayoutToChildren`, `childrenLayout_id`) VALUES
(1,	1,	NULL,	2,	'Cms:Static:default',	'',	'vyvoj',	'[]',	0,	1,	'vývoj',	'',	'',	'',	'',	NULL,	NULL,	0,	'default',	1,	1,	2),
(2,	2,	1,	2,	'Actuality:Default:default',	'aktuality',	'aktuality',	'[]',	0,	1,	'aktuality',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(3,	3,	1,	2,	'Thesiscollection:Default:default',	'zadani-praci',	'zadani-praci',	'[]',	0,	1,	'zadání prací',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(4,	4,	1,	2,	'Agenda:Default:default',	'agenda',	'agenda',	'[]',	0,	1,	'agenda',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(5,	5,	1,	2,	'Cms:Static:default',	'submenu',	'submenu',	'[]',	0,	1,	'submenu',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(6,	6,	5,	2,	'Cms:Static:default',	'submenu/polozka-a',	'polozka-a',	'[]',	0,	1,	'položka A',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(8,	8,	5,	2,	'Cms:Static:default',	'submenu/polozka-b',	'polozka-b',	'[]',	0,	1,	'položka B',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(9,	9,	5,	2,	'Cms:Static:default',	'submenu/polozka-c',	'polozka-c',	'[]',	0,	1,	'položka C',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(10,	10,	8,	2,	'Cms:Static:default',	'submenu/polozka-b/polozka-b1',	'polozka-b1',	'[]',	0,	1,	'položka B1',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(11,	11,	8,	2,	'Cms:Static:default',	'submenu/polozka-b/polozka-b2',	'polozka-b2',	'[]',	0,	1,	'položka B2',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(13,	12,	1,	2,	'Cms:Registration:default',	'registrace',	'registrace',	'[]',	0,	1,	'registrace',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(28,	19,	1,	2,	'Thesischat:Default:default',	'prubeh-prace-praci',	'prubeh-prace-praci',	'[]',	0,	1,	'průběh práce/prací',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(29,	19,	28,	2,	'Thesischat:Notification:default',	'prubeh-prace-praci/notification.js',	'notification.js',	'[]',	0,	1,	'Notification',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(33,	23,	1,	2,	'Thesisprearchive:Defaultpre:default',	'k-archivaci',	'k-archivaci',	'[]',	0,	1,	'k archivaci',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(34,	24,	1,	2,	'Thesisarchive:Default:default',	'archiv-praci',	'archiv-praci',	'[]',	0,	1,	'archiv prací',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(67,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/do-archivu',	'do-archivu',	'[]',	0,	1,	'do archivu!',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(68,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/finale',	'finale',	'[]',	0,	1,	'finále',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(70,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/je-pozde',	'je-pozde',	'[]',	0,	1,	'je pozdě',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(72,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/regsf-sdf-sdf',	'regsf-sdf-sdf',	'[]',	0,	1,	'regsf sdf sdf',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(74,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/ahoj',	'ahoj',	'[]',	0,	1,	'ahoj',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(76,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/aaaaa',	'aaaaa',	'[]',	0,	1,	'aaaaa',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(78,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/monitoring-stavu-hladiny-becvy',	'monitoring-stavu-hladiny-becvy',	'[]',	0,	1,	'Monitoring stavu hladiny Bečvy',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(79,	24,	34,	2,	'Thesisarchive:Thesisarchive:default',	'archiv-praci/jmeno-prace',	'jmeno-prace',	'[]',	0,	1,	'jméno práce',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(80,	3,	3,	2,	'Thesiscollection:Thesiscollection:default',	'zadani-praci/dgsdfg',	'dgsdfg',	'[]',	0,	1,	'dgsdfg',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2),
(81,	3,	3,	2,	'Thesiscollection:Thesiscollection:default',	'zadani-praci/pro-pokukusa',	'pro-pokukusa',	'[]',	0,	1,	'pro pokukusa',	'',	'',	'',	'',	NULL,	NULL,	1,	'default',	1,	1,	2);

DROP TABLE IF EXISTS `sociallogin`;
CREATE TABLE `sociallogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uniqueKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqueKey` (`type`,`uniqueKey`),
  KEY `IDX_79FD476BA76ED395` (`user_id`),
  CONSTRAINT `FK_79FD476BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `staticpage`;
CREATE TABLE `staticpage` (
  `id` int(11) NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_33C80524BF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `staticpage` (`id`, `text`) VALUES
(1,	'<div>\r\n	<h2>Použité technologie</h2>\r\n	<ul class=\"no-idents\">\r\n		<li><b>Serverová část</b>\r\n			<ul>\r\n				<li>server httpd Apache <span class=\"version\">2.2.15</span> - <a href=\"http://httpd.apache.org/\">http://httpd.apache.org/</a></li>\r\n				<li>PHP <span class=\"version\">5.4.13</span> - <a href=\"http://www.php.net/\">http://www.php.net/</a> (minimální potřebná verze je <span class=\"version\">5.3.6</span> pro funkci <code>SplFileInfo::getExtension</code> - <a href=\"http://www.php.net/manual/en/splfileinfo.getextension.php\">http://www.php.net/manual/en/splfileinfo.getextension.php</a>)\r\n					<ul>\r\n						<li><b>nutná rozšíření PHP:</b></li>\r\n						<li>Multibyte String extension - <a href=\"http://php.net/manual/en/book.mbstring.php\">http://php.net/manual/en/book.mbstring.php</a></li>\r\n						<li>PDO extension - <a href=\"http://php.net/manual/en/book.pdo.php\">http://php.net/manual/en/book.pdo.php</a></li>\r\n					</ul>\r\n				</li>\r\n				<li>MySQL <span class=\"version\">5.5.29</span> - <a href=\"http://www.mysql.com/\">http://www.mysql.com/</a> (není nutné použít právě mysql, ale je možné použít další populární alternativy jako Oracle, PostgreSQL, MSSQL, Sqlite, … při instalaci pak stačí jediné, nastavit jiný ovladač databáze)</li>\r\n				<li>Doctrine <span class=\"version\">2.3.3</span> - <a href=\"http://www.doctrine-project.org/\">http://www.doctrine-project.org/</a></li>\r\n				<li>Nette Framework <span class=\"version\">2.0.10</span> - <a href=\"http://nette.org\">http://www.php.net/</a></li>\r\n				<li>Venne CMS <span class=\"version\">2.0.0</span> - <a href=\"http://venne.github.io/\">http://venne.github.io/</a>\r\n					<ul>\r\n						<li><b>použité moduly (možnost instalace přes Composer):</b></li>\r\n						<li>venne/assets-module</li>\r\n						<li>venne/cms-module</li>\r\n						<li>venne/doctrine-module</li>\r\n						<li>venne/forms-module</li>\r\n						<li>venne/framework</li>\r\n						<li>venne/gedmo-module</li>\r\n						<li>venne/googleanalytics-module</li>\r\n						<li>venne/less-module</li>\r\n						<li>ic/appearanice-module</li>\r\n						<li><i>(ostatní moduly, které jsou součástí práce není možné volně stáhnou na internetu)</i></li>\r\n					</ul>\r\n				</li>\r\n				<li>Lessphp <span class=\"version\">0.3.9</span> - <a href=\"http://leafo.net/lessphp/\">http://leafo.net/lessphp/</a> (postavené na projektu Less - <a href=\"http://lesscss.org/\">http://lesscss.org/</a>)</li>\r\n			</ul>\r\n		</li>\r\n		\r\n		<li><b>Klientská část</b>\r\n			<ul>\r\n				<li>HTML5 <span class=\"version\">5.1</span> - <a href=\"http://dev.w3.org/html5/spec/\">http://dev.w3.org/html5/spec/</a></li>\r\n				<li>CSS <span class=\"version\">Level 3</span> - <a href=\"http://www.w3.org/Style/CSS/\">http://www.w3.org/Style/CSS/</a></li>\r\n				<li>ECMAscript (javascript) <span class=\"version\">edition 5</span> - <a href=\"http://www.ecmascript.org/\">http://www.ecmascript.org/</a></li>\r\n				<li>html5shiv <span class=\"version\">3.6.2</span> - <a href=\"https://github.com/aFarkas/html5shiv\">https://github.com/aFarkas/html5shiv</a></li>\r\n				<li>jQuery <span class=\"version\">1.8.2</span> - <a href=\"http://jquery.com/\">http://jquery.com/</a></li>\r\n				<li>normalize.css <span class=\"version\">2.0.1</span> - <a href=\"http://necolas.github.io/normalize.css/\">http://necolas.github.io/normalize.css/</a></li>\r\n				<li>Glyphicons (Bootstrap version) - <a href=\"http://glyphicons.com/\">http://glyphicons.com/</a></li>\r\n			</ul>\r\n		</li>\r\n		\r\n		<li><b>Nástroje použité pro vývoj</b>\r\n			<ul>\r\n				<li>NetBeans IDE <span class=\"version\">7.3</span> - <a href=\"https://netbeans.org/\">https://netbeans.org/</a>\r\n					<ul>\r\n						<li><b>pluginy:</b></li>\r\n						<li>PHP Nette Framework - <a href=\"http://plugins.netbeans.org/plugin/32720/\">http://plugins.netbeans.org/plugin/32720/</a></li>\r\n					</ul>\r\n				</li>\r\n				<li>PSPad <span class=\"version\">4.5.7</span> - <a href=\"https://pspad.com/\">https://pspad.com/</a></li>\r\n				<li>GitHub for Windows <span class=\"version\">1.0.41.2</span> - <a href=\"http://windows.github.com/\">http://windows.github.com/</a> (má v sobě instalaci GITu)</li>\r\n				<li>XAMPP <span class=\"version\">1.8.1</span> - <a href=\"http://www.apachefriends.org/en/xampp.html\">http://www.apachefriends.org/en/xampp.html</a></li>\r\n				<li>Composer <span class=\"version\">1.0.0-alpha6</span> - <a href=\"http://getcomposer.org/\">http://getcomposer.org/</a></li>\r\n				<li>WinSCP <span class=\"version\">5.1.4</span> - <a href=\"https://winscp.net/\">https://winscp.net/</a></li>\r\n				<li>PuTTY <span class=\"version\">0.62</span> - <a href=\"http://www.putty.org/\">http://www.putty.org/</a></li>\r\n				<li>Google Chrome <span class=\"version\">26.0.1410.64 m</span> - <a href=\"http://www.google.com/intl/cs/chrome/browser/\">http://www.google.com/intl/cs/chrome/browser/</a>\r\n					<ul>\r\n						<li>konzola \'Nástroje pro vývojáře\'</li>\r\n						<li>rozšíření (un)clrd - <a href=\"https://chrome.google.com/webstore/detail/unclrd/pjahllgfmfgobhbkjiaohonjejpnkfkh\">https://chrome.google.com/webstore/detail/unclrd/pjahllgfmfgobhbkjiaohonjejpnkfkh</a></li>\r\n						<li>rozšíření Dev HTTP Client - <a href=\"https://chrome.google.com/webstore/detail/dev-http-client/aejoelaoggembcahagimdiliamlcdmfm\">https://chrome.google.com/webstore/detail/dev-http-client/aejoelaoggembcahagimdiliamlcdmfm</a></li>\r\n						<li>rozšíření Edit This Cookie - <a href=\"https://chrome.google.com/webstore/detail/edit-this-cookie/fngmhnnpilhplaeedifhccceomclgfbg\">https://chrome.google.com/webstore/detail/edit-this-cookie/fngmhnnpilhplaeedifhccceomclgfbg</a></li>\r\n						<li>rozšíření Microformats for Google Chrome™ - <a href=\"https://chrome.google.com/webstore/detail/microformats-for-google-c/oalbifknmclbnmjlljdemhjjlkmppjjl\">https://chrome.google.com/webstore/detail/microformats-for-google-c/oalbifknmclbnmjlljdemhjjlkmppjjl</a></li>\r\n						<li>rozšíření Web Developer - <a href=\"https://chrome.google.com/webstore/detail/web-developer/bfbameneiokkgbdmiekhjnmfkcnldhhm\">https://chrome.google.com/webstore/detail/web-developer/bfbameneiokkgbdmiekhjnmfkcnldhhm</a></li>\r\n					</ul>\r\n				</li>\r\n			</ul>\r\n		</li>\r\n		\r\n		<li><b>Správné zobrazení testováno na prohlížečích</b>\r\n			<ul>\r\n				<li>Google Chrome <span class=\"version\">26.0.1410.64 m</span> - <a href=\"http://www.google.com/intl/cs/chrome/browser/\">http://www.google.com/intl/cs/chrome/browser/</a></li>\r\n				<li>Mozilla Firefox <span class=\"version\">20.0.1</span> - <a href=\"http://www.mozilla.org/cs/firefox\">http://www.mozilla.org/cs/firefox</a></li>\r\n				<li>Internet Explorer <span class=\"version\">10.0.9200.16540</span> - <a href=\"http://windows.microsoft.com/cs-cz/internet-explorer/download-ie\">http://windows.microsoft.com/cs-cz/internet-explorer/download-ie</a></li>\r\n				<li>Opera <span class=\"version\">12.15</span> - <a href=\"http://www.opera.com/cs/\">http://www.opera.com/cs/</a></li>\r\n				<li>Maxthon cloud browser <span class=\"version\">4.0.5.4000</span> - <a href=\"www.maxthon.com/\">www.maxthon.com/</a></li>\r\n			</ul>\r\n		</li>	\r\n	</ul>\r\n</div>\r\n\r\n<hr>\r\n\r\n<div>\r\n	<h2>Postup instalace (z linuxového serveru s podporou PHP)</h2>\r\n	<ul>\r\n		<li><b>Test konfigurace webového serveru</b>\r\n			<ul>\r\n				<li>Stáhnout nástroj Nette Requirements Checker - <a href=\"https://github.com/nette/tools/tree/master/Requirements-Checker\">https://github.com/nette/tools/tree/master/Requirements-Checker</a> a umísti ho do konřenového adresáře webu.</li>\r\n				<li>Z prohlížeče zobrazit URL adresu, na které je umístěn soubor <code>checker.php</code></li>\r\n				<li>Opravit případné chyby</li>\r\n			</ul>\r\n		</li>\r\n		<li><b>Instalace Venne CMS</b>\r\n			<ul>\r\n				<li>Nainstalovat nástroj composer (<a href=\"http://getcomposer.org/download/\">http://getcomposer.org/download/</a>), provést jeho update příkazem <code>composer self-update</code></li>\r\n				<li>Přepnout se do složky, do které zamýšlím instalovat CMS (například <code>cd /var/www</code>)</li>\r\n				<li><b>Instalovat Venne CMS dle doporučeného zadání:</b>\r\n					<ul>\r\n						<li><code>composer create-project venne/sandbox:2.0.x-dev myApp</code></li>\r\n						<li><code>cd myApp</code></li>\r\n						<li><code>composer require venne/cms-module:2.0.x</code></li>\r\n						<li><code>php www/index.php venne:module:update</code> (pro poslední 2 příkazy musí být už zapnutý php preprocesor)</li>\r\n						<li><code>php www/index.php venne:module:install cms</code></li>\r\n					</ul>\r\n				</li>\r\n			</ul>\r\n		</li>\r\n		<li><b>Instalace modulů vytvořených speciálně pro dané zadání</b>\r\n			<ul>\r\n				<li><b>Instalace frontend layoutu se provádí pomocí příkazů composeru</b>\r\n					<ul>\r\n						<li><code>composer require ic/appearanice-module:1.0.x</code></li>\r\n						<li><code>php www/index.php venne:module:update</code></li>\r\n						<li><code>php www/index.php venne:module:install appearanice</code></li>\r\n					</ul>\r\n				</li>\r\n				<li>Ostatní moduly se manuálně zkopírují do složky <code>vendor/venne/</code></li>\r\n			</ul>\r\n		</li>\r\n		<li><b>Konfigurace projektu přes prohlížeč</b>\r\n			<ul>\r\n				<li><b>Pro správnou instalaci Venne je nutné nastavit možnost zápisu do některých složek (chmod 777):</b>\r\n					<ul>\r\n						<li><code>app/config</code></li>\r\n						<li><code>app/data</code></li>\r\n						<li><code>app/temp</code></li>\r\n						<li><code>www/cache</code></li>\r\n						<li><code>www/public</code></li>\r\n						<li><code>www/resources</code></li>\r\n					</ul>\r\n				</li>\r\n				<li>Z prohlížrče navštívit adresu, která slouží jako index daného projektu</li>\r\n				<li>Pokus se vyskytnou nějaké chyby dá se zjistit, co se stalo editací souboru <code>www/index.php</code>, kde zapnu debug mode vložením <code>$configurator->setDebugMode(TRUE);</code> mezi řádky 12 a 13. (Jedním z problémů může být bug Composeru, který způsobuje, že soubory jsou nepřístupné. Pomůže je přesunout na lokální disk, a následně vrátit zpět na server.)</li>\r\n				<li>V dalším kroku je nutné vytvořit administrátora projektu (tento účet bude k dispozici i případě nefunkčnosti databáze)</li>\r\n				<li>Následně se nastaví připojení k databázi. Pokud zatím žádná databáze neexistuje s jejím vytvořením pomůže nástroj Adminer dostupný z <code>www/adminer/</code>, stačí zadat jméno nové databáze a její kódování. Ve venne se následně vybere vhodný ovladač databáze (mysqli, pdo_mysql, pdo_oracle, pdo_pgsql, pdo_sqlite, pdo_sqlsrv), vyplní se přístupové údaje a zvolí kódování stejně, jako při vytváření databáze. </li>\r\n				<li>V posledním kroku se vybere jazyk budoucího projektu a je hotovo.</li>\r\n			</ul>\r\n		</li>\r\n		<li><b>Před prvním použitím</b>\r\n			<ul>\r\n				<li>Následně je nutné v administraci vytvořit nový layout z v předchozích krocích nainstalovaného appearanice-module. V levém slouci záložka <i>Layouty</i>, nebo <i>Obsah/Správa layoutů</i>, zde kliknou na tlačítko Vytvořit, napsat libovolné označení a vybrat <i>appearanice/basic</i>.</li>\r\n				<li>Důležité je nezapomenout nainstalovat dříve ručně zkopírované moduly v <i>Systém/Správá modulů</i>. Napřed kliknutím na tlačítko <i>Obnovit databázi</i> načtete jejich seznam a násldně kliknete na tlačítko <i>Instalovat</i> u každého z nich.</li>\r\n				<li>V nabídce <i>Obsah/Správa stránek</i> už snadno začnete tvořit obsah.</li>\r\n				<li>Přesvěčte se, že z webového serveru jsou přístupné pouze soubory ve veřejné složce <code>www/</code> a že nejsou přístupné soubry ve složkách projektu <code>app/</code> a <code>vendor/</code>. Zvláště nebezpečné by bylo, pokud by byly přístupné soubory konfigurace například <code>app/config/config.neon</code>, ve kterém je v čitelné podobě umístěno jméno a heslo administrátora.</li>\r\n			</ul>\r\n		</li>\r\n	</ul>\r\n</div>			\r\n\r\n<hr>\r\n\r\n<div>\r\n	<h2>todo list (seznam úkolů)</h2>\r\n	<ul>\r\n		<li><b>nejdůležitější</b>\r\n			<ul>\r\n				<li><del>vytvořit responsivní layout (mobilní telefon, tablet, notebook, běžný monitor počítače)</del></li>\r\n				<li><del>udělat výpis sekce zadávání prací</del></li>\r\n				<li><del>přidávání nových záznamů do sekce zadávání prací</del></li>\r\n				<li><del>editace záznamů v sekci zadávání prací</del></li>\r\n				<li><del>mazání záznamů v sekci zadávání prací</del></li>\r\n				<li><del>udělat vlastní modul do venne pro zadávání prací</del></li>\r\n				<li>Vymyslet nějaké pěkné unikátní jméno pro celou aplikaci</li>\r\n				<li>Napsat úvodní odstavec o významu a účelu aplikace</li>\r\n				<li>Vytvořit pro studenta možnost rezervace práce</li>\r\n				<li>Vytvořit diskuzi vedoucího a studenta u rezervované práce</li>\r\n				<li>Předávání dat z thesiscollection-module do archive-module !!!</li>\r\n				<li>zprovoznit vyhledávání na webu (Google, nebo vlastní řešení?)</li>\r\n				<li><del>vytvořit modul pro zadávání a zveřejňování aktualit</del></li>\r\n				<li><del>zabezpečit modul aktualit pomocí vhodných uživatelských rolí</del></li>\r\n				<li>vytvořit modul archivu</li>\r\n				<li>zkontrolovat, jestli je nette panel viditelný jen účtu administrátora, případně to tak zajistit</li>\r\n				<li><del>vytvořit modul Agendy</del></li>\r\n				<li><del>umístit widget modulu Agendy do pravého aside sloupce</del></li>\r\n				<li>navázat automatické akce na určitý čas zvolený v agenda modulu (přesunutí práce do archivu…)</li>\r\n				<li>opravit jQuery kalendář, který se má zobrazovat pod datetime prvky formuláře !!!</li>\r\n				<li><del>zprovoznit virtuální server a nainstalovat na něj ukázkovou kopii projektu</del></li>\r\n				<li>vytvořit javascript, který po načtení stránky přesměruje na #info (čímž otevře informace o aplikaci) a javascriptem upravit menu tak, aby se v odkazech následně přenášela informace o tom, jestli jsou tyto informace zobrazeny, nebo ne.</li>\r\n				<li>umožnit studentovi napsat si úplně vlastní zadání (a najít si vedoucího/oponenta), které by poté garant oboru povolil, nebo neschválil</li>\r\n			</ul>\r\n		</li>\r\n		<li><b>důležité</b>\r\n			<ul>\r\n				<li>unit test a slenium testy</li>\r\n				<li>uživatelské testování UX</li>\r\n				<li>vytvořit/sehnat nějakou favikonu pro web</li>\r\n				<li>vytvořit testovací uživatelské účty pro různá oprávnění</li>\r\n				<li>vytvořit všechny možné role a nastavit je testovacím uživatelům</li>\r\n				<li>naplnit modul zadávání prací nějakými testovacími daty</li>\r\n				<li>vytvořit nějakou pěknou a užitečnou patičku stránky</li>\r\n				<li>otestovat layout na prohlížečích mobilních zařízení (telefony, tablety, čtečky) a udělat screenshoty</li>\r\n				<li><del>vytvořit css pro víceúrovňová menu, které Venne CMS umožňuje tvořit</del></li>\r\n				<li><del>vytvořit a ostylovat drobečkovou navigaci</del></li>\r\n				<li>umístit verzované zdrojové kódy všech modulů na bitbucket.org</li>\r\n				<li>k uživatelským účtům přidělit obrázky z gravataru (třeba v diskuzi a možná i u informace o přihlášeném uživateli)</li>\r\n				<li>javascripty z hlavičky umístit až na úplný konec stránky</li>\r\n				<li>refaktorovat moduly agendy a aktualit -> lepší názvy tříd</li>\r\n				<li><del>Zprovoznit real-time kompilování CSS souborů z Less souborů</del></li>\r\n				<li>naplnit modul agendy nějakými ukázkovými daty</li>\r\n				<li>nakonec si projet zdrojáky na všechny výskyty @todo komentářů a vyřešit ty poznámečky</li>\r\n				<li>doplnit k modulům texty vysvětlující jejich funkci a upravit je tak, aby se předmětné části zobrazovaly jen tomu, komu mají</li>\r\n			</ul>\r\n		</li>\r\n		<li><b>nedůležité</b>\r\n			<ul>\r\n				<li>vytvořit alternativní css vzhledy</li>\r\n				<li>překlad do dalších jazyků</li>\r\n				<li>modul pro A/B testování</li>\r\n				<li>modul pro ankety</li>\r\n				<li>vytvořit vlastní grafický layout i pro backend část aplikace (administraci)</li>\r\n				<li>použít nějakou grafickou nástavbu (tinymce) pro textarea určené ke vkládání obsahu webu</li>\r\n				<li>použít nějaký mezifiltr, který by minimalizovat (odstranil mezery, středníky u posledního pravidal, …) CSSka vzniklé z Less souborů</li>\r\n				<li>vytvořit několik ukázkových flash zpráviček na \'vývoj\' stránku</li>\r\n				<li>upravit CSS zobrazování číslovaných i odrážkových seznamů v section části webu</li>\r\n				<li>v administraci vyplnit meta tagy stránky jako keywords, description, …</li>\r\n				<li>umožnit uživatelům se přihlašovat pomocí oAuth a OpenID</li>\r\n				<li>vytvořit nějaký modul, který umožní přihlašování s hashem hesla vytvořeným javascriptem přímo u klienta (upravit nejlépe i už samotnou registraci)</li>\r\n				<li><del>vytvořit nějaký grafický efekt pro flash zprávičky, aby nebylo snadné je přehlédnout a ideálně bez javascriptu</del></li>\r\n			</ul>\r\n		</li>\r\n	</ul>\r\n</div>'),
(5,	'<p>víceúrovňová nabídka - obsah</p>'),
(6,	'<p>položka A - obsah</p>'),
(8,	'<p>položka B - obsah</p>'),
(9,	'<p>položka C - obsah</p>'),
(10,	'<p>položka B1 - obsah</p>'),
(11,	'<p>položka B2 - obsah</p>');

DROP TABLE IF EXISTS `textelement`;
CREATE TABLE `textelement` (
  `id` int(11) NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_624219D5BF396750` FOREIGN KEY (`id`) REFERENCES `element` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesisarchive`;
CREATE TABLE `thesisarchive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `specialization` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `released` datetime DEFAULT NULL,
  `archived` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route_id` int(11) DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `for_rent` tinyint(1) NOT NULL,
  `defense_year` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `annexes` int(11) DEFAULT NULL,
  `annotation` longtext CHARACTER SET utf8,
  `keywords` longtext CHARACTER SET utf8,
  `location` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `thesis_pdf_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_FD156DE934ECB4E6` (`route_id`),
  KEY `IDX_FD156DE9C4663E4` (`page_id`),
  KEY `IDX_FD156DE973154ED4` (`leader_id`),
  CONSTRAINT `FK_FD156DE934ECB4E6` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FD156DE973154ED4` FOREIGN KEY (`leader_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FD156DE9C4663E4` FOREIGN KEY (`page_id`) REFERENCES `thesisarchivepage` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesisarchive` (`id`, `page_id`, `text`, `specialization`, `created`, `updated`, `expired`, `released`, `archived`, `name`, `route_id`, `leader_id`, `student_id`, `for_rent`, `defense_year`, `size`, `annexes`, `annotation`, `keywords`, `location`, `thesis_pdf_id`) VALUES
(22,	24,	's tím',	'Bc: DOL',	'2013-06-06 00:21:23',	NULL,	'2013-06-21 00:00:00',	'2013-06-06 00:21:23',	NULL,	'do archivu!',	67,	5,	2,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(23,	24,	'tu je popisek , tak hurá do toho',	'Bc: LOS',	'2013-05-03 16:50:46',	NULL,	'2013-06-07 00:00:00',	'2013-05-03 16:50:46',	NULL,	'finále',	68,	4,	2,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(24,	24,	'a tak',	'Bc: DOL',	'2013-06-07 00:32:41',	NULL,	NULL,	'2013-06-07 00:32:41',	NULL,	'je pozdě',	70,	5,	2,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(25,	24,	'sd sdf sdfsdf ddsd df',	'Bc: LLD',	'2013-06-07 09:48:14',	NULL,	NULL,	'2013-06-07 09:48:14',	NULL,	'regsf sdf sdf',	72,	5,	3,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(26,	24,	'sdf sdf',	'Bc: DOL',	'2013-06-07 10:17:10',	NULL,	NULL,	'2013-06-07 10:17:10',	NULL,	'ahoj',	74,	5,	2,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(27,	24,	'sdf sadf',	'Bc: LLD',	'2013-06-07 10:39:42',	NULL,	NULL,	'2013-06-07 10:39:42',	NULL,	'aaaaa',	76,	5,	2,	1,	2015,	29,	9,	'Lorem ipsum dolor sit amet consectetuer id Nam faucibus diam pede. Convallis faucibus eu augue ut interdum sagittis commodo sed metus Vivamus. Ante Sed pretium cursus risus porttitor faucibus Quisque dui porttitor tristique. Lorem porttitor volutpat nunc laoreet et malesuada sed quis semper tincidunt. Amet pellentesque sed ac leo laoreet turpis justo ornare Lorem tellus. Pede tellus nonummy interdum libero interdum dolor.\r\n\r\nJusto Phasellus sodales eu Curabitur laoreet turpis tellus id Nam leo. Vestibulum orci Nam auctor risus tincidunt Vivamus Vestibulum porta mus nulla. Mauris sed Nunc elit id tempor Pellentesque sed laoreet fringilla neque. Pellentesque et mauris elit ut eget lacinia amet in Quisque cursus. Pretium purus mollis pretium In montes feugiat dui Duis Nam at. Wisi mollis ipsum ante et nibh Nulla tellus justo tincidunt faucibus. \r\n\r\nUt hac quis tristique Vivamus tellus porta urna justo vitae interdum. Laoreet et odio ac euismod eget ac congue tempor odio eget. Et ante velit elit Quisque tortor Duis tellus mollis Aenean elit. Condimentum penatibus aliquam orci tristique Nam eleifend Pellentesque dolor condimentum at. Suscipit Curabitur vel enim sollicitudin Nulla tincidunt ac tortor nibh pretium. Vel libero.\r\n\r\nInterdum convallis Lorem ante nulla ipsum felis non nibh odio vitae. Nam a id Curabitur sociis eu cursus elit metus quis semper. Metus augue in et Pellentesque ut odio massa nibh Sed Aenean. Vel Nunc tincidunt condimentum eget nisl nibh ut dui sagittis Nulla. Eget ipsum Fusce sed Suspendisse magna adipiscing Phasellus penatibus Quisque nibh. Dis.',	'sdf sdf sd',	'sd sdf sdhg',	NULL),
(28,	24,	'<p>Lorem ipsum dolor sit amet consectetuer leo Quisque et porta at. Urna nibh habitant nibh faucibus penatibus In massa Nullam tempor faucibus. Interdum auctor Phasellus In massa urna Vestibulum cursus iaculis pellentesque at. Laoreet id et sapien convallis justo eget Curabitur suscipit a convallis. Aenean wisi iaculis tincidunt est Aenean rhoncus.</p>\r\n<p>Sed tristique Curabitur mattis Nullam dui Vivamus sagittis aliquet quis quis. Convallis commodo auctor sed neque adipiscing non semper tincidunt pretium ligula. Phasellus Mauris felis neque Curabitur venenatis pretium est auctor eget gravida. Ipsum Curabitur rhoncus ut nulla Nunc In orci Pellentesque tellus laoreet. Justo tincidunt rhoncus ligula ante tincidunt mollis congue adipiscing.</p>\r\n<p>Nec Donec orci sed semper tristique vitae pretium quis vitae fringilla. Tincidunt id sed magna tempor at lacus ante facilisis dis penatibus. Dictumst porttitor sagittis tellus egestas rutrum felis vel id risus Nam. Sagittis metus ut vitae quis vitae justo commodo tristique nec eget. Vestibulum a sagittis condimentum turpis metus non.</p>\r\n<p>Auctor tortor risus cursus consequat Quisque Vestibulum ante euismod Ut wisi. Dictumst vel Mauris orci augue vitae faucibus id et et elit. Enim Vestibulum justo semper ac Curabitur tincidunt sapien lacinia Lorem eu. Amet metus nec ut eu dictum semper orci ridiculus sapien Curabitur. Felis enim dui Curabitur justo tempor convallis Maecenas eu netus hendrerit. Auctor orci sagittis Curabitur nibh pede condimentum tempor Vestibulum id eros. </p>\r\n',	'Bc: DOL',	'2013-06-07 11:26:04',	NULL,	NULL,	'2013-06-07 11:26:04',	NULL,	'Monitoring stavu hladiny Bečvy',	78,	5,	2,	1,	2015,	53,	19,	'stgs sdg sdf',	'dfg sdfg dfg',	'd fd fdg',	NULL),
(29,	24,	'huhuéééé',	'Bc: INM',	'2013-04-27 15:55:19',	NULL,	'2013-08-09 00:00:00',	'2013-04-27 15:55:19',	NULL,	'jméno práce',	79,	4,	2,	1,	2016,	27,	7,	'',	'',	'',	4);

DROP TABLE IF EXISTS `thesisarchiveelement`;
CREATE TABLE `thesisarchiveelement` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_70A34E6DBF396750` FOREIGN KEY (`id`) REFERENCES `element` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesisarchiveentity_archivepageentity`;
CREATE TABLE `thesisarchiveentity_archivepageentity` (
  `thesisarchiveentity_id` int(11) NOT NULL,
  `archivepageentity_id` int(11) NOT NULL,
  PRIMARY KEY (`thesisarchiveentity_id`,`archivepageentity_id`),
  KEY `IDX_7AF9BB02C0B44629` (`thesisarchiveentity_id`),
  KEY `IDX_7AF9BB023998EF6F` (`archivepageentity_id`),
  CONSTRAINT `FK_7AF9BB023998EF6F` FOREIGN KEY (`archivepageentity_id`) REFERENCES `thesisarchivepage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_7AF9BB02C0B44629` FOREIGN KEY (`thesisarchiveentity_id`) REFERENCES `thesisarchiveelement` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesisarchivepage`;
CREATE TABLE `thesisarchivepage` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_ABF9276ABF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesisarchivepage` (`id`, `itemsPerPage`) VALUES
(24,	10);

DROP TABLE IF EXISTS `thesisarchivesliderelement`;
CREATE TABLE `thesisarchivesliderelement` (
  `id` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_2B0B2D39BF396750` FOREIGN KEY (`id`) REFERENCES `element` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesisarchivesliderentity_archivepageentity`;
CREATE TABLE `thesisarchivesliderentity_archivepageentity` (
  `thesisarchivesliderentity_id` int(11) NOT NULL,
  `archivepageentity_id` int(11) NOT NULL,
  PRIMARY KEY (`thesisarchivesliderentity_id`,`archivepageentity_id`),
  KEY `IDX_D34FD3E7421D8BAD` (`thesisarchivesliderentity_id`),
  KEY `IDX_D34FD3E73998EF6F` (`archivepageentity_id`),
  CONSTRAINT `FK_D34FD3E73998EF6F` FOREIGN KEY (`archivepageentity_id`) REFERENCES `thesisarchivepage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D34FD3E7421D8BAD` FOREIGN KEY (`thesisarchivesliderentity_id`) REFERENCES `thesisarchivesliderelement` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesischat_comment`;
CREATE TABLE `thesischat_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `viewed` tinyint(1) NOT NULL,
  `thesis_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E20D4C9EC4663E4` (`page_id`),
  KEY `IDX_E20D4C9E68D82738` (`thesis_id`),
  KEY `IDX_E20D4C9EF675F31B` (`author_id`),
  CONSTRAINT `FK_E20D4C9E68D82738` FOREIGN KEY (`thesis_id`) REFERENCES `thesiscollection` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E20D4C9EC4663E4` FOREIGN KEY (`page_id`) REFERENCES `thesischat_page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E20D4C9EF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesischat_library`;
CREATE TABLE `thesischat_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `defense_year` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `annexes` int(11) DEFAULT NULL,
  `annotation` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywords` longtext COLLATE utf8_unicode_ci,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `thesis_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  `thesis_pdf_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D7F9864EC4663E4` (`page_id`),
  KEY `IDX_D7F9864E68D82738` (`thesis_id`),
  KEY `IDX_D7F9864ECB944F1A` (`student_id`),
  KEY `IDX_D7F9864E73154ED4` (`leader_id`),
  CONSTRAINT `FK_D7F9864E68D82738` FOREIGN KEY (`thesis_id`) REFERENCES `thesiscollection` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D7F9864E73154ED4` FOREIGN KEY (`leader_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D7F9864EC4663E4` FOREIGN KEY (`page_id`) REFERENCES `thesischat_page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D7F9864ECB944F1A` FOREIGN KEY (`student_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesischat_library` (`id`, `page_id`, `defense_year`, `size`, `annexes`, `annotation`, `keywords`, `location`, `created`, `updated`, `thesis_id`, `student_id`, `leader_id`, `thesis_pdf_id`) VALUES
(3,	19,	2013,	24,	9,	'sdf',	' sdf ds dfs',	'sdf',	'2013-06-07 16:17:19',	'2013-06-08 15:59:08',	6,	2,	5,	2),
(4,	19,	2015,	25,	8,	'vcb',	'df fdg',	'',	'2013-06-07 16:30:39',	'2013-06-07 18:02:14',	7,	3,	5,	NULL);

DROP TABLE IF EXISTS `thesischat_page`;
CREATE TABLE `thesischat_page` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  `notificationRoute_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C607BE97ACCCBD7A` (`notificationRoute_id`),
  CONSTRAINT `FK_C607BE97ACCCBD7A` FOREIGN KEY (`notificationRoute_id`) REFERENCES `route` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C607BE97BF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesischat_page` (`id`, `itemsPerPage`, `notificationRoute_id`) VALUES
(19,	4,	29);

DROP TABLE IF EXISTS `thesiscollection`;
CREATE TABLE `thesiscollection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `specialization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `released` datetime DEFAULT NULL,
  `approved` int(11) NOT NULL,
  `booked_by_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route_id` int(11) DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A8E7A38E34ECB4E6` (`route_id`),
  KEY `IDX_A8E7A38EC4663E4` (`page_id`),
  KEY `IDX_A8E7A38E73154ED4` (`leader_id`),
  KEY `IDX_A8E7A38EF4A5BD90` (`booked_by_id`),
  CONSTRAINT `FK_A8E7A38E34ECB4E6` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A8E7A38E73154ED4` FOREIGN KEY (`leader_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A8E7A38EC4663E4` FOREIGN KEY (`page_id`) REFERENCES `thesiscollectionpage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A8E7A38EF4A5BD90` FOREIGN KEY (`booked_by_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesiscollection` (`id`, `page_id`, `text`, `specialization`, `created`, `updated`, `expired`, `released`, `approved`, `booked_by_id`, `name`, `route_id`, `leader_id`) VALUES
(6,	3,	'dfg fdg',	'Bc: DOL',	'2013-06-07 16:16:04',	NULL,	NULL,	'2013-06-07 16:16:04',	1,	2,	'dgsdfg',	80,	5),
(7,	3,	'sdf sdfd',	'Bc: DOL',	'2013-06-07 16:29:06',	NULL,	NULL,	'2013-06-07 16:29:06',	1,	3,	'pro pokukusa',	81,	5);

DROP TABLE IF EXISTS `thesiscollectionelement`;
CREATE TABLE `thesiscollectionelement` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_DB77BF6FBF396750` FOREIGN KEY (`id`) REFERENCES `element` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesiscollectionentity_pageentity`;
CREATE TABLE `thesiscollectionentity_pageentity` (
  `thesiscollectionentity_id` int(11) NOT NULL,
  `pageentity_id` int(11) NOT NULL,
  PRIMARY KEY (`thesiscollectionentity_id`,`pageentity_id`),
  KEY `IDX_A36B673F38384DF5` (`thesiscollectionentity_id`),
  KEY `IDX_A36B673F2F7A3F1D` (`pageentity_id`),
  CONSTRAINT `FK_A36B673F2F7A3F1D` FOREIGN KEY (`pageentity_id`) REFERENCES `thesiscollectionpage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A36B673F38384DF5` FOREIGN KEY (`thesiscollectionentity_id`) REFERENCES `thesiscollectionelement` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesiscollectionpage`;
CREATE TABLE `thesiscollectionpage` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  `dir_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_ABB275DEEEB38DE6` (`dir_id`),
  CONSTRAINT `FK_ABB275DEBF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_ABB275DEEEB38DE6` FOREIGN KEY (`dir_id`) REFERENCES `directory` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesiscollectionpage` (`id`, `itemsPerPage`, `dir_id`) VALUES
(3,	10,	NULL);

DROP TABLE IF EXISTS `thesiscollectionsliderelement`;
CREATE TABLE `thesiscollectionsliderelement` (
  `id` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_6F8C6990BF396750` FOREIGN KEY (`id`) REFERENCES `element` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesiscollectionsliderentity_pageentity`;
CREATE TABLE `thesiscollectionsliderentity_pageentity` (
  `thesiscollectionsliderentity_id` int(11) NOT NULL,
  `pageentity_id` int(11) NOT NULL,
  PRIMARY KEY (`thesiscollectionsliderentity_id`,`pageentity_id`),
  KEY `IDX_51EFE7AF4C694D03` (`thesiscollectionsliderentity_id`),
  KEY `IDX_51EFE7AF2F7A3F1D` (`pageentity_id`),
  CONSTRAINT `FK_51EFE7AF2F7A3F1D` FOREIGN KEY (`pageentity_id`) REFERENCES `thesiscollectionpage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_51EFE7AF4C694D03` FOREIGN KEY (`thesiscollectionsliderentity_id`) REFERENCES `thesiscollectionsliderelement` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `thesisprearchive`;
CREATE TABLE `thesisprearchive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thesis_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4FA7EC3C68D82738` (`thesis_id`),
  CONSTRAINT `FK_4FA7EC3C68D82738` FOREIGN KEY (`thesis_id`) REFERENCES `thesiscollection` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesisprearchive` (`id`, `name`, `thesis_id`) VALUES
(12,	'',	6),
(13,	'$form->data->name',	6),
(14,	'pro pokukusa',	7);

DROP TABLE IF EXISTS `thesisprearchivepage`;
CREATE TABLE `thesisprearchivepage` (
  `id` int(11) NOT NULL,
  `itemsPerPage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_A91801ECBF396750` FOREIGN KEY (`id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `thesisprearchivepage` (`id`, `itemsPerPage`) VALUES
(23,	10);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_id` int(11) DEFAULT NULL,
  `enable` tinyint(1) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `fullName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `specialization` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `socialType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socialData` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D64986383B10` (`avatar_id`),
  CONSTRAINT `FK_8D93D64986383B10` FOREIGN KEY (`avatar_id`) REFERENCES `file` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `avatar_id`, `enable`, `email`, `fullName`, `specialization`, `password`, `key`, `salt`, `socialType`, `socialData`, `created`, `type`) VALUES
(1,	NULL,	1,	'studijni@heslo.je.studijni',	'Pavlína ze studijního',	NULL,	'a10cd9f9c6816fe96fd0c747c9111004',	'',	'ljyktaf6',	NULL,	NULL,	'2013-04-27 15:52:51',	'base'),
(2,	NULL,	1,	'student@heslo.je.student',	'Franta pilný student',	'Bc: INM',	'9553df9771d418ccee182a090c92eeec',	'',	'tafpnb2q',	NULL,	NULL,	'2013-05-03 11:48:38',	'base'),
(3,	NULL,	1,	'pokus@heslo.je.pokus',	'Marek Pokus',	'Bc: LCR',	'88915a7b163b0a02a835115d2741092f',	'',	'sk0j74k6',	NULL,	NULL,	'2013-05-03 15:07:32',	'base'),
(4,	NULL,	1,	'vedouci@heslo.je.vedouci',	'Jarek Vedoucí',	NULL,	'5a3b4b044b2f7b882494dfc620a7b757',	'',	'as7t0qfs',	NULL,	NULL,	'2013-05-10 16:59:15',	'base'),
(5,	NULL,	1,	'admin@heslo.je.admin',	'Alexandr Admin',	NULL,	'3fc1069c2d5c086a5955b66da1a35e81',	'',	'o118xd8k',	NULL,	NULL,	'2013-05-10 17:01:14',	'base'),
(6,	NULL,	1,	'garant@heslo.je.garant',	'Gustav Garant',	NULL,	'6f1e41eaf3601e5febc5732be48504bc',	'',	'wffmo3e6',	NULL,	NULL,	'2013-05-24 18:42:22',	'base'),
(7,	NULL,	1,	'magistr@heslo.je.magistr',	'Martin Magistr',	'Mgr: LOG',	'3b989502a5e24dbde95fd916e8fc139c',	'',	'piz4owmm',	NULL,	NULL,	'2013-05-25 15:53:49',	'base');

DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `IDX_51498A8EA76ED395` (`user_id`),
  KEY `IDX_51498A8ED60322AC` (`role_id`),
  CONSTRAINT `FK_51498A8EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_51498A8ED60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
(1,	1),
(2,	3),
(3,	3),
(4,	2),
(5,	5),
(6,	4),
(7,	3);

-- 2013-06-08 20:58:43