<?php

// autoload_namespaces.php generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Venne' => $vendorDir . '/venne/framework',
    'TranslatorModule' => $vendorDir . '/venne/translator-module',
    'Symfony\\Component\\Yaml' => $vendorDir . '/symfony/yaml',
    'Symfony\\Component\\Console' => $vendorDir . '/symfony/console',
    'Nella' => $vendorDir . '/nella/gedmo',
    'LessModule' => $vendorDir . '/venne/less-module',
    'Kdyby\\Replicator\\' => $vendorDir . '/kdyby/forms-replicator/src',
    'Kdyby\\Extension\\Forms\\BootstrapRenderer' => $vendorDir . '/kdyby/bootstrap-form-renderer',
    'GoogleanalyticsModule' => $vendorDir . '/venne/googleanalytics-module',
    'Gedmo' => $vendorDir . '/gedmo/doctrine-extensions/lib',
    'FormsModule' => $vendorDir . '/venne/forms-module',
    'Doctrine\\ORM' => $vendorDir . '/doctrine/orm/lib',
    'Doctrine\\DBAL' => $vendorDir . '/doctrine/dbal/lib',
    'Doctrine\\Common\\Lexer\\' => $vendorDir . '/doctrine/lexer/lib',
    'Doctrine\\Common\\Inflector\\' => $vendorDir . '/doctrine/inflector/lib',
    'Doctrine\\Common\\Collections\\' => $vendorDir . '/doctrine/collections/lib',
    'Doctrine\\Common\\Cache\\' => $vendorDir . '/doctrine/cache/lib',
    'Doctrine\\Common\\Annotations\\' => $vendorDir . '/doctrine/annotations/lib',
    'Doctrine\\Common\\' => $vendorDir . '/doctrine/common/lib',
    'DoctrineModule' => $vendorDir . '/venne/doctrine-module',
    'CmsModule' => $vendorDir . '/venne/cms-module',
    'AssetsModule' => $vendorDir . '/venne/assets-module',
    'AppearaniceModule' => $vendorDir . '/ic/appearanice-module',
);
