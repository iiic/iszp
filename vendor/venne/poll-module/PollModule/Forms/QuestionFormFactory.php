<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Forms;

use Venne;
use Venne\Forms\Form;
use Nette\Security\User;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 */
class QuestionFormFactory extends FormFactory
{



	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}



	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();
		$form->addDateTime('created', 'Created');
		$form->addDateTime('expired', 'Expired');
		$form->addManyToOne('author', 'Author');
		$form->addSelect('poll_type', 'Poll type:', \PollModule\Components\Table\Columns\PollColumn::$poll_types)
			->setRequired('Musíte zvolit typ ankety');
		$form->addSelect('another_vote', 'Another vote after:', \PollModule\Components\Table\Columns\PollColumn::$another_vote_after)
			->setRequired('Musíte zvolit kdy je možné znovu hlasovat');
		$form->addCheckbox('allow_user_add', 'Umožnit uživatelům psát vlastní odpovědi');

		$form->addGroup('Question');
		$form->addText('text', 'Question')
			->setRequired(TRUE);

		$form->addGroup('Answers');
		$form->addText('answer_1', 'option 1');
		$form->addText('answer_2', 'option 2');
		$form->addText('answer_3', 'option 3');

		$form->addSaveButton('Save');
	}

}
