<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Components;

use PollModule\Repositories\QuestionRepository;
use PollModule\Repositories\AnswerRepository;
use PollModule\Repositories\VoteRepository;
use CmsModule\Content\SectionControl;
use PollModule\Forms\QuestionFormFactory;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 */
class TableControl extends SectionControl
{

	/** @var QuestionRepository */
	protected $questionRepository;

	/** @var AnswerRepository */
	protected $answerRepository;

	/** @var VoteRepository */
	protected $voteRepository;

	/** @var QuestionFormFactory */
	protected $questionFormFactory;



	/**
	 * @param \PollModule\Repositories\QuestionRepository $questionRepository
	 * @param \PollModule\Forms\QuestionFormFactory $questionFormFactory
	 */
	public function __construct(QuestionRepository $questionRepository, QuestionFormFactory $questionFormFactory)
	{
		parent::__construct();

		$this->questionRepository = $questionRepository;
		$this->questionFormFactory = $questionFormFactory;
	}



	protected function createComponentTable()
	{
		$table = new \CmsModule\Components\Table\TableControl;
		$table->setTemplateConfigurator($this->templateConfigurator);
		$table->setRepository($this->questionRepository);

		$pageId = $this->entity->id;
		$table->setDql(function ($sql) use ($pageId) {
			$sql = $sql->andWhere('a.page = :page')->setParameter('page', $pageId);
			return $sql;
		});

		// forms
		$repository = $this->questionRepository;
		$entity = $this->entity;
		$form = $table->addForm($this->questionFormFactory, 'Question', function () use ($repository, $entity) {
			return $repository->createNew(array($entity));
		}, \CmsModule\Components\Table\Form::TYPE_LARGE);

		// navbar
		$table->addButtonCreate('create', 'Create new', $form, 'file');

		$table->addColumn('text', 'Text')
			->setWidth('35%')
			->setSortable(TRUE)
			->setFilter();
		$table->addColumn('author', 'Author')
			->setWidth('25%')
			->setCallback(function ($entity) {
				return $entity->author ? $entity->author : $entity->authorName;
			});
		$table->addColumn('created', 'Created', \CmsModule\Components\Table\TableControl::TYPE_DATE_TIME)
			->setWidth('20%')
			->setSortable(TRUE);
		$table->addColumn('updated', 'Updated', \CmsModule\Components\Table\TableControl::TYPE_DATE_TIME)
			->setWidth('20%')
			->setSortable(TRUE);

		$table->addActionEdit('edit', 'Edit', $form);
		$table->addActionDelete('delete', 'Delete');

		// global actions
		$table->setGlobalAction($table['delete']);

		return $table;
	}



	public function render()
	{
		$this['table']->render();
	}

}
