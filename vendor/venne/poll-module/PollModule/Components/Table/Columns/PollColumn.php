<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Components\Table\Columns;
/*
use Venne;
use CmsModule\Components\Table\TableControl;
use Nette\ComponentModel\Component;
use Nette\ComponentModel\IContainer;
*/
/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 */
class PollColumn extends \CmsModule\Components\Table\Columns\BaseColumn
{

	public static $poll_types = array(
		'druhy anket' => array(
			'neomezené hlasování',
			'hlasování podle IP adres',
			'hlasování uložením cookie v prohlížeči uživatele',
			'hlasování podle IP adres i cookies u uživatele',
			'hlasování podle uživatelských jmen autorizovaných uživatelů',
		),
	);

	public static $another_vote_after = array(
		'další hlasování' => array( // index je čas v ks (kilo sekunda) s tím, že 0 je okamžitě a -1 nikdy
			'0' => 'okamžitě',
			'0.01' => 'za 10 sekund',
			'0.03' => 'za půl minuty',
			'0.06' => 'za minutu',
			'0.6' => 'za 10 minut',
			'1.8' => 'za půl hodiny',
			'3.6' => 'za hodinu',
			'7.2' => 'za 2 hodiny',
			'21.6' => 'za 6 hodin',
			'43.2' => 'za 12 hodin',
			'64.8' => 'za 18 hodin',
			'86.4' => 'za 1 den',
			'172.8' => 'za 2 dny',
			'604.8' => 'za týden',
			'864' => 'za 10 dní',
			'1210' => 'za 2 týdny',
			'2628' => 'za měsíc', // http://www.wolframalpha.com/input/?i=1+month+to+ks
			'5256' => 'za 2 měsíce',
			'15768' => 'za půl roku',
			'31536' => 'za rok',
			'63072' => 'za 2 roky',
			'-1' => 'nikdy',
		),
	);
/*
	public function __construct(TableControl $table, $name, $title)
	{
		parent::__construct($table, $name, $title);

		$_this = $this;
		$this->callback = function ($entity) use ($_this) {
			$column = $entity->{$_this->name};
			return self::$poll_types['druhy anket'][$column];
		};
	}
*/
}
