<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Entities;

use Venne;
use Doctrine\ORM\Mapping as ORM;
use CmsModule\Security\Entities\UserEntity;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\PollModule\Repositories\AnswerRepository")
 * @ORM\Table(name="poll_answer")
 */
class AnswerEntity extends \DoctrineModule\Entities\IdentifiedEntity
{
	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $text = '';

	/**
	 * @var QuestionEntity
	 * @ORM\ManyToOne(targetEntity="\PollModule\Entities\QuestionEntity", inversedBy="text")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $question;

	/**
	 * @var VoteEntity
	 * @ORM\ManyToOne(targetEntity="\PollModule\Entities\VoteEntity", inversedBy="text")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $vote;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $author;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;


	/**
	 * @param QuestionEntity $questionEntity
	 * @param VoteEntity $voteEntity
	 */
	public function __construct(QuestionEntity $questionEntity, VoteEntity $voteEntity)
	{
		parent::__construct();

		$this->question = $questionEntity;
		$this->vote = $voteEntity;
		$this->created = new \Nette\DateTime();
	}


	/**
	 * @param \CmsModule\Security\Entities\UserEntity $author
	 */
	public function setAuthor($author)
	{
		if ($author instanceof UserEntity) {
			$this->author = $author;
			return;
		} else if (is_string($author) && $author) {
			$this->author = NULL;
			return;
		}
	}


	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getAuthor()
	{
		return $this->author;
	}


	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}


	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}


	/**
	 * @param \PollModule\Entities\PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}


	/**
	 * @return \PollModule\Entities\PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}


	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}


	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;
	}


	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}
}
