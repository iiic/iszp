<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Entities;

use Venne;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\CmsModule\Content\Repositories\PageRepository")
 * @ORM\Table(name="poll_page")
 * @ORM\DiscriminatorEntry(name="pollPage")
 */
class PageEntity extends \CmsModule\Content\Entities\PageEntity
{

	/**
	 * @var ArrayCollection|QuestionEntity[]
	 * @ORM\OneToMany(targetEntity="QuestionEntity", mappedBy="page")
	 */
	protected $questions;

	
	
	/**
	 * @var string
	 * @ORM\Column(type="integer")
	 */
	protected $itemsPerPage = 10;


	
	public function __construct()
	{
		parent::__construct();

		$this->mainRoute->type = 'Poll:Default:default';
	}


	
	/**
	 * @param $questions
	 */
	public function setQuestions($questions)
	{
		$this->questions = $questions;
	}

	

	/**
	 * @return ArrayCollection|QuestionEntity[]
	 */
	public function getQuestions()
	{
		return $this->questions;
	}


	
	/**
	 * @param string $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
	}


	
	/**
	 * @return string
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}

}
