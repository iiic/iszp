<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Entities;

use Venne;
use Doctrine\ORM\Mapping as ORM;
use CmsModule\Security\Entities\UserEntity;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\PollModule\Repositories\QuestionRepository")
 * @ORM\Table(name="poll_question")
 */
class QuestionEntity extends \DoctrineModule\Entities\IdentifiedEntity
{
	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $text = '';

	/**
	 * @var PageEntity
	 * @ORM\ManyToOne(targetEntity="\PollModule\Entities\PageEntity", inversedBy="votes")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $author;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $expired;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;

	/**
	 * @var integer
	 * @ORM\Column(type="smallint")
	 */
	protected $poll_type;

	/**
	 * @var double
	 * @ORM\Column(type="decimal")
	 */
	protected $another_vote;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $allow_user_add = false;



	/**
	 * @param PageEntity $pageEntity
	 */
	public function __construct(PageEntity $pageEntity)
	{
		parent::__construct();

		$this->page = $pageEntity;
		$this->created = new \Nette\DateTime();
	}



	/**
	 * @param \CmsModule\Security\Entities\UserEntity $author
	 */
	public function setAuthor($author)
	{
		if ($author instanceof UserEntity) {
			$this->author = $author;
			return;
		} else if (is_string($author) && $author) {
			$this->author = NULL;
			return;
		}
	}



	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getAuthor()
	{
		return $this->author;
	}



	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}



	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}



	/**
	 * @param \DateTime $expired
	 */
	public function setExpired($expired)
	{
		$this->expired = $expired;
	}



	/**
	 * @return \DateTime
	 */
	public function getExpired()
	{
		return $this->expired;
	}



	/**
	 * @param \PollModule\Entities\PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}



	/**
	 * @return \PollModule\Entities\PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}



	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}



	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}



	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;
	}



	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}



	/**
	 * @param integer $poll_type
	 */
	public function setPoll_type($poll_type)
	{
		$this->poll_type = $poll_type;
	}



	/**
	 * @return integer
	 */
	public function getPoll_type()
	{
		return $this->poll_type;
	}



	/**
	 * @param double $poll_type
	 */
	public function setAnother_vote($another_vote)
	{
		$this->another_vote = $another_vote;
	}



	/**
	 * @return double
	 */
	public function getAnother_vote()
	{
		return $this->another_vote;
	}



	/**
	 * @param boolean $poll_type
	 */
	public function setAllow_user_add($allow_user_add)
	{
		$this->allow_user_add = $allow_user_add;
	}



	/**
	 * @return boolean
	 */
	public function getAllow_user_add()
	{
		return $this->allow_user_add;
	}

}
