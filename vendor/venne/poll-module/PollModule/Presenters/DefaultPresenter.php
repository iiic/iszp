<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule\Presenters;

use PollModule\Forms\QuestionFrontFormFactory;
use PollModule\Repositories\QuestionRepository;
use PollModule\Repositories\AnswerRepository;
use PollModule\Repositories\VoteRepository;
use Nette\Application\ForbiddenRequestException;
use Nette\DateTime;
use Nette\Forms\Form;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 */
class DefaultPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $key;

	/** @var QuestionRepository */
	protected $questionRepository;

	/** @var AnswerRepository */
	protected $answerRepository;

	/** @var VoteRepository */
	protected $voteRepository;

	/** @var QuestionFrontFormFactory */
	protected $questionFormFactory;



	/**
	 * @param \PollModule\Repositories\QuestionRepository $questionRepository
	 */
	public function injectQuestionRepository(QuestionRepository $questionRepository)
	{
		$this->questionRepository = $questionRepository;
	}



	/**
	 * @param \PollModule\Repositories\AnswerRepository $answerRepository
	 */
	public function injectAnswerRepository(AnswerRepository $answerRepository)
	{
		$this->answerRepository = $answerRepository;
	}



	/**
	 * @param \PollModule\Repositories\VoteRepository $voteRepository
	 */
	public function injectVoteRepository(VoteRepository $voteRepository)
	{
		$this->voteRepository = $voteRepository;
	}



	/**
	 * @param \PollModule\Forms\QuestionFrontFormFactory $questionFormFactory
	 */
	public function injectQuestionFormFactory(QuestionFrontFormFactory $questionFormFactory)
	{
		$this->questionFormFactory = $questionFormFactory;
	}



	public function actionDefault()
	{
		if ($this->key) {
			$entity = $this->questionRepository->find($this->key);
			if (!$entity->author || !$this->user->isLoggedIn() || $entity->author->email !== $this->user->identity->getId()) {
				throw new ForbiddenRequestException;
			}
		}

		if ($this->isLoggedInAsSuperadmin()) {
			$this->flashMessage('You are logged in as superadmin. You can not send new questions.', 'info', TRUE);
		}
	}



	public function handleEdit($id)
	{
		$this->key = $id;

		$this->redirect('this', array('key' => $id));
	}



	public function handleDelete($id)
	{
		$entity = $this->questionRepository->find($id);

		if ($entity->author && $this->user->isLoggedIn() && $entity->author->email === $this->user->identity->getId()) {
			$this->questionRepository->delete($entity);
		} else {
			throw new ForbiddenRequestException;
		}

		$this->flashMessage('Question has been deleted.', 'success');
		$this->redirect('this');
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults($this->page->itemsPerPage)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->questionRepository->createQueryBuilder("a")
			->andWhere('a.page = :page')->setParameter('page', $this->page->id);
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	protected function createComponentForm()
	{
		if ($this->isLoggedInAsSuperadmin()) {
			throw new ForbiddenRequestException;
		}

		$form = $this->questionFormFactory->invoke($this->questionRepository->createNew(array($this->page)));
		$form->onSuccess[] = $this->formSuccess;
		return $form;
	}



	public function formSuccess()
	{
		$this->flashMessage('Message has been saved.', 'success');
		$this->redirect('this');
	}



	protected function createComponentEditForm()
	{
		$entity = $this->questionRepository->find($this->key);

		$form = $this->questionFormFactory->invoke($entity);
		$form->onAttached[] = function (Form $form) {
			if ($form->isSubmitted()) {
				$form->data->updated = new DateTime;
			}
		};
		$form->onSuccess[] = $this->editFormSuccess;
		return $form;
	}



	public function editFormSuccess()
	{
		$this->flashMessage('Message has been updated.', 'success');
		$this->redirect('this', array('key' => NULL));
	}



	public function renderDefault()
	{
		$this->invalidateControl('poll');
	}



	/**
	 * @return bool
	 */
	public function isLoggedInAsSuperadmin()
	{
		return ($this->user->isLoggedIn() && $this->user->identity->getId() === $this->context->parameters['administration']['login']['name']);
	}

}
