<?php

/**
 * This file is unofficial part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka (http://icweb.eu)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace PollModule;

use Venne;
use Venne\Module\ComposerModule;

/**
 * @author Michal Lupečka <ic.czech@gmail.com>
 */
class Module extends ComposerModule
{

}
