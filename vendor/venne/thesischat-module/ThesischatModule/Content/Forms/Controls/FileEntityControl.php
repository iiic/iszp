<?php

/**
 * This file is part of the Nette Framework (http://nette.org)
 *
 * Copyright (c) 2004, 2011 David Grudl (http://davidgrudl.com)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Content\Forms\Controls;

use Nette\Utils\Html;
use Venne\Tools\Objects;
use CmsModule\Content\Entities\FileEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>, Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class FileEntityControl extends \Nette\Forms\Controls\UploadControl
{

	/** @var FileEntity */
	protected $fileEntity;

	/** @var bool */
	protected $multi = false;

	/** @var bool */
	private $_valueLoaded = false;


	protected function attached($form)
	{
		parent::attached($form);

		$this->fileEntity = Objects::hasProperty($this->parent->data, $this->name) ? Objects::getProperty($this->parent->data, $this->name) : NULL;

		if ($this->fileEntity instanceof \Doctrine\Common\Collections\Collection) {
			$this->multi = true;
		}
	}



	public function getValue()
	{
		if (!$this->_valueLoaded) {
			$path = explode('[', strtr(str_replace(array('[]', ']'), '', $this->getHtmlName()), '.', '_'));
			unset($path[count($path) - 1]);
			$values = \Nette\Utils\Arrays::get((array)$this->getForm()->getHttpData(), $path, NULL);

			if ($this->multi) {
				if (!$this->fileEntity) {
					$this->fileEntity = new ArrayCollection;
				}
				foreach ($this->fileEntity as $file) {
					$delete = isset($values[$this->name . '_delete_' . $file->id]) && $values[$this->name . '_delete_' . $file->id] == 'on';
					if ($delete) {
						$coll = $this->fileEntity;
						$coll->removeElement($file);
					}
				}
			} else if ($this->fileEntity) {
				$delete = isset($values[$this->name . '_delete_' . $this->fileEntity->id]) && $values[$this->name . '_delete_' . $this->fileEntity->id] == 'on';
				if ($delete) {
					return NULL;
				}
			}

			if ($values) {
				if ($this->multi) {
					for ($i = 0; $i < 20; $i++) {
						if ($values[$this->name . '-' . $i]->isOk()) {
							$this->fileEntity[] = $entity = new FileEntity();
							$entity->setFile($values[$this->name . '-' . $i]);
						}
					}
				} else {
					if ($values[$this->name]->isOk()) {
						$this->fileEntity = $entity = new FileEntity();
						$entity->setFile($values[$this->name]);
					}
				}
			}
			$this->_valueLoaded = true;
		}

		return $this->fileEntity;
	}



	public function getControl()
	{
		$control = Html::el();

		if ($this->multi) {
			for ($i = 0; $i < 20; $i++) {
				$parent = parent::getControl();
				$parent->name .= '-' . $i;
				$parent->onChange = 'if($(this).val()) { $("#' . $parent->id . '-' . ($i + 1) . '").parent().show(); }';
				$parent->id .= '-' . $i;

				$control->add($d = Html::el('div'));
				$d->add($parent);

				if ($i > 0) {
					$d->style = 'display: none;';
				}
			}
		} else {
			$control->add(parent::getControl());
		}

		if ($this->fileEntity) {
			if ($this->multi) {
				$files = $this->fileEntity;
			} else {
				$files = array();
				if ($this->fileEntity) {
					$files[] = $this->fileEntity;
				}
			}

			$div = $control->create('div');
			foreach ($files as $file) {
				$div2 = $div->create('div')->setText('current file: ');
				$div2->create('i', array(
					'style' => 'icon-file',
				));
				$div2->create('a', array(
					'href' => $file->getFileUrl(),
				))->setText($file->getName());
				$div2->create('span')->setText(' ');
				$div2->create('input', array('id' => 'delete-file', 'type' => 'checkbox', 'name' => $this->name . '_delete_' . $file->id));
				$div2->create('label', array('for' => 'delete-file'))->setText(' ' . ($this->translator ? $this->translator->translate('delete') : 'delete'));
			}
		}

		return $control;
	}



	public function setMulti()
	{
		$this->multi = TRUE;
		return $this;
	}

}
