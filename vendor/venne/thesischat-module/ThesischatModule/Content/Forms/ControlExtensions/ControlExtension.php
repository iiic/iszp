<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Content\Forms\ControlExtensions;

use DoctrineModule\Forms\Mappers\EntityMapper;
use Nette\Object;
use Venne\Forms\Form;
use Venne\Forms\IControlExtension;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>, Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ControlExtension extends Object implements IControlExtension
{

	/**
	 * @param Form $form
	 */
	public function check($form)
	{
		if (!$form->getMapper() instanceof EntityMapper) {
			throw new \Nette\InvalidArgumentException("Form mapper must be instanceof 'EntityMapper'. '" . get_class($form->getMapper()) . "' is given.");
		}

		if (!$form->getData() instanceof \DoctrineModule\Entities\IEntity) {
			throw new \Nette\InvalidArgumentException("Form data must be instanceof 'IEntity'. '" . get_class($form->getData()) . "' is given.");
		}
	}


	/**
	 * @return array
	 */
	public function getControls(Form $form)
	{
		$this->check($form);

		return array(
			'fileEntityInput',
		);
	}


	/**
	 * Adds upload input for FileEntity.
	 *
	 * @param $form
	 * @param $name
	 * @param null $label
	 * @return \DoctrineModule\Forms\Containers\EntityContainer
	 */
	public function addFileEntityInput($form, $name, $label = NULL)
	{
		return $form[$name] = new \ThesischatModule\Content\Forms\Controls\FileEntityControl($label);
	}
}
