<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS., Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Components;

use ThesischatModule\Repositories\LibraryRepository;
use CmsModule\Content\SectionControl;
use ThesischatModule\Forms\LibraryFormFactory;


/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com> a Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class LibraryControl extends SectionControl
{

	/** @var LibraryRepository */
	protected $libraryRepository;

	/** @var LibraryFormFactory */
	protected $libraryFormFactory;


	/**
	 * @param \ThesischatModule\Repositories\LibraryRepository $libraryRepository
	 * @param \ThesischatModule\Forms\LibraryFormFactory $libraryFormFactory
	 */
	public function __construct(LibraryRepository $libraryRepository, LibraryFormFactory $libraryFormFactory)
	{
		parent::__construct();

		$this->libraryRepository = $libraryRepository;
		$this->libraryFormFactory = $libraryFormFactory;
	}


	protected function createComponentTable()
	{
		$table = new \CmsModule\Components\Table\TableControl;
		$table->setTemplateConfigurator($this->templateConfigurator);
		$table->setRepository($this->libraryRepository);

		$pageId = $this->entity->id;
		$table->setDql(function ($sql) use ($pageId) {
			$sql = $sql->andWhere('a.page = :page')->setParameter('page', $pageId);
			return $sql;
		});

		// forms
		$repository = $this->libraryRepository;
		$entity = $this->entity;
		$form = $table->addForm($this->libraryFormFactory, 'Library', function () use ($repository, $entity) {
			return $repository->createNew(array($entity));
		}, \CmsModule\Components\Table\Form::TYPE_LARGE);

		// navbar
		$table->addButtonCreate('create', 'Create new', $form, 'file');

		$table->addColumn('student', 'student')
			->setWidth('25%')
			->setSortable(TRUE)
			->setFilter();

		$table->addColumn('thesis', 'thesis')
			->setWidth('25%')
			->setSortable(TRUE)
			->setFilter();

		$table->addColumn('leader', 'leader')
			->setWidth('25%')
			->setSortable(TRUE)
			->setFilter();

		$table->addColumn('defense_year', 'Defense year')
			->setWidth('5%')
			->setSortable(TRUE)
			->setFilter();

		$table->addActionEdit('edit', 'Edit', $form);
		$table->addActionDelete('delete', 'Delete');

		// global actions
		$table->setGlobalAction($table['delete']);

		return $table;
	}


	public function render()
	{
		$this['table']->render();
	}

}
