<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS., Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Entities;

use Doctrine\ORM\Mapping as ORM;
use ThesiscollectionModule\Entities\ThesiscollectionEntity;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com> a Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\ThesischatModule\Repositories\LibraryRepository")
 * @ORM\Table(name="thesischat_library")
 */
class LibraryEntity extends \DoctrineModule\Entities\IdentifiedEntity
{

	/**
	 * @var PageEntity
	 * @ORM\ManyToOne(targetEntity="\ThesischatModule\Entities\PageEntity", inversedBy="library")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var ThesiscollectionEntity
	 * @ORM\ManyToOne(targetEntity="\ThesiscollectionModule\Entities\ThesiscollectionEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $thesis;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $student;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $leader;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $defense_year;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $size;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $annexes;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	//protected $opponent;

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $annotation = '';

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $keywords;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $location;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;

	/**
	 * @var \CmsModule\Content\Entities\FileEntity
	 * @ORM\OneToOne(targetEntity="\CmsModule\Content\Entities\FileEntity", cascade={"all"}, orphanRemoval=true)
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 */
	protected $thesis_pdf;



	/**
	 * @param PageEntity $pageEntity
	 * @param ThesiscollectionEntity $thesis
	 */
	public function __construct(PageEntity $pageEntity)
	{
		parent::__construct();

		$this->page = $pageEntity;
		$this->defense_year = Date("Y", Time());
		$this->created = new \Nette\DateTime();
	}



	/**
	 * @param \ThesischatModule\Entities\PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}



	/**
	 * @return \ThesischatModule\Entities\PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}



	/**
	 * @param \ThesiscollectionModule\Entities\ThesiscollectionEntity $thesis
	 */
	public function setThesis($thesis)
	{
		$this->thesis = $thesis;
	}



	/**
	 * @return \ThesiscollectionModule\Entities\ThesiscollectionEntity
	 */
	public function getThesis()
	{
		return $this->thesis;
	}



	/**
	 * @param \CmsModule\Security\Entities\UserEntity $student
	 */
	public function setStudent($student)
	{
		$this->student = $student;
	}



	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getStudent()
	{
		return $this->student;
	}



	/**
	 * @param \CmsModule\Security\Entities\UserEntity $leader
	 */
	public function setLeader($leader)
	{
		$this->leader = $leader;
	}



	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getLeader()
	{
		return $this->leader;
	}



	/**
	 * @param integer $defense_year
	 */
	public function setDefense_year($defense_year)
	{
		$this->defense_year = $defense_year;
	}



	/**
	 * @return integer
	 */
	public function getDefense_year()
	{
		return $this->defense_year;
	}



	/**
	 * @param integer $size
	 */
	public function setSize($size)
	{
		$this->size = $size;
	}



	/**
	 * @return integer
	 */
	public function getSize()
	{
		return $this->size;
	}



	/**
	 * @param integer $annexes
	 */
	public function setAnnexes($annexes)
	{
		$this->annexes = $annexes;
	}



	/**
	 * @return integer
	 */
	public function getAnnexes()
	{
		return $this->annexes;
	}



	/**
	 * @param string $annotation
	 */
	public function setAnnotation($annotation)
	{
		$this->annotation = $annotation;
	}



	/**
	 * @return string
	 */
	public function getAnnotation()
	{
		return $this->annotation;
	}



	/**
	 * @param string $keywords
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}



	/**
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}



	/**
	 * @param string $location
	 */
	public function setLocation($location)
	{
		$this->location = $location;
	}



	/**
	 * @return string
	 */
	public function getLocation()
	{
		return $this->location;
	}



	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}



	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}



	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;
	}



	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}



	/**
	 * @param \CmsModule\Content\Entities\FileEntity $thesis_pdf
	 */
	public function setThesis_pdf($thesis_pdf)
	{
		$this->thesis_pdf = $thesis_pdf;

		/*if ($this->thesis_pdf instanceof FileEntity) {
			$this->thesis_pdf->setName('__wtf__' . $this->thesis_pdf->getName());
			$this->thesis_pdf->setInvisible(TRUE);
		}*/
	}



	/**
	 * @return \CmsModule\Content\Entities\FileEntity
	 */
	public function getThesis_pdf()
	{
		return $this->thesis_pdf;
	}

}
