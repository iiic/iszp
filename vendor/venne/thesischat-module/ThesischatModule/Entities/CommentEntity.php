<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Entities;

use Doctrine\ORM\Mapping as ORM;
use ThesiscollectionModule\Entities\ThesiscollectionEntity;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\ThesischatModule\Repositories\CommentRepository")
 * @ORM\Table(name="thesischat_comment")
 */
class CommentEntity extends \DoctrineModule\Entities\IdentifiedEntity
{
	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $text = '';

	/**
	 * @var PageEntity
	 * @ORM\ManyToOne(targetEntity="\ThesischatModule\Entities\PageEntity", inversedBy="comments")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var ThesiscollectionEntity
	 * @ORM\ManyToOne(targetEntity="\ThesiscollectionModule\Entities\ThesiscollectionEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $thesis;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $author;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $title = '';

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $viewed = false;



	/**
	 * @param PageEntity $pageEntity
	 * @param ThesiscollectionEntity $thesis
	 */
	public function __construct(PageEntity $pageEntity, ThesiscollectionEntity $thesis = NULL)
	{
		parent::__construct();

		$this->page = $pageEntity;
		$this->thesis = $thesis;
		$this->created = new \Nette\DateTime();
	}


	/**
	 * @param \CmsModule\Security\Entities\UserEntity $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}


	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getAuthor()
	{
		return $this->author;
	}


	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}


	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}


	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}


	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}


	/**
	 * @param \ThesischatModule\Entities\PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}


	/**
	 * @return \ThesischatModule\Entities\PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}

	
	/**
	 * @param \ThesiscollectionModule\Entities\ThesiscollectionEntity $thesis
	 */
	public function setThesis($thesis)
	{
		$this->thesis = $thesis;
	}


	/**
	 * @return \ThesiscollectionModule\Entities\ThesiscollectionEntity
	 */
	public function getThesis()
	{
		return $this->thesis;
	}


	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}


	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;
	}



	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}



	/**
	 * @param boolean $viewed
	 */
	public function setViewed($viewed)
	{
		$this->viewed = $viewed;
	}



	/**
	 * @return boolean
	 */
	public function getViewed()
	{
		return $this->viewed;
	}

}
