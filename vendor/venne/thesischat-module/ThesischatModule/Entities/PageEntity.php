<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\DoctrineModule\Repositories\BaseRepository")
 * @ORM\Table(name="thesischat_page")
 * @ORM\DiscriminatorEntry(name="thesischatPage")
 */
class PageEntity extends BasePageEntity
{

	/**
	 * @var ArrayCollection|CommentEntity[]
	 * @ORM\OneToMany(targetEntity="CommentEntity", mappedBy="page")
	 */
	protected $comments;

	/**
	 * @var string
	 * @ORM\Column(type="integer")
	 */
	protected $itemsPerPage = 10;



	public function __construct()
	{
		parent::__construct();

	}



	/**
	 * @param $comments
	 */
	public function setComments($comments)
	{
		$this->comments = $comments;
	}



	/**
	 * @return ArrayCollection|CommentEntity[]
	 */
	public function getComments()
	{
		return $this->comments;
	}



	/**
	 * @param string $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
	}



	/**
	 * @return string
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}

}
