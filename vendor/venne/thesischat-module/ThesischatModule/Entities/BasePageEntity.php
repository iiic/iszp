<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Entities;

use Doctrine\ORM\Mapping as ORM;


/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class BasePageEntity extends \CmsModule\Content\Entities\PageEntity
{

	/**
	 * @var RouteEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Content\Entities\RouteEntity", cascade={"persist", "remove", "detach"})
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $notificationRoute;



	public function __construct()
	{
		parent::__construct();

		$this->mainRoute->type = 'Thesischat:Default:default';

		$this->routes[] = $this->notificationRoute = new \CmsModule\Content\Entities\RouteEntity;
		$this->notificationRoute->setTitle('Notification');
		$this->notificationRoute->setParent($this->mainRoute);
		$this->notificationRoute->setType('Thesischat:Notification:default');
		$this->notificationRoute->setLocalUrl('notification.js');
		$this->notificationRoute->setPage($this);
	}



	/**
	 * @return \ThesischatModule\Entities\RouteEntity
	 */
	public function getNotificationRoute()
	{
		return $this->notificationRoute;
	}

}
