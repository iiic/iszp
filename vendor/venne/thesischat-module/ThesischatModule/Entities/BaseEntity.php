<?php

namespace ThesischatModule\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use Nette\Utils\Strings;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 * @ORM\MappedSuperclass
 */
abstract class BaseEntity extends \DoctrineModule\Entities\IdentifiedEntity implements Translatable
{

	/**
	 * @var PageEntity
	 * @ORM\ManyToOne(targetEntity="PageEntity")
	 * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var \CmsModule\Content\Entities\RouteEntity
	 * @ORM\OneToOne(targetEntity="\CmsModule\Content\Entities\RouteEntity", cascade={"persist"}, orphanRemoval=true)
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $route;



	public function __construct(PageEntity $page, $name)
	{
		$this->setPage($page);
		$this->setName($name);

		$this->types = new ArrayCollection;
	}



	public function __destruct()
	{
		$this->removeRoute();
	}



	/**
	 * @return PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}



	/**
	 * @param PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}



	/**
	 * @return \CmsModule\Content\Entities\RouteEntity
	 */
	public function getRoute()
	{
		return $this->route;
	}



	/**
	 * @param \CmsModule\Content\Entities\RouteEntity $route
	 */
	public function setRoute($route)
	{
		$this->route = $route;
	}



	protected function createRoute($link)
	{
		$this->route = new \CmsModule\Content\Entities\RouteEntity;
		$this->route->setType($link);
		$this->route->setLocalUrl(Strings::webalize($this->name));
		$this->page->routes[] = $this->route;
		$this->route->setPage($this->page);
		$this->route->setParent($this->page->mainRoute);
	}



	protected function removeRoute()
	{
		//$this->page->routes->removeElement($this->route);
		unset($this->route);
	}

}
