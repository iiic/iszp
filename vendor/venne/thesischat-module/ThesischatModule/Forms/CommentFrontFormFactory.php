<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Forms;

use Venne\Forms\Form;
use Nette\Security\User;
use DoctrineModule\Forms\FormFactory;
use DoctrineModule\Forms\Mappers\EntityMapper;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class CommentFrontFormFactory extends FormFactory
{

	/** @var User */
	protected $user;



	/**
	 * @param \DoctrineModule\Forms\Mappers\EntityMapper $mapper
	 * @param \Nette\Security\User $user
	 */
	public function __construct(EntityMapper $mapper, User $user)
	{
		parent::__construct($mapper);

		$this->user = $user;
	}


	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addProtection();

		$form->addText('title', 'Title');

		$form->addTextArea('text', 'Text')
			->setRequired();

		$form->addSaveButton('Save');
	}


	
	public function handleSave(Form $form)
	{
		$form->data->author = $this->mapper->getEntityManager()->getRepository('CmsModule\Security\Entities\UserEntity')->findOneBy(array('email' => $this->user->identity->getId()));

		parent::handleSave($form);
	}

}
