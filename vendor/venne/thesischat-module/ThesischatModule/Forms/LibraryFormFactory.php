<?php // Administrační část - tohle je továrna na formulář pro editaci komentáře

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS., Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com> a Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class LibraryFormFactory extends FormFactory
{


	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();
		//$form->addDateTime('created', 'Created');

		$form->addManyToOne('student', 'Student');
		$form->addManyToOne('leader', 'Leader');

		$form->addManyToOne('thesis', 'Thesis Name');

		$form->addSelect('defense_year', 'Defense Year', array(
				Date("Y", Time())-3 => Date("Y", Time())-3,
				Date("Y", Time())-2 => Date("Y", Time())-2,
				Date("Y", Time())-1 => Date("Y", Time())-1,
				Date("Y", Time()) => Date("Y", Time()),
				Date("Y", Time())+1 => Date("Y", Time())+1,
				Date("Y", Time())+2 => Date("Y", Time())+2,
				Date("Y", Time())+3 => Date("Y", Time())+3,
			));

		$form->addText('size', 'Count of pages')
			->setType('number')
			->addRule(Form::RANGE, 'Počet stran musí být od %d do %d', array(20, 200));

		$form->addText('annexes', 'Count of annexes')
			->setType('number')
			->addRule(Form::RANGE, 'Počet příloh musí být kladné celé číslo menší než %d', array(0, 100));

		$form->addTextArea('annotation', 'Annotation');

		$form->addTextArea('keywords', 'Keywords');

		$form->addText('location', 'Location of thesis');

		$form->addSaveButton('Save');
	}

}
