<?php // Administrační část - tohle je továrna na formulář pro editaci komentáře

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class CommentFormFactory extends FormFactory
{


	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();
		$form->addDateTime('created', 'Created');
		$form->addCheckbox('viewed', 'Viewed');

		$form->addGroup('Dependencies');
		$form->addManyToOne('author', 'Author');
		$form->addManyToOne('thesis', 'Thesis');

		$form->addGroup('Text');
		$form->addText('title', 'Title')
			->addRule(Form::FILLED, 'Don\'t forget to fill title');
		$form->addTextArea('text', 'Text')
			->setRequired(TRUE)
			->getControlPrototype()->attrs['class'][] = 'span12';

		$form->addSaveButton('Save');
	}

}
