<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS., Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Forms;

use Venne\Forms\Form;
use Nette\Security\User;
use DoctrineModule\Forms\FormFactory;
use DoctrineModule\Forms\Mappers\EntityMapper;
use Nette\Utils\Html;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com> a Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class LibraryFrontFormFactory extends FormFactory
{

	/** @var User */
	protected $user;



	/**
	 * @param \DoctrineModule\Forms\Mappers\EntityMapper $mapper
	 * @param \Nette\Security\User $user
	 */
	public function __construct(EntityMapper $mapper, User $user)
	{
		parent::__construct($mapper);

		$this->user = $user;
	}



	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \ThesischatModule\Content\Forms\ControlExtensions\ControlExtension(),
		));
	}



	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addProtection();

		$form->addGroup("záznamy pro knihovnu")
			->setOption("description", Html::el("a", "zobrazit formulář pro knihovnu ▼")->href("#frm-libraryEditFrontForm"));

		$form->addSelect('defense_year', 'Defense Year', array(
			Date("Y", Time())-3 => Date("Y", Time())-3,
			Date("Y", Time())-2 => Date("Y", Time())-2,
			Date("Y", Time())-1 => Date("Y", Time())-1,
			Date("Y", Time()) => Date("Y", Time()),
			Date("Y", Time())+1 => Date("Y", Time())+1,
			Date("Y", Time())+2 => Date("Y", Time())+2,
			Date("Y", Time())+3 => Date("Y", Time())+3,
		));

		$form->addText('size', 'Count of Pages')
			->setType('number')
			->addRule(Form::RANGE, 'Počet stran musí být od %d do %d', array(20, 200));

		$form->addText('annexes', 'Count of Annexes')
			->setType('number')
			->addRule(Form::RANGE, 'Počet příloh musí být kladné celé číslo menší než %d', array(0, 100));

		$form->addTextArea('annotation', 'Annotation');

		$form->addTextArea('keywords', 'Keywords');

		$form->addText('location', 'Location of thesis');

		$form->addFileEntityInput('thesis_pdf', 'Pdf')
			->addRule(function ($control) { // vlastní validační funkce
				$loggedUser = $this->mapper->getEntityManager()->getRepository('CmsModule\Security\Entities\UserEntity')->findOneBy(array('email' => $this->user->id));
				$loggedUserLibrary = $this->mapper->getEntityManager()->getRepository('ThesischatModule\Entities\LibraryEntity')->findOneBy(array('student' => $loggedUser->id));
				$conflict = $this->mapper->getEntityManager()->getRepository('CmsModule\Content\Entities\FileEntity')->findOneBy(array('path' => $control->getValue()->file->name));
				
				if($conflict && ($loggedUserLibrary->thesis_pdf->path != $control->getValue()->file->name)) {
					return false; //chyba
				} else {
					return true; //ok
				}
			}, 'chyba');

		$form->addSaveButton('Save');
	}



	public function handleSave(Form $form)
	{
		$form->data->student = $this->mapper->getEntityManager()->getRepository('CmsModule\Security\Entities\UserEntity')->findOneBy(array('email' => $this->user->identity->id));
		$form->data->thesis = $this->mapper->getEntityManager()->getRepository('ThesiscollectionModule\Entities\ThesiscollectionEntity')->findOneBy(array('booked_by' => $form->data->student));
		$form->data->leader = $this->mapper->getEntityManager()->getRepository('CmsModule\Security\Entities\UserEntity')->findOneBy(array('id' => $form->data->thesis->leader));

		parent::handleSave($form);
	}

}
