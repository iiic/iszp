<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Presenters;

use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use ThesischatModule\Repositories\CommentRepository;
use CmsModule\Security\Repositories\UserRepository;
use Nette\Diagnostics\Debugger;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */

class NotificationPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $forId;

	/** @var LinkRepository */
	protected $linkRepository;

	/** @var CommentRepository */
	protected $commentRepository;

	/** @var UserRepository */
	protected $userRepository;

	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $linkRepository
	 */
	public function injectThesiscollectionRepository(ThesiscollectionRepository $linkRepository)
	{
		$this->linkRepository = $linkRepository;
	}



	/**
	 * @param \ThesischatModule\Repositories\CommentRepository $commentRepository
	 */
	public function injectCommentRepository(CommentRepository $commentRepository)
	{
		$this->commentRepository = $commentRepository;
	}



	/**
	 * @param \CmsModule\Security\Repositories\UserRepository $userRepository
	 */
	public function injectUserRepository(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}



	public function __construct()
	{
		parent::__construct();

		//$this->absoluteUrls = true;
		Debugger::$bar = false;
	}



	public function startup()
	{
		parent::startup();

		$count = 0;

		if(\Nette\Utils\Strings::contains($this->forId, '@')) {
			$id = $this->userRepository->findOneBy(array('email' => $this->forId));
		} else {
			$id = (integer) $this->forId;
		}

		$leader = $this->linkRepository->findBy(array('leader' => $id));

		foreach($leader as $thesis) {

			$count += $this->commentRepository->createQueryBuilder("a")
				->select('count(a.id)')
				->where('a.author = :author')
				->andWhere('a.viewed = 0')
				->setParameters(array('author' => $thesis->booked_by))
				->getQuery()
				->getSingleScalarResult();

		}

		$student = $this->linkRepository->findOneBy(array('booked_by' => $id));

		if($student) {

			$count = $this->commentRepository->createQueryBuilder("a")
				->select('count(a.id)')
				->where('a.thesis = :id')
				->andWhere('a.author = :author')
				->andWhere('a.viewed = 0')
				->setParameters(array('author' => $student->leader, 'id' => $student->id))
				->getQuery()
				->getSingleScalarResult();

		}

		$this->template->unreadNotificationsCount = $count;

	}

}
