<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesischatModule\Presenters;

use ThesischatModule\Forms\CommentFrontFormFactory;
use ThesischatModule\Forms\LibraryFrontFormFactory;
use ThesischatModule\Repositories\CommentRepository;
use ThesischatModule\Repositories\LibraryRepository;
use Nette\DateTime;
use Nette\Forms\Form;
use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use CmsModule\Security\Repositories\UserRepository;

/**
 * @secured
 */
class DefaultPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $key; // identifikátor položky chatu

	/** @persistent */
	public $thesisId; // identifikátor práce

	/** @var CommentRepository */
	protected $commentRepository;

	/** @var LibraryRepository */
	protected $libraryRepository;

	/** @var LinkRepository */
	protected $linkRepository;

	/** @var UserRepository */
	protected $userRepository;

	/** @var CommentFrontFormFactory */
	protected $commentFormFactory;

	/** @var LibraryFrontFormFactory */
	protected $libraryFormFactory;



	/**
	 * @param \ThesischatModule\Repositories\CommentRepository $commentRepository
	 */
	public function injectCommentRepository(CommentRepository $commentRepository)
	{
		$this->commentRepository = $commentRepository;
	}



	/**
	 * @param \ThesischatModule\Repositories\LibraryRepository $libraryRepository
	 */
	public function injectLibraryRepository(LibraryRepository $libraryRepository)
	{
		$this->libraryRepository = $libraryRepository;
	}



	/**
	 * @param \ThesischatModule\Forms\CommentFrontFormFactory $commentFormFactory
	 */
	public function injectCommentFormFactory(CommentFrontFormFactory $commentFormFactory)
	{
		$this->commentFormFactory = $commentFormFactory;
	}



	/**
	 * @param \ThesischatModule\Forms\LibraryFrontFormFactory $libraryFormFactory
	 */
	public function injectLibraryFormFactory(LibraryFrontFormFactory $libraryFormFactory)
	{
		$this->libraryFormFactory = $libraryFormFactory;
	}



	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $linkRepository
	 */
	public function injectThesiscollectionRepository(ThesiscollectionRepository $linkRepository)
	{
		$this->linkRepository = $linkRepository;
	}



	/**
	 * @param \CmsModule\Security\Repositories\UserRepository $userRepository
	 */
	public function injectUserRepository(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}



	protected function createComponentCommentFrontForm()
	{
		$form = $this->commentFormFactory->invoke($this->commentRepository->createNew(array($this->page,$this->linkRepository->findOneBy(array('id' => $this->thesisId)))));
		$form->onSuccess[] = $this->formSuccess;
		return $form;
	}



	protected function createComponentLibraryEditFrontForm()
	{
		$entity = $this->libraryRepository->findOneBy(array('student' => $this->userRepository->findOneBy(array('email' => $this->user->identity->id))));
		if($entity) {
			$entity = $this->libraryRepository->find($entity->id);
			$form = $this->libraryFormFactory->invoke($entity);
		} else {
			$form = $this->libraryFormFactory->invoke($this->libraryRepository->createNew(array($this->page)));
		}
		$form->onAttached[] = function (Form $form) {
			if ($form->isSubmitted()) {
				$form->data->updated = new DateTime;
			}
		};
		$form->onSuccess[] = $this->libraryFormSuccess;
		return $form;
	}



	public function actionDefault()
	{
		if ($this->key) {
			$entity = $this->commentRepository->find($this->key);
			$this->template->authorId = $entity->author->email;
		}
	}



	/**
	* @secured(roles="admin, student, vedouci")
	*/
	public function handleEdit($id)
	{
		$this->key = $id;

		$this->redirect('this#frm-editForm', array('key' => $id));
	}



	/**
	* @secured(roles="admin, student, vedouci")
	*/
	public function handleDelete($id)
	{
		$entity = $this->commentRepository->find($id);

		$this->commentRepository->delete($entity);

		$this->flashMessage('Thesischat has been deleted.');
		$this->redirect('this');
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults($this->page->itemsPerPage)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->commentRepository->createQueryBuilder("a")
			->andWhere('a.thesis = :thesis')->setParameter('thesis', $this->thesisId)
			->andWhere('a.page = :page')->setParameter('page', $this->page->id);
	}



	public function setViewed()
	{
		if ($this->user->isInRole('student')) {
			$entity = $this->getQueryBuilder()
				->where('a.thesis = :thesis')
				->andWhere('a.author != :author')
				->setParameters(array('thesis' => $this->thesisId, 'author' => $this->template->thesis->booked_by->id))
				->getQuery()
				->getResult();
		} elseif ($this->user->isInRole('vedouci')) {
			$entity = $this->getQueryBuilder()
				->where('a.thesis = :thesis')
				->andWhere('a.author != :author')
				->setParameters(array('thesis' => $this->thesisId, 'author' => $this->template->thesis->leader->id))
				->getQuery()
				->getResult();
		} elseif ($this->user->isInRole('admin') || $this->user->isInRole('garant')) {
			$entity = $this->getQueryBuilder()
				->where('a.thesis = :thesis')
				->setParameters(array('thesis' => $this->thesisId))
				->getQuery()
				->getResult();
		}
		foreach ($entity as $key => $value) {
			$entity[$key]->viewed = true;
		}
		if(!$this->user->isInRole('admin')) {
			$this->commentRepository->save($entity);
		}
		return true;
	}



	public function getUnreadCount($id)
	{
		if ($this->user->isInRole('vedouci')) {
			$entity = $this->getQueryBuilder()
				->select('count(a.id)')
				->where('a.viewed = 0')
				->andWhere('a.thesis = :id')
				->andWhere('a.author != :author')
				->setParameters(array('author' => $this->template->loggedUser, 'id' => $id))
				->getQuery()
				->getSingleScalarResult();
		} else {
			$entity = count($this->commentRepository->findBy(array('thesis' => $id, 'viewed' => 0)));
		}
		return $entity;
	}



	public function getLibraryTable($id)
	{
		return $this->libraryRepository->findOneBy(array('student' => $id));
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	public function formSuccess()
	{
		if($this->lang == 'cs') {
			$this->flashMessage('Záznam chatu byl uložen');
		} else {
			$this->flashMessage('Thesischat has been saved.');
		}
		$this->redirect('this');
	}



	public function libraryFormSuccess()
	{
		$this->flashMessage('Library item has been saved succesfully.');
		$this->redirect('this');
	}



	protected function createComponentEditForm()
	{
		$entity = $this->commentRepository->find($this->key);

		$form = $this->commentFormFactory->invoke($entity);
		$form->onAttached[] = function (Form $form) {
			if ($form->isSubmitted()) {
				$form->data->updated = new DateTime;
			}
		};
		$form->onSuccess[] = $this->editFormSuccess;
		return $form;
	}



	public function editFormSuccess()
	{
		$this->flashMessage('Thesischat has been updated.');
		$this->redirect('this', array('key' => NULL)); // tu nejhe # z nějakého neznámého důvodu
	}



	public function renderDefault()
	{
		if( $this->user->isLoggedIn() ) {
			$this->template->loggedUser = $this->userRepository->findOneBy(array('email' => $this->user->identity->id));

			if ($this->thesisId) {
				$this->template->thesis = $this->linkRepository->findOneBy(array('id' => $this->thesisId));

			} else {

				if ( $this->user->isInRole('vedouci') ) {
					$this->template->theses = $this->linkRepository->createQueryBuilder("a")->where('a.leader = :leader')->setParameters(array('leader' => $this->template->loggedUser))->getQuery()->getResult();
				} elseif( $this->user->isInRole('admin') || $this->user->isInRole('garant') ) {
					$this->template->theses = $this->linkRepository->createQueryBuilder("a")->getQuery()->getResult();
				} else { // student
					$this->template->thesis = $this->linkRepository->findOneBy(array('booked_by' => $this->template->loggedUser)); // najdu, jestli akruálně přihlášený uživatel má už rezervovanou nějakou práci
					if($this->template->thesis) {
						$this->thesisId = $this->template->thesis->id;
					}

				}

			}

		}
		$this->invalidateControl('thesischat');
	}



	public function createTemplate($class = NULL)
	{
		$template = parent::createTemplate($class);
		$template->registerHelper('gravatar', function ($email, $defaultUri = NULL, $size = 50, $rating = 'pg') {
			if ($defaultUri) {
				$defaultUri = '&d='.urlencode($defaultUri);
			}
			return 'http://www.gravatar.com/avatar/'.md5(strtolower($email)).'?s='.$size.'&r='.$rating.$defaultUri;
		});
		return $template;
	}

}
