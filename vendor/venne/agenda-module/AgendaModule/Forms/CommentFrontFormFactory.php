<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Forms;

use Venne\Forms\Form;
use Nette\Security\User;
use DoctrineModule\Forms\FormFactory;
use DoctrineModule\Forms\Mappers\EntityMapper;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class CommentFrontFormFactory extends FormFactory
{

	/** @var User */
	protected $user;

	/** @persistent */
	public $editId;


	/**
	 * @param \DoctrineModule\Forms\Mappers\EntityMapper $mapper
	 * @param \Nette\Security\User $user
	 */
	public function __construct(EntityMapper $mapper, User $user)
	{
		parent::__construct($mapper);

		$this->user = $user;
	}


	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addProtection();

		$form->addSelect('event_id', 'Event id', \AgendaModule\Components\Table\Columns\EventsColumn::$event_ids)
			->setRequired('You must choose an event.');

		$form->addDateTime('event_date', 'Event date')
			->setRequired('You must specify the date of the event.');

		$form->addTextArea('text', 'Text')
			->getControlPrototype()->attrs['class'][] = 'span12';

		$form->addSaveButton('Save');
	}


	public function handleSave(Form $form)
	{
		if ($this->user->isLoggedIn()) {
			$form->data->author = $this->mapper->getEntityManager()->getRepository('CmsModule\Security\Entities\UserEntity')->findOneBy(array('email' => $this->user->identity->getId()));
		} else {
			$form->data->author = $form['author']->getValue();
		}

		parent::handleSave($form);
	}
}
