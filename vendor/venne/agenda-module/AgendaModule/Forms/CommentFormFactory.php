<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class CommentFormFactory extends FormFactory
{

	/** @persistent */
	public $editId;

	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{

		$form->addSelect('event_id', 'Event id', \AgendaModule\Components\Table\Columns\EventsColumn::$event_ids)
			->setRequired('x');

		$form->addDateTime('event_date', 'Event date');

		$form->addEditor('text', 'Text')
			->getControlPrototype()->attrs['class'][] = 'span12';

		$form->addSaveButton('Save');
	}

}
