<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Presenters;

use AgendaModule\Forms\CommentFrontFormFactory;
use AgendaModule\Repositories\CommentRepository;
use Nette\DateTime;
use Nette\Forms\Form;

/**
 * @secured
 */
class DefaultPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $key;

	/** @var CommentRepository */
	protected $commentRepository;

	/** @var CommentFrontFormFactory */
	protected $commentFormFactory;



	/**
	 * @param \AgendaModule\Repositories\CommentRepository $commentRepository
	 */
	public function injectCommentRepository(CommentRepository $commentRepository)
	{
		$this->commentRepository = $commentRepository;
	}



	/**
	 * @param \AgendaModule\Forms\CommentFrontFormFactory $commentFormFactory
	 */
	public function injectCommentFormFactory(CommentFrontFormFactory $commentFormFactory)
	{
		$this->commentFormFactory = $commentFormFactory;
	}



/*
	public function actionDefault()
	{
		if ($this->key) {
			$entity = $this->commentRepository->find($this->key);
		}
	}
*/



	/**
	* @secured(roles="admin, studijni")
	*/
	public function handleEdit($id)
	{
		$this->key = $id;

		$this->redirect('this', array('key' => $id));
	}



	/**
	* @secured(roles="admin, studijni")
	*/
	public function handleDelete($id)
	{
		$entity = $this->commentRepository->find($id);

		$this->commentRepository->delete($entity);

		$this->flashMessage('Event has been deleted.');
		$this->redirect('this');
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults($this->page->itemsPerPage)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->commentRepository->createQueryBuilder("a")
			->andWhere('a.page = :page')->setParameter('page', $this->page->id);
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	protected function createComponentForm()
	{
		$form = $this->commentFormFactory->invoke($this->commentRepository->createNew(array($this->page)));
		$form->onSuccess[] = $this->formSuccess;
		return $form;
	}



	public function formSuccess()
	{
		$this->flashMessage('Event has been saved.');
		$this->redirect('this');
	}



	protected function createComponentEditForm()
	{
		$entity = $this->commentRepository->find($this->key);

		$form = $this->commentFormFactory->invoke($entity);
		$form->onAttached[] = function (Form $form) {
			if ($form->isSubmitted()) {
				$form->data->updated = new DateTime;
			}
		};
		$form->onSuccess[] = $this->editFormSuccess;
		return $form;
	}



	public function editFormSuccess()
	{
		$this->flashMessage('Event has been updated.');
		$this->redirect('this', array('key' => NULL));
	}



	public function renderDefault()
	{
		$this->invalidateControl('agenda');
	}

}
