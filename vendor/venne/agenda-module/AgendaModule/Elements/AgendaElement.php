<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Elements;

use DoctrineModule\Repositories\BaseRepository;
use Nette\DateTime;
use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use ThesisarchiveModule\Repositories\ThesisprearchiveRepository;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class AgendaElement extends \CmsModule\Content\Elements\BaseElement
{

	/** @persistent */
	public $editId;

	/** @var BaseRepository */
	private $commentRepository;

	/** @var ThesiscollectionRepository */
	private $thesiscollectionRepository;

	/** @var ThesisprearchiveRepository */
	private $thesisprearchiveRepository;



	/**
	 * @param BaseRepository $commentRepository
	 */
	public function injectCommentRepository($commentRepository)
	{
		$this->commentRepository = $commentRepository;
	}



	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $thesiscollectionRepository
	 */
	public function injectThesiscollectionRepository(ThesiscollectionRepository $thesiscollectionRepository)
	{
		$this->thesiscollectionRepository = $thesiscollectionRepository;
	}



	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisprearchiveRepository $thesisprearchiveRepository
	 */
	public function injectThesisprearchiveRepository(ThesisprearchiveRepository $thesisprearchiveRepository)
	{
		$this->thesisprearchiveRepository = $thesisprearchiveRepository;
	}



	public function checkTime($query)
	{
		foreach($query as $item) {

			$event = $item->event_date;
			$now = new DateTime();
			if($now->diff($event)->format("%R") == '-') { // vrací + pokud je $event>$now jinak -

				if($item->event_id < 3) { // číslo je odvozeno od událostí, na které reaguje kalendář… tyto jsou v \AgendaModule\Components\Table\Columns\EventsColumn

					switch ($item->event_id) {
						case 0: //SZZ pro bakalářské studium
							$finished = $this->thesiscollectionRepository->createQueryBuilder("a")
								->where('a.specialization LIKE :specialization')
								->andWhere('a.booked_by != 0')
								->setParameters(array('specialization' => 'Bc: %'))
								->getQuery()
								->getResult(); // získám všechny práce, které by se teoreticky měly dostat do archivu
						break;

						case 1: //SZZ pro magisterské studium
							$finished = $this->thesiscollectionRepository->createQueryBuilder("a")
								->where('a.specialization LIKE :specialization')
								->andWhere('a.booked_by != 0')
								->setParameters(array('specialization' => 'Mgr: %'))
								->getQuery()
								->getResult(); // získám všechny práce, které by se teoreticky měly dostat do archivu
						break;

						case 2: //SZZ pro doktorandské studium
							$finished = $this->thesiscollectionRepository->createQueryBuilder("a")
								->where('a.specialization LIKE :specialization')
								->andWhere('a.booked_by != 0')
								->setParameters(array('specialization' => 'PhD: %'))
								->getQuery()
								->getResult(); // získám všechny práce, které by se teoreticky měly dostat do archivu
						break;
					}

					foreach ($finished as $thesis) {
						$newEntity = $this->thesisprearchiveRepository->createNew();
						$newEntity->thesis = $thesis;
						$newEntity->name = $thesis->name; // name je zděděné po rodičovsné třídě entity
						$this->thesisprearchiveRepository->save($newEntity);
					}

				}

				$this->commentRepository->delete($item); // smazat staré události z kalendáře

			}

		}

	}



	public function render()
	{
		$this->template->event_ids = \AgendaModule\Components\Table\Columns\EventsColumn::$event_ids;
		$this->template->render();
	}



	public function renderSetup()
	{
		echo $this['form']->render();
	}



	public function getItems()
	{
		$query = $this->getQueryBuilder();
		$this->checkTime($query->getQuery()->getResult());

		//$query->setMaxResults($this->getEntity()->itemsPerPage);
		$query->orderBy('a.event_date', 'ASC');

		return $query->getQuery()->getResult();
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->commentRepository->createQueryBuilder("a");
	}

}
