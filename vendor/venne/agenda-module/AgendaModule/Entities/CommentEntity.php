<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Entities;

use Doctrine\ORM\Mapping as ORM;
use CmsModule\Security\Entities\UserEntity;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\AgendaModule\Repositories\CommentRepository")
 * @ORM\Table(name="agenda_data")
 */
class CommentEntity extends \DoctrineModule\Entities\IdentifiedEntity // @ todo : přejmenovat na něco lepšího
{
	/**
	 * @var string
	 * @ORM\Column(type="string", length=250)
	 */
	protected $text = '';

	/**
	 * @var PageEntity
	 * @ORM\ManyToOne(targetEntity="\AgendaModule\Entities\PageEntity", inversedBy="comments")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $author;

	/**
	 * @var string
	 * @ORM\Column(type="smallint", length=2)
	 */
	protected $event_id;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $event_date;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;


	/**
	 * @param PageEntity $pageEntity
	 */
	public function __construct(PageEntity $pageEntity)
	{
		parent::__construct();

		$this->page = $pageEntity;
		$this->created = new \Nette\DateTime();
	}


	/**
	 * @param \CmsModule\Security\Entities\UserEntity $author
	 */
	public function setAuthor($author)
	{
		if ($author instanceof UserEntity) {
			$this->author = $author;
			return;
		}
	}


	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getAuthor()
	{
		return $this->author;
	}


	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}


	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}


	/**
	 * @param \AgendaModule\Entities\PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}


	/**
	 * @return \AgendaModule\Entities\PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}


	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}


	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


	/**
	 * @param integer $id
	 */
	public function setEvent_id($id)
	{
		$this->event_id = $id;
	}


	/**
	 * @return string
	 */
	public function getEvent_id()
	{
		return $this->event_id;
	}


	/**
	 * @param \DateTime $event_date
	 */
	public function setEvent_date($event_date)
	{
		$this->event_date = $event_date;
	}


	/**
	 * @return \DateTime
	 */
	public function getEvent_date()
	{
		return $this->event_date;
	}


	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;
	}


	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}
}
