<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\CmsModule\Content\Repositories\PageRepository")
 * @ORM\Table(name="agenda_page")
 * @ORM\DiscriminatorEntry(name="agendaPage")
 */
class PageEntity extends \CmsModule\Content\Entities\PageEntity
{

	/**
	 * @var ArrayCollection|CommentEntity[]
	 * @ORM\OneToMany(targetEntity="CommentEntity", mappedBy="page")
	 */
	protected $comments;

	/**
	 * @var string
	 * @ORM\Column(type="integer")
	 */
	protected $itemsPerPage = 10;


	public function __construct()
	{
		parent::__construct();

		$this->mainRoute->type = 'Agenda:Default:default';
	}


	/**
	 * @param $comments
	 */
	public function setComments($comments)
	{
		$this->comments = $comments;
	}


	/**
	 * @return ArrayCollection|CommentEntity[]
	 */
	public function getComments()
	{
		return $this->comments;
	}


	/**
	 * @param string $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
	}


	/**
	 * @return string
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}
}
