<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Components;

use AgendaModule\Repositories\CommentRepository;
use CmsModule\Content\SectionControl;
use AgendaModule\Forms\CommentFormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class TableControl extends SectionControl
{

	/** @var CommentRepository */
	protected $commentRepository;

	/** @var CommentFormFactory */
	protected $commentFormFactory;

	const TYPE_EVENTS = '\AgendaModule\Components\Table\Columns\EventsColumn';

	/**
	 * @param \AgendaModule\Repositories\CommentRepository $commentRepository
	 * @param \AgendaModule\Forms\CommentFormFactory $commentFormFactory
	 */
	public function __construct(CommentRepository $commentRepository, CommentFormFactory $commentFormFactory)
	{
		parent::__construct();

		$this->commentRepository = $commentRepository;
		$this->commentFormFactory = $commentFormFactory;
	}


	protected function createComponentTable()
	{
		$table = new \CmsModule\Components\Table\TableControl;
		$table->setTemplateConfigurator($this->templateConfigurator);
		$table->setRepository($this->commentRepository);

		$pageId = $this->entity->id;
		$table->setDql(function ($sql) use ($pageId) {
			$sql = $sql->andWhere('a.page = :page')->setParameter('page', $pageId);
			return $sql;
		});

		// forms
		$repository = $this->commentRepository;
		$entity = $this->entity;
		$form = $table->addForm($this->commentFormFactory, 'Comment', function () use ($repository, $entity) {
			return $repository->createNew(array($entity));
		}, \CmsModule\Components\Table\Form::TYPE_LARGE);

		// navbar
		$table->addButtonCreate('create', 'Create new', $form, 'file');

		$table->addColumn('text', 'Note')
			->setWidth('35%')
			->setSortable(TRUE)
			->setFilter();
		$table->addColumn('event_id', 'Event type', TableControl::TYPE_EVENTS)
			->setWidth('25%')
			->setSortable(TRUE);
		$table->addColumn('event_date', 'Date of event', \CmsModule\Components\Table\TableControl::TYPE_DATE)
			->setWidth('20%')
			->setSortable(TRUE);
		$table->addColumn('created', 'Created', \CmsModule\Components\Table\TableControl::TYPE_DATE_TIME)
			->setWidth('20%')
			->setSortable(TRUE);
		$table->addColumn('updated', 'Updated', \CmsModule\Components\Table\TableControl::TYPE_DATE_TIME)
			->setWidth('20%')
			->setSortable(TRUE);

		$table->addActionEdit('edit', 'Edit', $form);
		$table->addActionDelete('delete', 'Delete');

		// global actions
		$table->setGlobalAction($table['delete']);

		return $table;
	}


	public function render()
	{
		$this['table']->render();
	}
}
