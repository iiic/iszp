<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule\Components\Table\Columns;

use CmsModule\Components\Table\TableControl;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class EventsColumn extends \CmsModule\Components\Table\Columns\BaseColumn
{

	public static $event_ids = array(
		'události' => array(
			'SZZ pro bakalářské studium',
			'SZZ pro magisterské studium',
			'SZZ pro doktorantské studium',
			'odevzdání zadání bakalářských prací',
			'odevzdání zadání magisterských prácí',
			'odevzdání zadání doktorantských prácí',
			'uskutečnění bakalářského semináře',
			'uskutečnění magisterského semináře',
			'uskutečnění doktorantského semináře',
			'odevzdání bakalářských prací',
			'odevzdání magisterských prácí',
			'odevzdání doktorantských prácí',
		)
	);

	public function __construct(TableControl $table, $name, $title)
	{
		parent::__construct($table, $name, $title);

		$_this = $this;
		$this->callback = function ($entity) use ($_this) {
			$column = $entity->{$_this->name};
			return self::$event_ids['události'][$column];
		};
	}

}
