<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace AgendaModule;

use Venne\Module\ComposerModule;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class Module extends ComposerModule
{

}
