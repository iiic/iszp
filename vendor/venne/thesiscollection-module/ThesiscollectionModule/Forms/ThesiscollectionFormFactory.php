<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @@author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class ThesiscollectionFormFactory extends FormFactory
{

	protected function getControlExtensions()
	{
		return array(
			new \DoctrineModule\Forms\ControlExtensions\DoctrineExtension(),
			new \CmsModule\Content\ControlExtension(),
			new \FormsModule\ControlExtensions\ControlExtension(),
			new \CmsModule\Content\Forms\ControlExtensions\ControlExtension(),
		);
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();

		$form->addText('name', 'Thesis name')
			->setRequired('write name of thesis');

		$form->addSelect('specialization', 'Specialization', \ThesiscollectionModule\Components\Table\Columns\SpecializationsColumn::$specializations)
			->setRequired('select the specialization');

		$form->addManyToOne('leader', 'Leader');

		$form->addManyToOne('booked_by', 'Booked by');

		$form->addCheckbox('approved','Approved');

		$form->addGroup('Dates');
		$form->addDateTime('released', 'Release date');
		$form->addDateTime('expired', 'Expiry date');

		$form->addGroup();
		$form->addContentEditor('text', NULL, NULL, 20);

		$form->addSaveButton('Save');
	}

}
