<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Presenters;

use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use ThesiscollectionModule\Forms\ContentFormFactory;
use CmsModule\Security\Repositories\UserRepository;

/**
 * @secured
 */
class DefaultPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $key;

	/** @var ThesiscollectionRepository */
	private $thesiscollectionRepository;

	/** @var UserRepository */
	protected $userRepository;

	/** @var ContentFormFactory */
	protected $contentFormFactory;

	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $thesiscollectionRepository
	 */
	public function injectThesiscollectionRepository(ThesiscollectionRepository $thesiscollectionRepository)
	{
		$this->thesiscollectionRepository = $thesiscollectionRepository;
	}



	/**
	 * @param \CmsModule\Security\Repositories\UserRepository $userRepository
	 */
	public function injectUserRepository(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}



	/**
	 * @param \ThesiscollectionModule\Forms\ContentFormFactory $contentFormFactory
	 */
	public function injectContentFormFactory(ContentFormFactory $contentFormFactory)
	{
		$this->contentFormFactory = $contentFormFactory;
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults(9999)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->thesiscollectionRepository->createQueryBuilder("a")
			->leftJoin('a.route', 'r')
			->andWhere('r.published = :true')->setParameter('true', TRUE)
			->andWhere('a.page = :page')->setParameter('page', $this->page->id)
			->andWhere('a.released <= :released')->setParameter('released', new \Nette\DateTime())
			->andWhere('(a.expired >= :expired OR a.expired IS NULL)')->setParameter('expired', new \Nette\DateTime());
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	public function actionDefault()
	{
		if ($this->key) {
			$this->thesiscollectionRepository->find($this->key);
		}
	}



	public function renderDefault()
	{
		$this->template->itemsPerPage = $this->page->itemsPerPage;
	}



	public function handleEdit($id)
	{
		$this->template->action = 'edit';
		$this->template->loggedUser = $this->userRepository->findOneBy(array('email' => $this->user->identity->id));;

		if($id) {
			$this->key = $id;

			$this->redirect('this', array('key' => $id));
		}
	}



	public function handleAdd()
	{
		$this->template->action = 'add';
	}



	/**
	* @secured(roles="vedouci, admin")
	*/
	public function handleDelete($id)
	{
		$this->template->action = 'delete';

		if($id) {
			$entity = $this->thesiscollectionRepository->find($id);

			$this->thesiscollectionRepository->delete($entity);

			$this->flashMessage('Task has been deleted.');
			$this->redirect('this', array('do' => 'delete'));
		}

	}



	/**
	* @secured(roles="garant, studijni, admin")
	*/
	public function handleApprove($id)
	{
		$this->template->action = 'approve';

		if($id) {
			$entity = $this->thesiscollectionRepository->find($id);
			$entity->approved = 1;

			$this->thesiscollectionRepository->save($entity);

			$this->flashMessage('Task has been approved.');
			$this->redirect('this');
		}
	}



	protected function createComponentForm() // formulář pro přidání nové práce
	{
		$form = $this->contentFormFactory->invoke($this->thesiscollectionRepository->createNew(array($this->page)));
		$form->onSuccess[] = $this->formSuccess;
		return $form;
	}



	public function formSuccess()
	{
		$this->flashMessage('Task has been saved.');
		$this->redirect('this');
	}



	protected function createComponentEditForm()
	{
		$entity = $this->thesiscollectionRepository->find($this->key);

		$form = $this->contentFormFactory->invoke($entity);
		$form->onSuccess[] = $this->editFormSuccess;
		return $form;
	}



	public function editFormSuccess()
	{
		$this->flashMessage('Task has been updated.');
		$this->redirect('this', array('key' => NULL));
	}

}
