<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Presenters;

use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use CmsModule\Security\Repositories\UserRepository;
use ThesischatModule\Forms\CommentFrontFormFactory;
use ThesischatModule\Repositories\CommentRepository;

/**
 * @secured
 */
class ThesiscollectionPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $key;

	/** @persistent */
	public $chatLink = FALSE;

	/** @var \ThesiscollectionModule\Repositories\ThesiscollectionRepository */
	private $thesiscollectionRepository;

	/** @var UserRepository */
	protected $userRepository;

	/** @var \ThesischatModule\Forms\CommentFrontFormFactory $commentFrontFormFactor */
	private $commentFrontFormFactor;

	/** @var \ThesischatModule\Repositories\CommentRepository */
	private $thesischatRepository;



	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $thesiscollectionRepository
	 */
	public function injectThesiscollectionRepository(ThesiscollectionRepository $thesiscollectionRepository)
	{
		$this->thesiscollectionRepository = $thesiscollectionRepository;
	}

	/**
	 * @param \CmsModule\Security\Repositories\UserRepository $userRepository
	 */
	public function injectUserRepository(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	/**
	 * @param \ThesischatModule\Forms\CommentFrontFormFactory $commentFrontFormFactor
	 */
	public function injectCommentFrontFormFactor(CommentFrontFormFactory $commentFrontFormFactor)
	{
		$this->commentFrontFormFactor = $commentFrontFormFactor;
	}

	/**
	 * @param \ThesischatModule\Repositories\CommentRepository $thesischatRepository
	 */
	public function injectCommentRepository(CommentRepository $thesischatRepository)
	{
		$this->thesischatRepository = $thesischatRepository;
	}



	/**
	* @secured(roles="student")
	*/
	public function handleBook($id)
	{
		$actual = $this->thesiscollectionRepository->find($id); // najdu práci, podle id
		$actual->booked_by = $this->userRepository->findOneBy(array('email' => $this->user->identity->getId())); // najdu z entity user uživatele, který má email odpovídající emailu aktulálně přihlášeného uživatele
		$this->thesiscollectionRepository->save($actual); // uložtím entitu thesiscollection (záznam práce) se změněným booked_by

		$this->flashMessage('Task has been reserved.');
		$this->redirect('this');
	}



	/**
	* @secured(roles="garant, studijni, admin")
	*/
	public function handleDisapprove($id)
	{
		$this->template->action = 'disapprove';

		if($id) {
			$entity = $this->thesiscollectionRepository->find($id);
			$entity->approved = -1;

			$this->thesiscollectionRepository->save($entity);

			$this->flashMessage('Task has been disapproved.');
		}
	}



	protected function createComponentForm()
	{
		$thesischatPage = $this->context->entityManager->getRepository('\CmsModule\Content\Entities\PageEntity')->createQueryBuilder('a')->where('a INSTANCE OF :type')->setParameters(array('type' => 'thesischatPage'))->getQuery()->getSingleResult();
		$form = $this->commentFrontFormFactor->invoke($this->thesischatRepository->createNew(array($thesischatPage, $this->thesiscollectionRepository->findOneBy(array('name' => $this->route->title)))));
		$form->onSuccess[] = $this->formSuccess;
		return $form;
	}



	public function formSuccess()
	{
		$this->chatLink = TRUE;
		$this->flashMessage('message sent');
		$this->redirect('this');
	}



	public function renderDefault()
	{
		if($this->chatLink) {
			$this->template->chatLink = $this->context->entityManager->getRepository('\CmsModule\Content\Entities\PageEntity')->createQueryBuilder('a')->where('a INSTANCE OF :type')->setParameters(array('type' => 'thesischatPage'))->getQuery()->getSingleResult();
		}
		if($this->user->isLoggedIn()) {
			$this->template->hasTask = $this->thesiscollectionRepository->findOneBy(array('booked_by' => $this->userRepository->findOneBy(array('email' => $this->user->identity->getId())))); // najdu, jestli akruálně přihlášený uživatel má už rezervovanou nějakou práci
		} else {
			$this->template->hasTask = false;
		}
		$this->template->thesiscollection = $this->thesiscollectionRepository->findOneBy(array('route' => $this->route->id));
	}

}
