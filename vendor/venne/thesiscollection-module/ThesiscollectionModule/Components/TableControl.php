<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Components;

use CmsModule\Content\SectionControl;
use ThesiscollectionModule\Forms\ThesiscollectionFormFactory;
use DoctrineModule\Repositories\BaseRepository;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class TableControl extends SectionControl
{

	/** @var BaseRepository */
	protected $thesiscollectionRepository;

	/** @var ThesiscollectionFormFactory */
	protected $formFactory;

	const TYPE_APPROVED_BOOLS = '\ThesiscollectionModule\Components\Table\Columns\ApprovedColumn';


	/**
	 * @param BaseRepository $thesiscollectionRepository
	 * @param ThesiscollectionFormFactory $formFactory
	 */
	public function __construct(BaseRepository $thesiscollectionRepository, ThesiscollectionFormFactory $formFactory)
	{
		parent::__construct();

		$this->thesiscollectionRepository = $thesiscollectionRepository;
		$this->formFactory = $formFactory;
	}


	protected function createComponentTable()
	{
		$table = new \CmsModule\Components\Table\TableControl;
		$table->setTemplateConfigurator($this->templateConfigurator);
		$table->setRepository($this->thesiscollectionRepository);

		$pageId = $this->entity->id;
		$table->setDql(function ($sql) use ($pageId) {
			$sql = $sql->andWhere('a.page = :page')->setParameter('page', $pageId);
			return $sql;
		});

		// forms
		$repository = $this->thesiscollectionRepository;
		$entity = $this->entity;
		$form = $table->addForm($this->formFactory, 'Options', function () use ($repository, $entity) {
			return $repository->createNew(array($entity));
		}, \CmsModule\Components\Table\Form::TYPE_FULL);

		// navbar
		$table->addButtonCreate('create', 'Create new', $form, 'file');

		$table->addColumn('name', 'Name')
			->setWidth('40%')
			->setSortable(TRUE)
			->setFilter();

		$table->addColumn('approved', 'Approved', TableControl::TYPE_APPROVED_BOOLS)
			->setWidth('20%')
			->setSortable(TRUE)
			->setFilter();

		$table->addColumn('leader', 'Leader')
			->setWidth('20%')
			->setSortable(TRUE)
			->setFilter();

		$table->addColumn('booked_by', 'Booked by')
			->setWidth('20%')
			->setSortable(TRUE)
			->setFilter();

		$repository = $this->thesiscollectionRepository;
		$presenter = $this;
		$action = $table->addAction('on', 'On');
		$action->onClick[] = function ($button, $entity) use ($presenter, $repository) {
			$entity->route->published = TRUE;
			$repository->save($entity);

			if (!$presenter->presenter->isAjax()) {
				$presenter->redirect('this');
			}

			$presenter['table']->invalidateControl('table');
			$presenter->presenter->payload->url = $presenter->link('this');
		};
		$action->onRender[] = function ($button, $entity) use ($presenter, $repository) {
			$button->setDisabled($entity->route->published);
		};

		$action = $table->addAction('off', 'Off');
		$action->onClick[] = function ($button, $entity) use ($presenter, $repository) {
			$entity->route->published = FALSE;
			$repository->save($entity);

			if (!$presenter->presenter->isAjax()) {
				$presenter->redirect('this');
			}

			$presenter['table']->invalidateControl('table');
			$presenter->presenter->payload->url = $presenter->link('this');
		};
		$action->onRender[] = function ($button, $entity) use ($presenter, $repository) {
			$button->setDisabled(!$entity->route->published);
		};

		$table->addActionEdit('edit', 'Edit', $form);
		$table->addActionDelete('delete', 'Delete');

		// global actions
		$table->setGlobalAction($table['delete']);

		return $table;
	}


	public function render()
	{
		$this->template->render();
	}
}
