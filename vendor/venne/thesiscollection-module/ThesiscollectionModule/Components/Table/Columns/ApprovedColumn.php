<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Components\Table\Columns;

use CmsModule\Components\Table\TableControl;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class ApprovedColumn extends \CmsModule\Components\Table\Columns\BaseColumn
{

	public static $approved_bools = array(
		'schváleno',
		'neschváleno',
	);

	public function __construct(TableControl $table, $name, $title)
	{
		parent::__construct($table, $name, $title);

		$_this = $this;
		$this->callback = function ($entity) use ($_this) {
			$column = $entity->{$_this->name};
			return self::$approved_bools[$column];
		};
	}

}
