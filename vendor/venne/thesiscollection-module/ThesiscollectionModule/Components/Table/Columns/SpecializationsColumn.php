<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Components\Table\Columns;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class SpecializationsColumn extends \CmsModule\Components\Table\Columns\BaseColumn
{

	public static $specializations = array(
		'bakalářské obory' => array(
			'Bc: DOL' => 'DOL',
			'Bc: INM' => 'INM',
			'Bc: LCR' => 'LCR',
			'Bc: LLD' => 'LLD',
			'Bc: LOS' => 'LOS',
		), 'magisterské obory' => array(
			'Mgr: LOG' => 'LOG',
		)
	);

}
