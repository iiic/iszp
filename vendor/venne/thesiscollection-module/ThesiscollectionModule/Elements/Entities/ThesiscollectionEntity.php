<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2011, 2012 Josef Kříž (http://www.josef-kriz.cz)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Elements\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\DoctrineModule\Repositories\BaseRepository")
 * @ORM\Table(name="thesiscollectionElement")
 * @ORM\DiscriminatorEntry(name="thesiscollectionElement")
 */
class ThesiscollectionEntity extends BaseThesiscollectionEntity
{

}
