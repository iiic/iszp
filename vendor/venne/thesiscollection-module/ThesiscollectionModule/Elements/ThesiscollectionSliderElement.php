<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Elements;

/**
 * @author Josef Kříž <pepakriz@gmail.com>
 */
class ThesiscollectionSliderElement extends ThesiscollectionElement
{
	/**
	 * @return string
	 */
	protected function getEntityName()
	{
		return get_class(new \ThesiscollectionModule\Elements\Entities\ThesiscollectionSliderEntity());
	}


	public function render()
	{
		//$this->template->width = $this->getEntity()->width;
		//$this->template->height = $this->getEntity()->height;
		parent::render();
	}
}
