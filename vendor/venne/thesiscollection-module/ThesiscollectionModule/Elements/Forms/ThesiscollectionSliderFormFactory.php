<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Elements\Forms;

use Venne\Forms\Form;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class ThesiscollectionSliderFormFactory extends ThesiscollectionFormFactory
{

	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();
		$form->addText('itemsPerPage', 'Items per page');
		$form->addManyToMany('pages', 'Pages');

		$form->addGroup();
		$form->addSaveButton('Save');
	}
}
