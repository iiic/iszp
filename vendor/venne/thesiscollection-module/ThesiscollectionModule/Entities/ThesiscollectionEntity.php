<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\ThesiscollectionModule\Repositories\ThesiscollectionRepository")
 * @ORM\Table(name="thesiscollection")
 * @ORM\HasLifecycleCallbacks
 */
class ThesiscollectionEntity extends \DoctrineModule\Entities\NamedEntity
{

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $text;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $specialization;

	/**
	 * @var PageEntity
	 * @ORM\ManyToOne(targetEntity="\ThesiscollectionModule\Entities\PageEntity", inversedBy="thesiscollections")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var \CmsModule\Content\Entities\RouteEntity
	 * @ORM\OneToOne(targetEntity="\CmsModule\Content\Entities\RouteEntity", cascade={"persist"}, orphanRemoval=true)
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $route;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $leader;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $expired;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $released;

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 */
	protected $approved;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $booked_by;

	/**
	 * @param PageEntity $pageEntity
	 */
	public function __construct(PageEntity $pageEntity)
	{
		parent::__construct();

		$this->page = $pageEntity;
		$this->text = '';
		$this->name = '';
		$this->created = new \Nette\DateTime();
		$this->released = new \Nette\DateTime();

		$this->route = new \CmsModule\Content\Entities\RouteEntity;
		$this->route->setPublished(TRUE);
		$this->route->setType('Thesiscollection:Thesiscollection:default');
		$this->page->routes[] = $this->route;
		$this->route->setPage($this->page);
		$this->route->setParent($this->page->mainRoute);
	}


	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate()
	{
		$this->updated = new \Nette\DateTime();
	}


	public function setName($name)
	{
		parent::setName($name);

		$this->route->setLocalUrl(Strings::webalize($this->name));
		$this->route->setTitle($name);
	}
	
	public function getName()
	{
		return $this->name;
	}

	public function getText()
	{
		return $this->text;
	}


	public function setText($text)
	{
		$this->text = $text;
	}


	/**
	 * @param \ThesiscollectionModule\Entities\PageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}


	/**
	 * @return \ThesiscollectionModule\Entities\PageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}


	/**
	 * @return \CmsModule\Content\Entities\RouteEntity
	 */
	public function getRoute()
	{
		return $this->route;
	}


	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}


	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}


	/**
	 * @param \DateTime $expired
	 */
	public function setExpired($expired)
	{
		$this->expired = $expired;
	}


	/**
	 * @return \DateTime
	 */
	public function getExpired()
	{
		return $this->expired;
	}


	/**
	 * @param \DateTime $released
	 */
	public function setReleased($released)
	{
		$this->released = $released;
	}


	/**
	 * @return \DateTime
	 */
	public function getReleased()
	{
		return $this->released;
	}


	/**
	 * @param \CmsModule\Security\Entities\UserEntity $leader
	 */
	public function setLeader($leader)
	{
		$this->leader = $leader;
	}


	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getLeader()
	{
		return $this->leader;
	}


	public function setSpecialization($specialization)
	{
		$this->specialization = $specialization;
	}


	public function getSpecialization()
	{
		return $this->specialization;
	}

	/**
	 * @param int $approved
	 */
	public function setApproved($approved)
	{
		$this->approved = $approved;
	}


	/**
	 * @return int
	 */
	public function getApproved()
	{
		return $this->approved;
	}


	/**
	 * @param \CmsModule\Security\Entities\UserEntity $booked_by
	 */
	public function setBooked_by($booked_by)
	{
		$this->booked_by = $booked_by;
	}


	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getBooked_by()
	{
		return $this->booked_by;
	}

}
