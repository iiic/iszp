<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 * @ORM\Entity(repositoryClass="\CmsModule\Content\Repositories\PageRepository")
 * @ORM\Table(name="thesiscollectionPage")
 * @ORM\DiscriminatorEntry(name="thesiscollectionPage")
 */
class PageEntity extends \CmsModule\Content\Entities\PageEntity
{

	/**
	 * @var ArrayCollection|ThesiscollectionEntity[]
	 * @ORM\OneToMany(targetEntity="ThesiscollectionEntity", mappedBy="page")
	 */
	protected $thesiscollections;

	/**
	 * @ORM\OneToOne(targetEntity="\CmsModule\Content\Entities\DirEntity", cascade={"all"})
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 */
	protected $dir;

	/**
	 * @var string
	 * @ORM\Column(type="integer")
	 */
	protected $itemsPerPage;

	public function __construct()
	{
		parent::__construct();
		$this->mainRoute->type = 'Thesiscollection:Default:default';
		$this->itemsPerPage = 10;

		$this->dir = new \CmsModule\Content\Entities\DirEntity();
		$this->dir->setInvisible(TRUE);
		$this->dir->setName('thesiscollectionPage');
	}


	public function getDir()
	{
		return $this->dir;
	}


	public function setThesiscollections($thesiscollections)
	{
		$this->thesiscollections = $thesiscollections;
	}


	public function getThesiscollections()
	{
		return $this->thesiscollections;
	}


	/**
	 * @param string $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
	}


	/**
	 * @return string
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}

}
