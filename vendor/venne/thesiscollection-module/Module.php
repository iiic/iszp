<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Michal Lupečka, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesiscollectionModule;

use Venne\Module\ComposerModule;

/**
 * @author Michal Lupečka, DiS. <ic.czech@gmail.com>
 */
class Module extends ComposerModule
{

}
