<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;
use DoctrineModule\Forms\Mappers\EntityMapper;
use Nette\Utils\Html;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class TransferFormFactory extends FormFactory
{

	/**
	 * @param \DoctrineModule\Forms\Mappers\EntityMapper $mapper
	 * @param \Nette\Security\User $user
	 */
	public function __construct(EntityMapper $mapper)
	{
		parent::__construct($mapper);
	}



	protected function getControlExtensions()
	{
		return array(
			new \DoctrineModule\Forms\ControlExtensions\DoctrineExtension(),
			new \CmsModule\Content\ControlExtension(),
			new \FormsModule\ControlExtensions\ControlExtension(),
			new \CmsModule\Content\Forms\ControlExtensions\ControlExtension(),
		);
	}



	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup('Transfer from collection to archive');

		$form->addManyToOne('thesis', 'Thesis')
			->setRequired('Select the item you want to move')
			->setOption("description", Html::el("small", "For safety reasons it is still necessary to confirm the move in to archive"));

		$form->addSaveButton('Transfer');
	}



	public function handleSave(Form $form)
	{
		$form->data->name = $form->data->thesis->name;

		parent::handleSave($form);
	}

}
