<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisprearchiveFormFactory extends FormFactory
{

	protected function getControlExtensions()
	{
		return array_merge(parent::getControlExtensions(), array(
			new \FormsModule\ControlExtensions\ControlExtension(),
		));
	}



	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();

		$form->addText('name', 'Thesis name')
			->setRequired('thesis name must be given');

		$form->addManyToOne('thesis', 'Thesis');

		$form->addSaveButton('Save');
	}

}
