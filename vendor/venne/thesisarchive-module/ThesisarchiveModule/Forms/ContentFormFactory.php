<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;
use Nette\Security\User;
use DoctrineModule\Forms\Mappers\EntityMapper;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ContentFormFactory extends FormFactory
{

	/** @var User */
	protected $user;

	/**
	 * @param \DoctrineModule\Forms\Mappers\EntityMapper $mapper
	 * @param \Nette\Security\User $user
	 */
	public function __construct(EntityMapper $mapper, User $user)
	{
		parent::__construct($mapper);

		$this->user = $user;
	}

	protected function getControlExtensions()
	{
		return array(
			new \DoctrineModule\Forms\ControlExtensions\DoctrineExtension(),
			new \CmsModule\Content\ControlExtension(),
			new \FormsModule\ControlExtensions\ControlExtension(),
			new \CmsModule\Content\Forms\ControlExtensions\ControlExtension(),
		);
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup('New Thesis Form');

		$form->addText('name', 'Thesis name')
			->setRequired('Thesis name must be given');

		$form->addSelect('specialization', 'Specialization', \ThesiscollectionModule\Components\Table\Columns\SpecializationsColumn::$specializations)
			->setRequired('Choose specialization');

		$form->addText('leader', 'Leader')
		->setEmptyValue($this->user->identity->getId())
		->setDisabled();

		//$form->addDate('released', 'Release date'); // datum, kdy se práce zobrazí ve výpisu

		$form->addDate('expired', 'Expiry date'); // datum, kdy se práce ztratí z výpisu

		$form->addContentEditor('text', 'Task');

		$form->addSaveButton('Save');
	}

	public function handleSave(Form $form)
	{
		$form->data->approved = 0;
		$form->data->leader = $this->mapper->getEntityManager()->getRepository('CmsModule\Security\Entities\UserEntity')->findOneBy(array('email' => $this->user->identity->id));

		parent::handleSave($form);
	}

}
