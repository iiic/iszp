<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class PrearchivepageFormFactory extends FormFactory
{

	protected function getControlExtensions()
	{
		return array(
			new \DoctrineModule\Forms\ControlExtensions\DoctrineExtension(),
			new \CmsModule\Content\ControlExtension(),
			new \FormsModule\ControlExtensions\ControlExtension(),
		);
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup('Settings');
		$form->addText('itemsPerPage', 'Items per page');
/*
		$form->addGroup('Notation');
		$form->addCheckbox('notationInHtml', 'In HTML format');
		$form->addCheckbox('autoNotation', 'Auto generate')->addCondition($form::EQUAL, true)->toggle('notationLength');
		$form->addGroup()->setOption('id', 'notationLength');
		$form->addText('notationLength', 'Length')->addRule($form::INTEGER);
*/
		$form->addGroup();
		$form->addSaveButton('Save');
	}

}
