<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Forms;

use Venne\Forms\Form;
use DoctrineModule\Forms\FormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisarchiveFormFactory extends FormFactory
{

	protected function getControlExtensions()
	{
		return array(
			new \DoctrineModule\Forms\ControlExtensions\DoctrineExtension(),
			new \CmsModule\Content\ControlExtension(),
			new \FormsModule\ControlExtensions\ControlExtension(),
			new \CmsModule\Content\Forms\ControlExtensions\ControlExtension(),
		);
	}


	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();

		$form->addText('name', 'Thesis name')
			->setRequired('Thesis name must be given');
		$form->addSelect('specialization', 'Specialization', \ThesiscollectionModule\Components\Table\Columns\SpecializationsColumn::$specializations)
			->setRequired('Choose specialization');

		$form->addGroup('Příslušnost');
		$form->addManyToOne('leader', 'Leader');
		$form->addManyToOne('student', 'Student');

		$form->addGroup('Dates');
		$form->addDateTime('created', 'Created date');
		$form->addDateTime('released', 'Release date');
		$form->addDateTime('expired', 'Expiry date');

		$form->addGroup();
		$form->addContentEditor('text', NULL, NULL, 20);

		$form->addSaveButton('Save');
	}

}
