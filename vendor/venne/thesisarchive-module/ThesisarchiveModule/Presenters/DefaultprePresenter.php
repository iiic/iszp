<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Presenters;

use ThesisarchiveModule\Repositories\ThesisprearchiveRepository;
use ThesisarchiveModule\Repositories\ThesisarchiveRepository;
use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use ThesischatModule\Repositories\LibraryRepository;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class DefaultprePresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @var ThesisprearchiveRepository */
	private $thesisprearchiveRepository;

	/** @var ThesisarchiveRepository */
	private $thesisarchiveRepository;

	/** @var LinkRepository */
	protected $linkRepository;

	/** @var LibraryRepository */
	protected $libraryRepository;



	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $linkRepository
	 */
	public function injectThesiscollectionRepository(ThesiscollectionRepository $linkRepository)
	{
		$this->linkRepository = $linkRepository;
	}



	/**
	 * @param \ThesischatModule\Repositories\LibraryRepository $libraryRepository
	 */
	public function injectLibraryRepository(LibraryRepository $libraryRepository)
	{
		$this->libraryRepository = $libraryRepository;
	}



	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisprearchiveRepository $thesisprearchiveRepository
	 */
	public function injectThesisprearchiveRepository(ThesisprearchiveRepository $thesisprearchiveRepository)
	{
		$this->thesisprearchiveRepository = $thesisprearchiveRepository;
	}



	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisarchiveRepository $thesisarchiveRepository
	 */
	public function injectThesisarchiveRepository(ThesisarchiveRepository $thesisarchiveRepository)
	{
		$this->thesisarchiveRepository = $thesisarchiveRepository;
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults(9999)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->thesisprearchiveRepository->createQueryBuilder("a");
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	public function renderDefault()
	{
		$this->template->itemsPerPage = $this->page->itemsPerPage;
	}



	public function handleGg($thesisCollectionId)
	{
		/* transfer z thesiscollection do thesisarchive */
		$ThesiscollectionEntity = $this->linkRepository->find($thesisCollectionId);
		$LibraryEntity = $this->libraryRepository->findOneBy(array('thesis' => $ThesiscollectionEntity));

		$thesisarchivePage = $this->context->entityManager->getRepository('\CmsModule\Content\Entities\PageEntity')->createQueryBuilder('a')->where('a INSTANCE OF :type')->setParameters(array('type' => 'thesisarchivePage'))->getQuery()->getSingleResult();
		$empty = $this->thesisarchiveRepository->createNew(array($thesisarchivePage));

		$empty->text = $ThesiscollectionEntity->text;
		$empty->specialization = $ThesiscollectionEntity->specialization;
		$empty->leader = $ThesiscollectionEntity->leader;
		$empty->student = $ThesiscollectionEntity->booked_by;
		$empty->created = $ThesiscollectionEntity->created;
		$empty->expired = $ThesiscollectionEntity->expired;
		$empty->released = $ThesiscollectionEntity->released;
		$empty->name = $ThesiscollectionEntity->name;

		$empty->defense_year = $LibraryEntity->defense_year;
		$empty->size = $LibraryEntity->size;
		$empty->annexes = $LibraryEntity->annexes;
		$empty->annotation = $LibraryEntity->annotation;
		$empty->keywords = $LibraryEntity->keywords;
		$empty->location = $LibraryEntity->location;
		$empty->thesis_pdf = $LibraryEntity->thesis_pdf;

		$this->thesisarchiveRepository->save($empty);
		$this->linkRepository->delete($ThesiscollectionEntity);

		// záznam z pre archivu se odstraní sám díky databázové vazbě

		$this->flashMessage('thesis was moved to the archive');
		$this->redirect('this');
	}



	public function handleBg($preArchiveId)
	{
		$entity = $this->thesisprearchiveRepository->find($preArchiveId);
		$this->thesisprearchiveRepository->delete($entity);

		$this->flashMessage('Thesis was returned to tasks');
		$this->redirect('this');
	}

}
