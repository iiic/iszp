<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Presenters;

use ThesisarchiveModule\Repositories\ThesisarchiveRepository;
use ThesisarchiveModule\Repositories\ThesisprearchiveRepository;
//use ThesiscollectionModule\Repositories\ThesiscollectionRepository;
use ThesisarchiveModule\Forms\TransferFormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class DefaultPresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @var ThesisarchiveRepository */
	private $thesisarchiveRepository;

	/** @var ThesisprearchiveRepository */
	private $thesisprearchiveRepository;

	/** @var TransferFormFactory */
	protected $transferFormFactory;

	/** @var LinkRepository */
	//protected $linkRepository;



	/**
	 * @param \ThesiscollectionModule\Repositories\ThesiscollectionRepository $linkRepository
	 *
	public function injectThesiscollectionRepository(ThesiscollectionRepository $linkRepository)
	{
		$this->linkRepository = $linkRepository;
	}*/



	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisarchiveRepository $thesisarchiveRepository
	 */
	public function injectThesisarchiveRepository(ThesisarchiveRepository $thesisarchiveRepository)
	{
		$this->thesisarchiveRepository = $thesisarchiveRepository;
	}



	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisprearchiveRepository $thesisprearchiveRepository
	 */
	public function injectThesisprearchiveRepository(ThesisprearchiveRepository $thesisprearchiveRepository)
	{
		$this->thesisprearchiveRepository = $thesisprearchiveRepository;
	}



	/**
	 * @param \ThesisarchiveModule\Forms\TransferFormFactory $transferFormFactory
	 */
	public function injectTransferFormFactory(TransferFormFactory $transferFormFactory)
	{
		$this->transferFormFactory = $transferFormFactory;
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults(9999)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->thesisarchiveRepository->createQueryBuilder("a")
			->leftJoin('a.route', 'r')
			->andWhere('r.published = :true')->setParameter('true', TRUE)
			->andWhere('a.page = :page')->setParameter('page', $this->page->id);
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	public function renderDefault()
	{
		$this->template->itemsPerPage = $this->page->itemsPerPage;
	}



	public function handleDelete($id)
	{
		$this->template->action = 'delete';

		if($id) {
			$entity = $this->thesisarchiveRepository->find($id);

			$this->thesisarchiveRepository->delete($entity);

			$this->flashMessage('Task has been deleted.');
			$this->redirect('this', array('do' => 'delete'));
		}

	}



	public function handleShowAll()
	{
		$this->template->view = 'all';
	}



	public function handleShowOnlyForRent()
	{
		$this->template->view = 'only-for-rent';
	}



	public function handleTransfer()
	{
		$this->template->action = 'transfer';
	}



	protected function createComponentForm()
	{
		$form = $this->transferFormFactory->invoke($this->thesisprearchiveRepository->createNew());
		$form->onSuccess[] = $this->formSuccess;
		return $form;
	}



	public function formSuccess()
	{
		$this->flashMessage('Transfer completed, now only approve transfer in pre-archive.');
		$this->redirect('this', array('do' => 'transfer'));
	}

}
