<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Presenters;

use ThesisarchiveModule\Repositories\ThesisarchiveRepository;
use ThesischatModule\Repositories\LibraryRepository;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisarchivePresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/**
	 * @var \ThesisarchiveModule\Repositories\ThesisarchiveRepository
	 */
	private $thesisarchiveRepository;

	/**
	 * @var \ThesischatModule\Repositories\LibraryRepository
	 */
	private $libraryRepository;


	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisarchiveRepository $thesisarchiveRepository
	 */
	public function injectThesisarchiveRepository(ThesisarchiveRepository $thesisarchiveRepository)
	{
		$this->thesisarchiveRepository = $thesisarchiveRepository;
	}



	/**
	 * @param \ThesischatModule\Repositories\LibraryRepository $libraryRepository
	 */
	public function injectLibraryRepository(LibraryRepository $libraryRepository)
	{
		$this->libraryRepository = $libraryRepository;
	}



	public function renderDefault()
	{
		$this->template->thesisarchive = $this->thesisarchiveRepository->findOneBy(array('route' => $this->route->id));
		//$this->template->libraryTable = $this->thesisarchiveRepository->findOneBy(array('route' => $this->route->id));
	}

}
