<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Presenters;

//use Venne;
use ThesisarchiveModule\Repositories\ThesisprearchiveRepository;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisprearchivePresenter extends \CmsModule\Content\Presenters\PagePresenter
{

	/** @persistent */
	public $key;

	/** @var ThesisprearchiveRepository */
	private $thesisprearchiveRepository;



	/**
	 * @param \ThesisarchiveModule\Repositories\ThesisprearchiveRepository $thesisprearchiveRepository
	 */
	public function injectThesisprearchiveRepository(ThesisprearchiveRepository $thesisprearchiveRepository)
	{
		$this->thesisprearchiveRepository = $thesisprearchiveRepository;
	}



	public function getItemsBuilder()
	{
		return $this->getQueryBuilder()
			->setMaxResults(9999)
			->setFirstResult($this['vp']->getPaginator()->getOffset());
	}



	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		return $this->thesisprearchiveRepository->createQueryBuilder("a");
			//->leftJoin('a.route', 'r')
			//->andWhere('r.published = :true')->setParameter('true', TRUE)
			//->andWhere('a.page = :page')->setParameter('page', $this->page->id)
			//->andWhere('a.released <= :released')->setParameter('released', new \Nette\DateTime())
			//->andWhere('(a.expired >= :expired OR a.expired IS NULL)')->setParameter('expired', new \Nette\DateTime());
	}



	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->page->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}



	public function renderDefault()
	{
		$this->template->thesisarchive = 'hoj';
	}

}
