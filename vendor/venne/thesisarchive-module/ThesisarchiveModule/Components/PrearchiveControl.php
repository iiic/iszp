<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Components;

use CmsModule\Content\SectionControl;
use ThesisarchiveModule\Forms\ThesisprearchiveFormFactory;
use DoctrineModule\Repositories\BaseRepository;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class PrearchiveControl extends SectionControl
{

	/** @var BaseRepository */
	protected $thesisprearchiveRepository;

	/** @var ThesisprearchiveFormFactory */
	protected $formFactory;



	/**
	 * @param BaseRepository $thesisprearchiveRepository
	 * @param ThesisprearchiveFormFactory $formFactory
	 */
	public function __construct(BaseRepository $thesisprearchiveRepository, ThesisprearchiveFormFactory $formFactory)
	{
		parent::__construct();

		$this->thesisprearchiveRepository = $thesisprearchiveRepository;
		$this->formFactory = $formFactory;
	}



	protected function createComponentTable()
	{
		$table = new \CmsModule\Components\Table\TableControl;
		$table->setTemplateConfigurator($this->templateConfigurator);
		$table->setRepository($this->thesisprearchiveRepository);

		// forms
		$repository = $this->thesisprearchiveRepository;
		$entity = $this->entity;
		$form = $table->addForm($this->formFactory, 'Options', function () use ($repository, $entity) {
			return $repository->createNew(array($entity));
		}, \CmsModule\Components\Table\Form::TYPE_NORMAL);

		// navbar
		$table->addButtonCreate('create', 'Create new', $form, 'file');

		$table->addColumn('name', 'Name')
			->setWidth('100%')
			->setSortable(TRUE)
			->setFilter();

		//$repository = $this->thesisprearchiveRepository;

		$table->addActionEdit('edit', 'Edit', $form);
		$table->addActionDelete('delete', 'Delete');

		// global actions
		$table->setGlobalAction($table['delete']);

		return $table;
	}



	public function render()
	{
		$this->template->render();
	}

}
