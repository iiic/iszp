<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Components;

use CmsModule\Content\SectionControl;
use ThesisarchiveModule\Forms\ThesisarchiveFormFactory;
use DoctrineModule\Repositories\BaseRepository;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ArchiveControl extends SectionControl
{

	/** @var BaseRepository */
	protected $thesisarchiveRepository;

	/** @var ThesisarchiveFormFactory */
	protected $formFactory;


	/**
	 * @param BaseRepository $thesisarchiveRepository
	 * @param ThesisarchiveFormFactory $formFactory
	 */
	public function __construct(BaseRepository $thesisarchiveRepository, ThesisarchiveFormFactory $formFactory)
	{
		parent::__construct();

		$this->thesisarchiveRepository = $thesisarchiveRepository;
		$this->formFactory = $formFactory;
	}


	protected function createComponentTable()
	{
		$table = new \CmsModule\Components\Table\TableControl;
		$table->setTemplateConfigurator($this->templateConfigurator);
		$table->setRepository($this->thesisarchiveRepository);

		$pageId = $this->entity->id;
		$table->setDql(function ($sql) use ($pageId) {
			$sql = $sql->andWhere('a.page = :page')->setParameter('page', $pageId);
			return $sql;
		});

		// forms
		$repository = $this->thesisarchiveRepository;
		$entity = $this->entity;
		$form = $table->addForm($this->formFactory, 'Options', function () use ($repository, $entity) {
			return $repository->createNew(array($entity));
		}, \CmsModule\Components\Table\Form::TYPE_FULL);

		// navbar
		$table->addButtonCreate('create', 'Create new', $form, 'file');

		$table->addColumn('name', 'Name')
			->setWidth('100%')
			->setSortable(TRUE)
			->setFilter();

		$repository = $this->thesisarchiveRepository;
		$presenter = $this;
		$action = $table->addAction('on', 'On');
		$action->onClick[] = function ($button, $entity) use ($presenter, $repository) {
			$entity->route->published = TRUE;
			$repository->save($entity);

			if (!$presenter->presenter->isAjax()) {
				$presenter->redirect('this');
			}

			$presenter['table']->invalidateControl('table');
			$presenter->presenter->payload->url = $presenter->link('this');
		};
		$action->onRender[] = function ($button, $entity) use ($presenter, $repository) {
			$button->setDisabled($entity->route->published);
		};

		$action = $table->addAction('off', 'Off');
		$action->onClick[] = function ($button, $entity) use ($presenter, $repository) {
			$entity->route->published = FALSE;
			$repository->save($entity);

			if (!$presenter->presenter->isAjax()) {
				$presenter->redirect('this');
			}

			$presenter['table']->invalidateControl('table');
			$presenter->presenter->payload->url = $presenter->link('this');
		};
		$action->onRender[] = function ($button, $entity) use ($presenter, $repository) {
			$button->setDisabled(!$entity->route->published);
		};

		$table->addActionEdit('edit', 'Edit', $form);
		$table->addActionDelete('delete', 'Delete');

		// global actions
		$table->setGlobalAction($table['delete']);

		return $table;
	}


	public function render()
	{
		$this->template->render();
	}
}
