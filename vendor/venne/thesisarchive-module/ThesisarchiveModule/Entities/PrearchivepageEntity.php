<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\CmsModule\Content\Repositories\PageRepository")
 * @ORM\Table(name="thesisprearchivePage")
 * @ORM\DiscriminatorEntry(name="thesisprearchivePage")
 */
class PrearchivepageEntity extends \CmsModule\Content\Entities\PageEntity
{

	/**
	 * @var ArrayCollection|ThesisprearchiveEntity[]
	 * @ORM\OneToMany(targetEntity="ThesisprearchiveEntity", mappedBy="page")
	 */
	protected $thesisprearchives;

	/**
	 * @var string
	 * @ORM\Column(type="integer")
	 */
	protected $itemsPerPage;



	public function __construct()
	{
		parent::__construct();
		$this->mainRoute->type = 'Thesisprearchive:Defaultpre:default';
		$this->itemsPerPage = 10;
	}



	public function setThesisprearchives($thesisprearchives)
	{
		$this->thesisprearchives = $thesisprearchives;
	}



	public function getThesisprearchives()
	{
		return $this->thesisprearchives;
	}



	/**
	 * @param string $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
	}



	/**
	 * @return string
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}

}
