<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\ThesisarchiveModule\Repositories\ThesisprearchiveRepository")
 * @ORM\Table(name="thesisprearchive")
 * @ORM\HasLifecycleCallbacks
 */
class ThesisprearchiveEntity extends \DoctrineModule\Entities\NamedEntity
{

	/**
	 * @var ThesiscollectionEntity
	 * @ORM\ManyToOne(targetEntity="\ThesiscollectionModule\Entities\ThesiscollectionEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $thesis;

	// + ještě tu bude $name (od rodiče)



	/**
	 * @param PrearchivepageEntity $prearchivepageEntity
	 */
	public function __construct(PrearchivepageEntity $prearchivepageEntity = NULL)
	{
		parent::__construct();
		$this->name = '';
	}



	/**
	 * @param \ThesiscollectionModule\Entities\ThesiscollectionEntity $thesis
	 */
	public function setThesis($thesis)
	{
		$this->thesis = $thesis;
	}



	/**
	 * @return \ThesiscollectionModule\Entities\ThesiscollectionEntity
	 */
	public function getThesis()
	{
		return $this->thesis;
	}

}
