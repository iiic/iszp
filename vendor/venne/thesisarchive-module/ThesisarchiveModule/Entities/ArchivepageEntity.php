<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\CmsModule\Content\Repositories\PageRepository")
 * @ORM\Table(name="thesisarchivePage")
 * @ORM\DiscriminatorEntry(name="thesisarchivePage")
 */
class ArchivepageEntity extends \CmsModule\Content\Entities\PageEntity
{

	/**
	 * @var ArrayCollection|ThesisarchiveEntity[]
	 * @ORM\OneToMany(targetEntity="ThesisarchiveEntity", mappedBy="page")
	 */
	protected $thesisarchives;



	/**
	 * @var string
	 * @ORM\Column(type="integer")
	 */
	protected $itemsPerPage;

	public function __construct()
	{
		parent::__construct();
		$this->mainRoute->type = 'Thesisarchive:Default:default';
		$this->itemsPerPage = 10;
	}



	public function setThesisarchives($thesisarchives)
	{
		$this->thesisarchives = $thesisarchives;
	}



	public function getThesisarchives()
	{
		return $this->thesisarchives;
	}



	/**
	 * @param string $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;
	}



	/**
	 * @return string
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}

}
