<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\ThesisarchiveModule\Repositories\ThesisarchiveRepository")
 * @ORM\Table(name="thesisarchive")
 * @ORM\HasLifecycleCallbacks
 */
class ThesisarchiveEntity extends \DoctrineModule\Entities\NamedEntity
{

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $text;

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $specialization;

	/**
	 * @var ArchivepageEntity
	 * @ORM\ManyToOne(targetEntity="\ThesisarchiveModule\Entities\ArchivepageEntity", inversedBy="thesisarchives")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $page;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $defense_year;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $size;

	/**
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $annexes;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	//protected $opponent;

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $annotation = '';

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $keywords;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $location;

	/**
	 * @var \CmsModule\Content\Entities\RouteEntity
	 * @ORM\OneToOne(targetEntity="\CmsModule\Content\Entities\RouteEntity", cascade={"persist"}, orphanRemoval=true)
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $route;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $leader;

	/**
	 * @var \CmsModule\Security\Entities\UserEntity
	 * @ORM\ManyToOne(targetEntity="\CmsModule\Security\Entities\UserEntity")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $student;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updated;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $expired;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $released;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $archived;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $for_rent = true;

	/**
	 * @var \CmsModule\Content\Entities\FileEntity
	 * @ORM\OneToOne(targetEntity="\CmsModule\Content\Entities\FileEntity", cascade={"all"}, orphanRemoval=true)
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 */
	protected $thesis_pdf;



	/**
	 * @param ArchivepageEntity $archivepageEntity
	 */
	public function __construct(ArchivepageEntity $archivepageEntity = NULL)
	{
		parent::__construct();

		$this->page = $archivepageEntity;
		$this->text = '';
		$this->name = '';

		$this->route = new \CmsModule\Content\Entities\RouteEntity;
		$this->route->setPublished(TRUE);
		$this->route->setType('Thesisarchive:Thesisarchive:default');
		if($this->page) {
			$this->page->routes[] = $this->route;
			$this->route->setPage($this->page);
			$this->route->setParent($this->page->mainRoute);
		}
	}



	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate()
	{
		$this->updated = new \Nette\DateTime();
	}



	public function setName($name)
	{
		parent::setName($name);

		$this->route->setLocalUrl(Strings::webalize($this->name));
		$this->route->setTitle($name);
	}



	public function getText()
	{
		return $this->text;
	}



	public function setText($text)
	{
		$this->text = $text;
	}



	/**
	 * @param \ThesisarchiveModule\Entities\ArchivepageEntity $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}



	/**
	 * @return \ThesisarchiveModule\Entities\ArchivepageEntity
	 */
	public function getPage()
	{
		return $this->page;
	}



	/**
	 * @param integer $defense_year
	 */
	public function setDefense_year($defense_year)
	{
		$this->defense_year = $defense_year;
	}



	/**
	 * @return integer
	 */
	public function getDefense_year()
	{
		return $this->defense_year;
	}



	/**
	 * @param integer $size
	 */
	public function setSize($size)
	{
		$this->size = $size;
	}



	/**
	 * @return integer
	 */
	public function getSize()
	{
		return $this->size;
	}



	/**
	 * @param integer $annexes
	 */
	public function setAnnexes($annexes)
	{
		$this->annexes = $annexes;
	}



	/**
	 * @return integer
	 */
	public function getAnnexes()
	{
		return $this->annexes;
	}



	/**
	 * @param string $annotation
	 */
	public function setAnnotation($annotation)
	{
		$this->annotation = $annotation;
	}



	/**
	 * @return string
	 */
	public function getAnnotation()
	{
		return $this->annotation;
	}



	/**
	 * @param string $keywords
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}



	/**
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}



	/**
	 * @param string $location
	 */
	public function setLocation($location)
	{
		$this->location = $location;
	}



	/**
	 * @return string
	 */
	public function getLocation()
	{
		return $this->location;
	}



	/**
	 * @return \CmsModule\Content\Entities\RouteEntity
	 */
	public function getRoute()
	{
		return $this->route;
	}



	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}



	/**
	 * @return \DateTime $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}



	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}



	/**
	 * @param \DateTime $expired
	 */
	public function setExpired($expired)
	{
		$this->expired = $expired;
	}



	/**
	 * @return \DateTime
	 */
	public function getExpired()
	{
		return $this->expired;
	}



	/**
	 * @param \DateTime $released
	 */
	public function setReleased($released)
	{
		$this->released = $released;
	}



	/**
	 * @return \DateTime
	 */
	public function getReleased()
	{
		return $this->released;
	}



	/**
	 * @param \DateTime $archived
	 */
	public function setArchived($archived)
	{
		$this->archived = $archived;
	}



	/**
	 * @return \DateTime
	 */
	public function getArchived()
	{
		return $this->archived;
	}



	/**
	 * @param \CmsModule\Security\Entities\UserEntity $leader
	 */
	public function setLeader($leader)
	{
		$this->leader = $leader;
	}



	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getLeader()
	{
		return $this->leader;
	}



	/**
	 * @param \CmsModule\Security\Entities\UserEntity $student
	 */
	public function setStudent($student)
	{
		$this->student = $student;
	}



	/**
	 * @return \CmsModule\Security\Entities\UserEntity
	 */
	public function getStudent()
	{
		return $this->student;
	}



	public function setSpecialization($specialization)
	{
		$this->specialization = $specialization;
	}



	public function getSpecialization()
	{
		return $this->specialization;
	}



	/**
	 * @param int $pre_archive
	 */
	public function setPre_archive($pre_archive)
	{
		$this->pre_archive = $pre_archive;
	}



	/**
	 * @return int
	 */
	public function getPre_archive()
	{
		return $this->pre_archive;
	}



	/**
	 * @param boolean $for_rent
	 */
	public function setFor_rent($for_rent)
	{
		$this->for_rent = $for_rent;
	}



	/**
	 * @return boolean
	 */
	public function getFor_rent()
	{
		return $this->for_rent;
	}



	/**
	 * @param \CmsModule\Content\Entities\FileEntity $thesis_pdf
	 */
	public function setThesis_pdf($thesis_pdf)
	{
		$this->thesis_pdf = $thesis_pdf;

		if ($this->thesis_pdf instanceof FileEntity) {
			$this->thesis_pdf->setName('__wtf__' . $this->thesis_pdf->getName());
			$this->thesis_pdf->setInvisible(TRUE);
		}
	}



	/**
	 * @return \CmsModule\Content\Entities\FileEntity
	 */
	public function getThesis_pdf()
	{
		return $this->thesis_pdf;
	}

}
