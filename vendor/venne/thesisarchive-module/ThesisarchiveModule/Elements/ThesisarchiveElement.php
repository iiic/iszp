<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Elements;

use DoctrineModule\Repositories\BaseRepository;
use Venne\Forms\FormFactory;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisarchiveElement extends \CmsModule\Content\Elements\BaseElement
{
	/**
	 * @var BaseRepository
	 */
	private $thesisarchiveRepository;

	/** @var TextFormFactory */
	protected $setupFormFactory;


	/**
	 * @param TextFormFactory $setupForm
	 */
	public function injectSetupForm(FormFactory $setupForm)
	{
		$this->setupFormFactory = $setupForm;
	}


	/**
	 * @param BaseRepository $thesisarchiveRepository
	 */
	public function injectThesisarchiveRepository($thesisarchiveRepository)
	{
		$this->thesisarchiveRepository = $thesisarchiveRepository;
	}


	/**
	 * @return array
	 */
	public function getViews()
	{
		return array(
			'setup' => 'Edit element',
		) + parent::getViews();
	}


	/**
	 * @return string
	 */
	protected function getEntityName()
	{
		return get_class(new \ThesisarchiveModule\Elements\Entities\ThesisarchiveEntity());
	}


	public function getItems()
	{
		$query = $this->getQueryBuilder();
		$query->setMaxResults($this->getEntity()->itemsPerPage);
		$query->setFirstResult($this['vp']->getPaginator()->getOffset());
		$query->orderBy('a.created', 'DESC');

		return $query->getQuery()->getResult();
	}


	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function getQueryBuilder()
	{
		$dql = $this->thesisarchiveRepository->createQueryBuilder("a");

		if (count($this->getEntity()->pages) > 0) {
			$ids = array();
			foreach ($this->getEntity()->pages as $page) {
				$ids[] = $page->id;
			}

			$dql = $dql->join('a.page', 'p');
			$dql = $dql->andWhere('p.id IN (:ids)')->setParameter('ids', $ids);
		}

		return $dql;
	}

/*
	protected function createComponentVp()
	{
		$vp = new \CmsModule\Components\VisualPaginator;
		$pg = $vp->getPaginator();
		$pg->setItemsPerPage($this->getEntity()->itemsPerPage);
		$pg->setItemCount($this->getQueryBuilder()->select("COUNT(a.id)")->getQuery()->getSingleScalarResult());
		return $vp;
	}
*/

	public function render()
	{
		$this->template->render();
	}


	public function renderSetup()
	{
		echo $this['form']->render();
	}


	/**
	 * @return \Venne\Forms\Form
	 */
	protected function createComponentForm()
	{
		$form = $this->setupFormFactory->invoke($this->getEntity());
		$form->onSuccess[] = $this->processForm;
		return $form;
	}


	public function processForm($form)
	{
		$this->getPresenter()->redirect('this');
	}
}
