<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Elements\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\DoctrineModule\Repositories\BaseRepository")
 * @ORM\Table(name="thesisarchiveElement")
 * @ORM\DiscriminatorEntry(name="thesisarchiveElement")
 */
class ThesisarchiveEntity extends BaseThesisarchiveEntity
{

}
