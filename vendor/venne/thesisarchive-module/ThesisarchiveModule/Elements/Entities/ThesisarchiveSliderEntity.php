<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Elements\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 * @ORM\Entity(repositoryClass="\DoctrineModule\Repositories\BaseRepository")
 * @ORM\Table(name="thesisarchiveSliderElement")
 * @ORM\DiscriminatorEntry(name="thesisarchiveSliderElement")
 */
class ThesisarchiveSliderEntity extends BaseThesisarchiveEntity
{

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 */
	protected $width;

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 */
	protected $height;


	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * @param int $height
	 */
	public function setHeight($height)
	{
		$this->height = $height;
	}


	/**
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}


	/**
	 * @param int $width
	 */
	public function setWidth($width)
	{
		$this->width = $width;
	}


	/**
	 * @return int
	 */
	public function getWidth()
	{
		return $this->width;
	}
}
