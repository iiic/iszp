<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Elements;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisarchiveSliderElement extends ThesisarchiveElement
{
	/**
	 * @return string
	 */
	protected function getEntityName()
	{
		return get_class(new \ThesisarchiveModule\Elements\Entities\ThesisarchiveSliderEntity());
	}


	public function render()
	{
		//$this->template->width = $this->getEntity()->width;
		//$this->template->height = $this->getEntity()->height;
		parent::render();
	}
}
