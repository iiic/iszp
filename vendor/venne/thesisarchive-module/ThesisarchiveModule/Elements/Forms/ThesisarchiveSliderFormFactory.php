<?php

/**
 * This file is part of the Venne:CMS (https://github.com/Venne)
 *
 * Copyright (c) 2013 Gustav Doležal, DiS.
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace ThesisarchiveModule\Elements\Forms;

use Venne\Forms\Form;

/**
 * @author Gustav Doležal, DiS. <gusta.dolezal@gmail.com>
 */
class ThesisarchiveSliderFormFactory extends ThesisarchiveFormFactory
{

	/**
	 * @param Form $form
	 */
	public function configure(Form $form)
	{
		$form->addGroup();
		$form->addText('itemsPerPage', 'Items per page');
		$form->addManyToMany('pages', 'Pages');

		$form->addGroup();
		$form->addSaveButton('Save');
	}
}
